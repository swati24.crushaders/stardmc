<?php
session_start();
include('header.php');
?>

   <div class="inner-banner">
       <img src="images/hotel-deal-banner.jpg" alt="" class="img-responsive">
       <h3>Hotel-Deals</h3>
    </div>
<div class="clearfix"></div>

<div class="head-bg">
<div class="container">
<ul class="breadcrumb">
  <li><a href="index.php">Home</a></li>
  <li><a href="hotel-deals.php">Hotel-Deals</a></li>
</ul></div>
    </div>
        <div class="clearfix"></div>
        <div class="container">
    <div class="about">
    <div class="col-md-8">
        <div id="indivisual-hotel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#indivisual-hotel" data-slide-to="0" class="active"></li>
      <li data-target="#indivisual-hotel" data-slide-to="1"></li>
      <li data-target="#indivisual-hotel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="item active">
        <img src="images/hotel-slider-1.jpg" alt="" style="width:100%;">
        </div>

      <div class="item">
        <img src="images/hotel-slider-2.jpg" alt="" style="width:100%;">
      </div>
    
      <div class="item">
        <img src="images/hotel-slider-3.jpg" alt="" style="width:100%;">
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#indivisual-hotel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#indivisual-hotel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> 
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <div class="clearfix"></div>
        </div>
        <div class="col-md-4">
      <h3 class="get-in-touch">Get a Free Hotel Quotation</h3>
          <form class="sidebar-form">
              <div class="form-group col-md-12">
                  <div class="row">
                    <input type="text" placeholder="Full Name" class="form-control">
              </div></div><br>
            <div class="form-group col-md-12">
                  <div class="row">
                    <input type="text" placeholder="Email" class="form-control">
              </div></div>
              <div class="form-group col-md-12">
                  <div class="row">
                    <input type="tel" placeholder="Phone Number" class="form-control">
              </div></div>
              
              <div class="form-group col-md-6">
              <div class="row">
                  <input type="text" name="arrivaldate_taxi" id="arrival" class="form-control datepicker" readonly="readonly" placeholder="Arival Date (If Available) ..." value="" required="">
                  </div>
              </div>
              <div class="form-group col-md-6">
              <div class="row pdlft">
                  <input type="text" name="arrivaldate_taxi" id="arrival" class="form-control datepicker" readonly="readonly" placeholder="Arival Date (If Available) ..." value="" required="">
                  </div>
              </div>
              <div class="form-group col-md-4">
                  <div class="row">
                    <input type="text" placeholder="No. of Room" class="form-control">
              </div></div>
              <div class="form-group col-md-4">
                  <div class="row pdlft">
                    <input type="text" placeholder="Adults" class="form-control">
              </div></div>
              <div class="form-group col-md-4">
                  <div class="row pdlft">
                    <input type="text" placeholder="Children" class="form-control">
              </div></div>
              <div class="form-group col-md-3">
                  <div class="row">
                    <input type="text" class="form-control" value="4507">
              </div></div>
              <div class="form-group col-md-9">
                  <div class="row pdlft">
                    <input type="text" placeholder="Enter captcha code here" class="form-control">
              </div></div>
              <button type="submit" class="btn  btn-block custom">Submit</button>
            </form>
        </div>
        
        
    </div>
        </div>
        <?php
include("footer.php");
?>
        
           </body>
    </html>