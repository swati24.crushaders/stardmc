<?php
session_start();
include('header.php');
?>

   <div class="inner-banner">
       <img src="images/holiday-banner.jpg" alt="" class="img-responsive">
       <h3>Holidays</h3>
    </div>

       <div class="clearfix"></div>

<div class="head-bg">
<div class="container">
<ul class="breadcrumb">
  <li><a href="index.php">Home</a></li>
  <li><a HREF="odisha-package.php">Our Holidays</a></li>
</ul></div>
    </div>
        <div class="clearfix"></div>
        <div class="container">
    <div class="about">
    <div class="col-md-8">
        <div class="row">
           <div class="col-md-4">
            <div class="package"><img src="images/odisha-package-1.jpg" class="img-responsive">
            <div class="tour-title">
                <div class=" col-md-12 name">
                <p>Culture of Orissa</p>               
                </div>
                <div class="col-md-12 detail">
                <a href="detail.php">View Details</a>
                <button class="btn  btn-info book-now" data-toggle="modal" data-target="#myModal">Book Now</button>
                    <div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Booking Request Form</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       <form>
           <div class="form-group">
          <input type="text" placeholder="name" class="form-control">
          </div>
           <div class="form-group">
          <input type="text" placeholder="email" class="form-control">
          </div>
           <div class="form-group">
          <input type="text" placeholder="phone" class="form-control">
          </div>
           <div class="form-group">
          <textarea class="form-control" placeholder="message"></textarea>
          </div>
           <button class="btn submit">submit</button>
          </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
                </div>
                <div class="clearfix"></div>
                </div>
               </div>
            </div> 
            <div class="col-md-4">
                <div class="package"><img src="images/odisha-package-2.jpg" class="img-responsive">
                <div class="tour-title">
                <div class=" col-md-12 name">
                <p>Odisha With Diamond Triangle...</p>
                </div>
                <div class="col-md-12 detail">
                <a href="#">View Details</a>
                <button class="btn  btn-info book-now">Book Now</button>
                </div>
                <div class="clearfix"></div>
                </div></div>
            </div>
            <div class="col-md-4">
            <div class="package"><img src="images/odisha-package-3.jpg" class="img-responsive">
                <div class="tour-title">
                <div class=" col-md-12 name">
                <p>Exotic Orissa</p>
                </div>
                <div class="col-md-12 detail">
                <a href="#">View Details</a>
                <button class="btn  btn-info book-now">Book Now</button>
                </div>
                <div class="clearfix"></div>
                </div>
                </div>
            </div>
        </div>
        <div class="row"><br>
           <div class="col-md-4">
            <div class="package"><img src="images/beach.jpg" class="img-responsive">
<div class="tour-title">
                <div class=" col-md-12 name">
                <p>Odisha beach Tour</p>
                </div>
                <div class="col-md-12 detail">
                <a href="#">View Details</a>
                <button class="btn  btn-info book-now">Book Now</button>
                </div>
                <div class="clearfix"></div>
                </div>
                        </div>
            </div> 
            <div class="col-md-4">
                <div class="package"><img src="images/odisha-package-4.jpg" class="img-responsive">
                <div class="tour-title">
                <div class=" col-md-12 name">
                <p>Amazing Odisha</p>
                </div>
                <div class="col-md-12 detail">
                <a href="#">View Details</a>
                <button class="btn  btn-info book-now">Book Now</button>
                </div>
                <div class="clearfix"></div>
                </div>
                </div>
            </div>
            <div class="col-md-4">
            <div class="package"><img src="images/odisa.jpg" class="img-responsive">
                <div class="tour-title">
                <div class=" col-md-12 name">
                <p>Luxury Odisha Tour</p>
                </div>
                <div class="col-md-12 detail">
                <a href="#">View Details</a>
                <button class="btn  btn-info book-now">Book Now</button>
                </div>
                <div class="clearfix"></div>
                </div>
                </div>
            </div>
        </div>
        <div class="row"><br>
           <div class="col-md-4">
            <div class="package"><img src="images/odisha-package-5.jpg" class="img-responsive">
                <div class="tour-title">
                <div class=" col-md-12 name">
                <p>Memorizing Odisha</p>
                </div>
                <div class="col-md-12 detail">
                <a href="#">View Details</a>
                <button class="btn  btn-info book-now">Book Now</button>
                </div>
                <div class="clearfix"></div>
                </div></div>
            </div> 
            <div class="col-md-4">
                <div class="package"><img src="images/odisha-package-6.jpg" class="img-responsive">
                <div class="tour-title">
                <div class=" col-md-12 name">
                <p> Bhubaneswar-Puri-Satapada-Pipli</p>
                </div>
                <div class="col-md-12 detail">
                <a href="#">View Details</a>
                <button class="btn  btn-info book-now">Book Now</button>
                </div>
                <div class="clearfix"></div>
                </div>
                </div>
            </div>
        </div>
        </div>
        <div class="col-md-4">
      <h3 class="get-in-touch">GET IN TOUCH</h3>
          <form class="sidebar-form">
           <input type="text" placeholder="Full Name" class="form-control"><br>
            <input type="text" placeholder="Email Id" class="form-control"><br>
            <input type="tel" placeholder="Contact Number" class="form-control"> <br>
            <textarea class="form-control" placeholder="Message..."></textarea><br>
            <button type="submit" class="btn  btn-block custom">Submit</button>
            </form>
        </div>
        
        
    </div>
        </div>
        <?php
include("footer.php");
?>
