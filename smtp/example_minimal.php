<?php

require 'class/SMTPMailer.php';
$mail = new SMTPMailer();

$mail->addTo('santanu.ctsl2618@gmail.com');

$mail->Subject('Test Mail message for you');
$mail->Body(
    '<h3>Test Mail message</h3>
     This is a <b>html</b> message.<br>
    Greetings!'
);

if ($mail->Send()) echo 'Mail sent successfully';
else               echo 'Mail failure';
