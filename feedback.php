<?php
session_start();
include('header.php');
?>

   
<!--
    <div id="tour" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#tour" data-slide-to="0" class="active"></li>
      <li data-target="#tour" data-slide-to="1"></li>
      <li data-target="#tour" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">

      <div class="item active">
        <img src="images/odisha-package-slider.jpg" alt="Los Angeles" style="width:100%;">
        <div class="carousel-caption">
        </div>
      </div>

      <div class="item">
        <img src="images/odisha-package-slider1.jpg" alt="Chicago" style="width:100%;">
        <div class="carousel-caption">
        </div>
      </div>
    
      <div class="item">
        <img src="images/footer.jpg" alt="New York" style="width:100%;">
        <div class="carousel-caption">
        </div>
      </div>
  
    </div>
    <a class="left carousel-control" href="#tour" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#tour" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
-->   
<div class="head-bg">
<div class="container">
<ul class="breadcrumb">
  <li><a href="index.html">Home</a></li>
  <li><a HREF="feedback.html">Review</a></li>
</ul></div>
    </div>
        <div class="clearfix"></div>
        <div class="container">
    <div class="about">
    <div class="col-md-9">
        <div class="row">
        <button class="review-book">Create Your Own Review</button>
        <div class="review">
            <h3>Submit Your Review</h3>
            <form>
            <div class="form-group row">
                <div class="col-md-2">
                   <label class="color">Your Name :</label>
                    </div>
                    <div class="col-md-6">
                    <input type="text" class="form-control">
                    </div>
                <div class="col-md-4"></div>
                </div>
                <div class="form-group row">
                <div class="col-md-2">
                   <label class="color">Email Address :</label>
                    </div>
                    <div class="col-md-6">
                    <input type="text" class="form-control">
                    </div>
                <div class="col-md-4"></div>
                </div>
                  <div class="form-group row">
                <div class="col-md-2">
                   <label class="color">Rating:</label>
                    </div>
                    <div class="col-md-6">
                    <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
                    </div>
                <div class="col-md-4"></div>
                </div>
                <div class="form-group row">
                <div class="col-md-2">
                   <label class="color">Review:</label>
                    </div>
                    <div class="col-md-6">
                    <textarea class="form-control"></textarea>
                    </div>
                <div class="col-md-4"></div>
                </div>
                <div class="form-group">
                <input type="reset" class="submit-feedabck" value="submit">
                <input type="reset" class="cancel" value="cancel">
                </div>
            </form>
        </div></div><br><br>
        <div class="row padr8">
       <div class="feedback-text">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <h5>Lorem ipsum dolor sit amet</h5>
            </div>
        </div><br>
        <div class="row padr8">
       <div class="feedback-text">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <h5>Lorem ipsum dolor sit amet</h5>
            </div>
        </div><br>
        <div class="row padr8">
       <div class="feedback-text">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <h5>Lorem ipsum dolor sit amet</h5>
            </div>
        </div><br>
        <div class="row padr8">
       <div class="feedback-text">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <h5>Lorem ipsum dolor sit amet</h5>
            </div>
        </div><br>
        <div class="row padr8">
       <div class="feedback-text">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <span class="fa fa-star checked"></span>
           <h5>Lorem ipsum dolor sit amet</h5>
            </div>
        </div>
        </div>
        <div class="col-md-3">
      <h3 class="get-in-touch">GET IN TOUCH</h3>
          <form class="sidebar-form">
           <input type="text" placeholder="Full Name" class="form-control"><br>
            <input type="text" placeholder="Email Id" class="form-control"><br>
            <input type="tel" placeholder="Contact Number" class="form-control"> <br>
            <textarea class="form-control" placeholder="Message..."></textarea><br>
            <button type="submit" class="btn  btn-block custom">Submit</button>
            </form>
        </div>
    </div>
        </div>
        
        
      <?php
include("footer.php");
?>
       

    </body>
    </html>