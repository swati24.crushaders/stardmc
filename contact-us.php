<?php
session_start();
include 'dbcon.php';
include('header.php');
?>
  
  <div class="head-bg">
<div class="container">
<ul class="breadcrumb">
  <li><a href="index.html">Home</a></li>
  <li><a>Contact Us</a></li>
</ul></div>
    </div>
        <div class="clearfix"></div>
        <div class="container">
    <div class="about">
    <div class="col-md-7">
        <h4>DROP US A MESSAGE</h4>
     <form name="contact_form" method="POST" id="contact_form">
        <div class="row">
         <div class="col-md-6">
             <div class="form-group">
            <input type="text"  name="user_fname" id="user_fname" class="form-control" placeholder="Enter Your First Name" required>
            </div></div>
         <div class="col-md-6">
             <div class="form-group">
            <input type="text" name="user_lname" id="user_lname"  placeholder="Enter Your Last Name" class="form-control" required>
            </div></div>
         </div>
         <div class="row">
         <div class="col-md-6">
             <div class="form-group">
            <input type="email" name="user_email" id="user_email" placeholder="Enter Your Email id" class="form-control" required>
            </div></div>
         <div class="col-md-6">
             <div class="form-group">
            <input type="number" name="user_phn" id="user_phn" placeholder="Enter Your Phone" class="form-control" required>
            </div></div>
         </div>
         <div class="row">
         <div class="col-md-12">
             <div class="form-group">
            <textarea class="form-control pad100"  name="user_mrg" id="user_msg" placeholder="Place Your Message"></textarea>
            </div></div>
         </div>
         <div class="row">
             <div class="col-md-12">
                 <div class="form-group">
            <button class="btn submit" name="contact_submit" id="contact_submit">submit</button>
                 </div></div>
         </div>
        </form>
        </div>
        <div class="col-md-5">
<h4>Get In Touch</h4>
<p><i class="fas fa-map-marker-alt"></i>&nbsp;Plot No 72, Kharvel Nagar,<br>&nbsp;&nbsp;&nbsp;&nbsp;Master Canteen RoadUnit-3,<br>&nbsp; &nbsp; Kharvel Nagar,Bhubaneswar, 751001,<br>&nbsp;&nbsp;&nbsp;&nbsp;INDIA</p>
<p><i class="fas fa-envelope"></i><a href="mailto:suvi@stardmc.com" class="a-email">&nbsp;&nbsp;suvi@stardmc.com</a></p>
<p><i class="fas fa-envelope"></i><a href="mailto:bobby@stardmc.com" class="a-email">&nbsp;&nbsp;bobby@stardmc.com</a></p>
<p><i class="fas fa-mobile-alt"></i>&nbsp;<a href="#">&nbsp;7735749753/ 52/ 54/ 56/ 59/</a>,<br>&nbsp;&nbsp;<a href="#">&nbsp;&nbsp;63/7032200351/7735746608</a></p>
<h4>Follow Us</h4>
<div class="phone-icon">
<div class="top-icon1" style="float:left;">
<a href="#"><img src="images/fb.png" alt=""></a>
</div>
<div class="top-icon1" style="float:left;margin-left:5px;">
<a href="#"><img src="images/twit.png" alt=""></a>
</div>
<div class="top-icon1" style="float:left;margin-left:5px;">
<a href="#"><img src="images/utube.png" alt=""></a>
</div>
</div>
</div>
    </div>
<div class=" col-md-12 address-map">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3742.684799501727!2d85.84077431439509!3d20.271903018560547!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a19a7518e4b7987%3A0x93c2104866b0c3cc!2sSTARDMC%20HOLIDAYS%20PVT%20LTD!5e0!3m2!1sen!2sin!4v1573879900732!5m2!1sen!2sin" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe> 
</div>
</div>
 <script>
     $( document ).ready(function() {
    $("#contact_form").submit(function (e) {
    	e.preventDefault();
		$.ajax({
			type: $('#contact_form').attr('method'),
			url: 'contact_ajax.php',
			data: $('#contact_form').serialize(),
			success: function (data) {
				alertify.alert(data);
				$("#contact_form")[0].reset();
				//document.getElementById("cform").reset();
			},
			error: function (data) {
				console.log('An error occurred.');
			},
		});
        return false;
    });
     });
     
 </script>       
        
     <?php
include("footer.php");
?>

    </body>
    </html>