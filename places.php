<?php
session_start();
include('header.php');
?>

   <div class="inner-banner">
       <img src="images/holidays-banner.jpg" alt="" class="img-responsive">
       <h3>Places</h3>
    </div>
<div class="clearfix"></div>

<div class="head-bg">
<div class="container">
<ul class="breadcrumb">
  <li><a href="index.php">Home</a></li>
  <li><a href="places.php">Places</a></li>
</ul></div>
    </div>
        <div class="clearfix"></div>
        <div class="container">
    <div class="about">
    <div class="col-md-8">
  <div id="bhubaneswar" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#bhubaneswar" data-slide-to="0" class="active"></li>
      <li data-target="#bhubaneswar" data-slide-to="1"></li>
      <li data-target="#bhubaneswar" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="images/culture-banner.jpg" alt="" style="width:100%;">
      </div>

      <div class="item">
        <img src="images/culture-banner.jpg" alt="" style="width:100%;">
        </div>
    
      <div class="item">
        <img src="images/culture-banner.jpg" alt="" style="width:100%;">
        </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#bhubaneswar" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#bhubaneswar" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <p><strong>&#34;<a href="index.com">Odisha</a>&#34;</strong>&nbsp;the Indian state is famous for tourism. In this state very beautiful places are present those incredible natural views creates sweet memories on the peoples mind those visited those places. Some of the well&#45;known&nbsp;<strong>Tourist Places in Odisha</strong>&nbsp;are Puri Jagannath Temple or Shree Mandir&#44; Konark Sun Temple&#44; Puri and Konark Beach&#44; Lingaraj Temple&#44; Chilika Lake&#44; Simlipal&#44; DhauliGiri&#44; Udayagiri and Khandagiri Caves etc there are many more Tourist places are present in Odisha (Orissa). In these tourist places huge number of tourists exploring Odisha&#39;s great culture&#44; art&#44; natural beauty and creativity of Odia peoples.</p> 
        <p>From Economy point of view the&nbsp;<a href="index.php"><strong>Tourist Places of Odisha</strong></a>&nbsp;plays an important role&#44; the 500 km &#40;310 mi&#41; long coastline of Odisha (Orissa)&#44; the beautiful towering mountains&#44; serene lakes and frolicking rivers and many more places are present in Odisha those are not less than heaven for tourist. People love to spend time in Odisha&#39;s famous Tourist Places. That brings millions of tourist from all over the world come to Odisha&#44; India. So Odisha Tourism Development providing more priority for tourists and tourism in Odisha.</p>
        <div class="row">
        <div class="col-md-4">
            <div class="place-name">
            <img src="images/place.jpg" alt="" class="img-responsive">
            <div class="place-text">
            <a href="place-inner-page.php">Puri</a>
            </div></div>
            </div>
        <div class="col-md-4">
            <div class="place-name">
            <img src="images/place.jpg" alt="" class="img-responsive">
            <div class="place-text">
            <a href="#">Bhubaneswar</a>
            </div></div>
            </div>
        <div class="col-md-4">
            <div class="place-name">
            <img src="images/place.jpg" alt="" class="img-responsive">
            <div class="place-text">
            <a href="#">Konark</a>
            </div></div>
            </div>
        </div><br>
        <div class="row">
           <div class="col-md-4">
            <div class="place-name">
            <img src="images/place.jpg" alt="" class="img-responsive">
            <div class="place-text">
            <a href="#">Chilika</a>
            </div></div>
            </div>
               <div class="col-md-4">
            <div class="place-name">
            <img src="images/place.jpg" alt="" class="img-responsive">
            <div class="place-text">
            <a href="#">Goplapur</a>
            </div></div>
            </div>
               <div class="col-md-4">
            <div class="place-name">
            <img src="images/place.jpg" alt="" class="img-responsive">
            <div class="place-text">
            <a href="#">Bhubaneswar</a>
            </div></div>
            </div>
        </div>
        
        
        </div>
        <div class="col-md-4">
      <h3 class="get-in-touch">Enquiry Now</h3>
          <form class="sidebar-form">
              <div class="form-group col-md-12">
                  <div class="row">
                    <input type="text" placeholder="Full Name" class="form-control">
              </div></div><br>
            <div class="form-group col-md-12">
                  <div class="row">
                    <input type="text" placeholder="Email" class="form-control">
              </div></div>
              <div class="form-group col-md-12">
                  <div class="row">
                    <input type="tel" placeholder="Phone Number" class="form-control">
              </div></div>
              
              <div class="form-group col-md-12">
              <div class="row">
                  <input type="text" name="arrivaldate_taxi" id="arrival" class="form-control datepicker" readonly="readonly" placeholder="Arival Date (If Available) ..." value="" required="">
                  </div>
              </div>
              <div class="form-group col-md-12">
                  <div class="row">
                    <input type="text" placeholder="Adult : (12+yrs)" class="form-control">
              </div></div>
              <div class="form-group col-md-12">
                  <div class="row">
                    <input type="text" placeholder="Child : (2-11 yrs)" class="form-control">
              </div></div>
              <div class="form-group col-md-12">
                  <div class="row">
                    <input type="text" placeholder="Infant : (0-2yrs)" class="form-control">
              </div></div>
            <textarea class="form-control" placeholder="Tour Requirements & Preferences (if any)"></textarea><br>
              <button type="submit" class="btn  btn-block custom">Submit</button>
            </form>
        </div>
        
        
    </div>
        </div>
        <?php
include("footer.php");
?>
  
    </body>
    </html>