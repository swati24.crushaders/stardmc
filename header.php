<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/alertify.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="css/lightslider.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/lightslider.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
    <script src="js/alertify.min.js"></script>
    <style>
        .demo-error {
        display:inline-block;
        color:#FF0000;
        margin-left:5px;
        }
        .demo-input {
            width: 100%;
            border-radius: 5px;
            border: #CCC 1px solid;
            padding: 5px;
            margin-top: 5px;
        }
        .demo-btn {
        padding: 10px;
            border-radius: 5px;
            background: #478347;
            border: #325a32 1px solid;
            color: #FFF;
            font-size: 1em;
            width: 100%;
            cursor:pointer;
        }
        .demo-heading {
        font-size: 1.5em;
            border-bottom: #CCC 1px solid;
            margin-bottom:5px;
        }
        .demo-table {
            background: #dcfddc;
            border-radius: 5px;
            padding: 10px;
        }
        .demo-success {
            margin-top: 5px;
            color: #478347;
            background: #e2ead1;
            padding: 10px;
            border-radius: 5px;
        }
        .captcha-input {
        background:#FFF url('captcha_code.php') repeat-y;
        padding-left: 85px;
        }
    </style>
    </head>
    <body>
        <div class="header mrgn">
        <div class="container">
            <div class="col-md-5 col-sm-5 col-xs-6">
            <a href="index.php"><img src="images/logo.jpg" alt=""></a>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-6">
           <div class="col-md-offset-2 col-md-8 search-box">
               <form class="form-inline padng">
                   <button class="btn btn-info mgnr8">Pay Now</button>
               <input type="text" placeholder="Search...." class="form-control">
               <button type="submit" class="search"><i class="fa fa-search"></i></button>
               </form> 
            </div>
            </div>
            </div>
        </div>
        
   <div class="main">
   <nav class="navbar bg-blue">
    <div class="container">
        <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
    <ul class="nav navbar-nav menu">
      <li><a href="index.php">Home</a></li>
         <li><a href="about-us.php">About us</a></li>
         <li><a href="odisha-package.php">Holidays</a></li>
      <li><a href="visa.php">Visa</a></li>
        <li class="dropdown mega-dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" data-target=".js-navbar-collapse" >Car Rentals
        <span class="caret"></span></a>
        <ul class="dropdown-menu mega-dropdown-menu">
	<li class="w20">
						<ul>
							<li class="dropdown-header">Mini & Sedan Cars</li>
                            <hr>
							<li><a href="car-detail.php" class="car-rental-menu">Tata Indigo Cab</a></li>
							<li><a href="#" class="car-rental-menu">Swift Dzire Cab</a></li>
							<li><a href="#" class="car-rental-menu">Hyundai Xcent Cab</a></li>
							<li><a href="#" class="car-rental-menu">Chevrolet Sail Cab</a></li>
							<li><a href="#" class="car-rental-menu">Honda Amaze Cab</a></li>
							<li><a href="#" class="car-rental-menu">Toyota Etios Cab</a></li>
							<li><a href="#" class="car-rental-menu">Honda City Cab</a></li>
						</ul>
					</li>
					<li class="w20">
						<ul>
							<li class="dropdown-header">SUV Cars</li>
                            <hr>
							<li><a href="#" class="car-rental-menu">Chevrolet Enjoy</a></li>
							<li><a href="#" class="car-rental-menu">Mahindra Scorpio</a></li>
							<li><a href="#" class="car-rental-menu">Chevrolet Tavera</a></li>
							<li><a href="#" class="car-rental-menu">Toyota Innova</a></li>
							<li><a href="#" class="car-rental-menu">Four columns</a></li>
							<li><a href="#" class="car-rental-menu">Toyota Innova Crysta</a></li>
                            <li><a href="#" class="car-rental-menu">Toyota Fortuner</a></li>
						</ul>
					</li>
                    <li class="w20">
						<ul>
							<li class="dropdown-header">Tempo Travellers</li>
                            <hr>
							<li><a href="#" class="car-rental-menu">AC 9 Seater Luxury Winger Van</a></li>
							<li><a href="#" class="car-rental-menu">AC 13 Seater Tempo Traveller</a></li>
							<li><a href="#" class="car-rental-menu">AC 17 Seater Tempo Traveller</a></li>
							<li><a href="#" class="car-rental-menu">AC 26 Seater Tempo Traveller</a></li>
							<li><a href="#" class="car-rental-menu">AC 15 Seater Luxury Traveller</a></li>
							<li><a href="#" class="car-rental-menu">AC 18 Seater Luxury Traveller</a></li>
						</ul>
					</li>
					<li class="w20">
						<ul>
							<li class="dropdown-header">SML / Coach</li>
                            <hr>
							<li><a href="#" class="car-rental-menu">AC 13 Seater SML Coach</a></li>
							<li><a href="#" class="car-rental-menu">AC 18 Seater SML Coach</a></li>
							<li><a href="#" class="car-rental-menu">AC 28 Seater SML Coach</a></li>
							<li><a href="#" class="car-rental-menu">41 Seater and 14 Sleeper Bus</a></li>
							<li><a href="#" class="car-rental-menu">45 Seater AC Volvo Bus</a></li>
							<li><a href="#" class="car-rental-menu">44 Seater Luxury Bus</a></li>
						</ul>
					</li>
					<li class="w20">
						<ul>
							<li class="dropdown-header">Luxury Cars</li>
                            <hr>
							<li><a href="#" class="car-rental-menu">Mercedes Benz E250</a></li>
							<li><a href="#" class="car-rental-menu">Audi A6</a></li>
							<li><a href="#" class="car-rental-menu">Jaguar XF</a></li>
						    <li><a href="#" class="car-rental-menu">Audi A4</a></li> 
                            <li><a href="#" class="car-rental-menu">Audi Q3</a></li>    
                            <li><a href="#" class="car-rental-menu">Bmw 3 Series</a></li>    
						</ul>
            </li>
				</ul>
      </li>
      <li><a href="coach.php">Coach</a></li>
      <li><a href="tempo-traveller.php">Tempo Traveller</a></li>
        <li><a href="luxury-cars.php">Luxury Cars</a></li>
        <li><a href="wedding-car.php">Wedding Cars</a></li>
<li class="dropdown mega-dropdown" id="dropdown-place">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" data-target=".js-navbar-collapse" >Places
        <span class="caret"></span></a>
        <ul class="dropdown-menu mega-dropdown-menu">
	<li class="col-md-4">
						<ul>
							<li class="dropdown-header">Bhubaneswar</li>
							<li><a href="#" class="car-rental-menu">Lingaraj Temple</a></li>
							<li><a href="#" class="car-rental-menu">Udayagiri and Khandagiri Caves</a></li>
							<li><a href="#" class="car-rental-menu">Nandankanan Zoological Park</a></li>
							<li><a href="#" class="car-rental-menu">Dhauli Shanti Stupa</a></li>
							<li><a href="#" class="car-rental-menu">Pathani Samanta Planetarium</a></li>	
						</ul>
					</li>
					<li class="col-md-4">
						<ul>
							<li class="dropdown-header">Puri</li>
							<li><a href="#" class="car-rental-menu">Jagannath Temple, Puri</a></li>
                            <li><a href="#" class="car-rental-menu">Puri sea beach</a></li>
                            <li><a href="#" class="car-rental-menu">Gundicha Temple</a></li>
                            <li><a href="#" class="car-rental-menu">Ramachandi Temple</a></li>
                            <li><a href="#" class="car-rental-menu">Swargadwar Road</a></li>
						</ul>
					</li>
                    <li class="col-md-4">
						<ul>
							<li class="dropdown-header">Konark</li>
  <li><a href="#" class="car-rental-menu">Chandrabhaga Beach</a></li>
                            <li><a href="#" class="car-rental-menu">Konark Sun Temple</a></li>
                            <li><a href="#" class="car-rental-menu">Konark Beach</a></li>
                            <li><a href="#" class="car-rental-menu">Chandrabhaga Beach</a></li>
                            <li><a href="#" class="car-rental-menu">Konarak ASI museum</a></li>
						</ul>
					</li>
            <div class="clearfix"></div>
					<li class="col-md-4">
						<ul>
							<li class="dropdown-header">Chilika Lake</li>
                           <li><a href="#" class="car-rental-menu">Kalijai</a></li>
                            <li><a href="#" class="car-rental-menu">Baliharachandi Beach</a></li>
                            <li><a href="#" class="car-rental-menu">Kalijai Island</a></li>
                            <li><a href="#" class="car-rental-menu">Nalbana Bird Sanctuary</a></li>
                            <li><a href="#" class="car-rental-menu">Baliharachandi Temple</a></li>
						</ul>
					</li>
					<li class="col-md-4">
						<ul>
							<li class="dropdown-header">Gopalpur</li>
                            <li><a href="#" class="car-rental-menu">Gopalpur Beach</a></li>
                            <li><a href="#" class="car-rental-menu">Dhabaleshwar Beach</a></li>
                            <li><a href="#" class="car-rental-menu">Vijay Dwar</a></li>
                            <li><a href="#" class="car-rental-menu">Gopalpur Light House</a></li>
                            <li><a href="#" class="car-rental-menu">Markandi Beach</a></li> 
						</ul>
            </li>
            					<li class="col-md-4">
						<ul>
							<li class="dropdown-header">Konark</li>
                              <li><a href="#" class="car-rental-menu">Chandrabhaga Beach</a></li>
                            <li><a href="#" class="car-rental-menu">Konark Sun Temple</a></li>
                            <li><a href="#" class="car-rental-menu">Konark Beach</a></li>
                            <li><a href="#" class="car-rental-menu">Chandrabhaga Beach</a></li>
                            <li><a href="#" class="car-rental-menu">Konarak ASI museum</a></li> 
						</ul>
            </li>
				</ul>
      </li>
        <li><a href="contact-us.php">Contact Us</a></li>
    </ul></div>
       </div>
        </nav>
    </div>