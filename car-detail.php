<?php
session_start();
include('header.php');
?>

   <div class="inner-banner">
       <img src="images/tata-indigo-banner.jpg" alt="" class="img-responsive">
       <h3>Tata Indigo</h3>
    </div>
 <div class="clearfix"></div>

<div class="head-bg">
<div class="container">
<ul class="breadcrumb">
  <li><a href="index.html">Home</a></li>
  <li><a href="#">Tata Indigo</a></li>
</ul></div>
    </div>
        <div class="clearfix"></div>
        <div class="container">
    <div class="about">
    <div class="col-md-8">
      <div class="single-car">
        <img src="images/car-detail-tata-indigo.jpg" alt="" class="img-responsive">
    </div>
        <br>
      <div class="col-md-12">
        <div class="row">
        <p><strong>Hire Tata Indigo Car in Odisha</strong>, Book Prepaid Taxi from <a href="index.html">StarDmc Car Rentals</a> for <strong>Local Tour,</strong> <strong>Outstation Tour,</strong> <strong>Intercity Tour,</strong> <strong>Pick &amp; Drop Services (All Transfers),</strong> <strong>Tourist Transport</strong>. Customized your Odisha Tour with Tata Indigo Cab with Best Lowest Price Guaranteed. Book Tata Indigo Cab Online for Whole Odisha State, pick up &amp; Drop available from Bhubaneswar Airport (BBI), Bhubaneswar Railway Station (BBS), Puri Railway Station, Berhampur Railway Station, Balasore Railway Station &amp; All other Railway stations in Odisha. Hire Tata Indigo Taxi on rent for Private Odisha Tours. Pre-Book Your Bookings Online for Tata Indigo in Odisha &amp; Get Attractive Discounts.</p>
            <p><a href="index.html"><strong>Star Dmc</strong></a>, A Recognized <a href="index.html">Local Inbound Tour Operator</a> &amp; <a href="index.html">Tourist Transport Operator</a> of Odisha which provides All Variants of Mini &amp; Sedan Cars, SUVS Cars, Premium Luxury Cars, AC Winger, AC Luxury Tempo Traveller, Non-AC Tourist Bus and AC Luxury Tourist Coach on hireFor Entire Odisha (Orissa) State which Includes Cities Like Bhubaneswar, Bhubaneswar Airport (BBI), Bhubaneswar Railway Station, Puri Railway Station, Puri Jagannath Dham, Khurdha Jn Railway Station, Cuttack Railway Station, Berhampur Railway Station, Angul, Bhubaneswar, Brahmagiri, Bhitarkanika National Park, Brahmapur, Balangir, Balasore, Barbil, Bargarh, Baripada, Bhadrak, Balugaon, Bhawanipatna, Balliguda, Boudh, Chilika Lake, Cuttack, Chandipur, Chhatrapur, Daringbadi, Dhabaleswar, Dhenkanal, Deogarh, Damanjodi, Gopalpur, Ganjam, Jajpur, Jaleswar, Jharsuguda, Jagdalpur, Jeypore, Jagatsinghpur, Khurdha, Konark, Kendrapara, Koraput, Keonjhar, Mayurbhanj, Muniguda, Malkangiri, Nayagarh, Nowrangpur, Nuapada, Puri, Paradip, Phulbani, Paralakhemundi, Rayagada, Rourkela, Satapada, SakhiGopal Sambalpur, Sunabeda, Sundargarh, Sonepur, Simlipal National Park, Titlagarh, Talcher, Taptapani.</p>
            
             
   
<div class="">
    <table class="table table-bordered table-responsive">
    <thead>
      <tr>
        <th class="heading">Variants Of Vehicle</th>
        <th class="heading" colspan="4">Local Trip</th>
        <th class="heading">Outstation / Long Distance<br>
(Min 250Km Per Day)</th>
      </tr>
           <tr>
        <th></th>
<th>80km/8Hrs.(Rs.)</th>
<th>120km/12Hrs.(Rs.)</th>
<th>Extra Hrs. (Rs.)</th>
<th>Extra Km (Rs.)</th>
<th>Per Km (Rs.)</th>
      </tr>
        </thead>
        
    <tbody>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/car-image-t1.jpg" alt=""><br>
           <p>AC Tata Indigo (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>1,350</p></td>  
        <td class="wedding-table"><p>1,800</p></td>
        <td class="wedding-table"><p>90</p></td>
        <td class="wedding-table"><p>11</p></td>
        <td class="wedding-table"><p>11</p></td>
        </tr>
        <tr>
        <th class="pad15"></th>
            <th class="pad15"></th>
            <th class="pad15"></th>
            <th class="pad15"></th>
            <th class="pad15"></th>
            <th class="pad15"></th>
        </tr>
        
        
        </tbody>
        
  </table>
          
    
        </div>
           
          </div>
        </div>
        </div>  
        </div>
        <div class="col-md-4">
      <h3 class="get-in-touch">Enquiry Now</h3>
          <form class="sidebar-form">
              <div class="form-group col-md-12">
                  <div class="row">
              <select class="form-control">
          <option selected="selected">Pick up Location</option>
          <option value=" Bhubaneswar Airport (BBI)"> Bhubaneswar Airport (BBI)</option>
          <option value="Bhubaneswar Railway Station ">Bhubaneswar Railway Station </option>
          <option value="Bhubaneswar Hotels">Bhubaneswar Hotels</option>
          <option value="Puri Railway Station ">Puri Railway Station </option>
          <option value="Puri Hotels">Puri Hotels</option>
          <option value="Puri Area">Puri Area</option>
          <option value="Khurdha Jn Railway Station">Khurdha Jn Railway Station</option>
          <option value="Cuttack Railway Station">Cuttack Railway Station</option>
          <option value="Berhampur Railway Station">Berhampur Railway Station</option>
          <option value="Angul Odisha India">Angul Odisha India</option>
          <option value="Bhubaneswar Odisha India">Bhubaneswar Odisha India</option>
          <option value="Brahmagiri Odisha India">Brahmagiri Odisha India</option>
          <option value="Bhitarkanika Odisha India">Bhitarkanika Odisha India</option>
          <option value="Brahmapur Odisha India">Brahmapur Odisha India</option>
          <optgroup label="----------------------------------------"></optgroup>
          <option value="Balangir Odisha India">Balangir Odisha India</option>
          <option value="Balasore Odisha India ">Balasore Odisha India </option>
          <option value="Barbil Odisha India">Barbil Odisha India</option>
          <option value="Bargarh Odisha India">Bargarh Odisha India</option>
          <option value="Baripada Odisha India">Baripada Odisha India</option>
          <option value="Bhadrak Odisha India">Bhadrak Odisha India</option>
          <option value="Balugaon Odisha India">Balugaon Odisha India</option>
          <option value="Bhawanipatna Odisha India">Bhawanipatna Odisha India</option>
          <option value="Balliguda Odisha India">Balliguda Odisha India</option>
          <option value="Boudh Odisha India">Boudh Odisha India</option>
          <option value="Chilika Lake Odisha India">Chilika Lake Odisha India</option>
          <option value="Cuttack Odisha India">Cuttack Odisha India</option>
          <option value="Chandipur Odisha India">Chandipur Odisha India</option>
          <option value="Chhatrapur Odisha India">Chhatrapur Odisha India</option>
          <option value="Daringbadi Odisha India">Daringbadi Odisha India</option>
          <option value="Dhabaleswar Odisha India">Dhabaleswar Odisha India</option>
          <option value="Dhenkanal Odisha India">Dhenkanal Odisha India</option>
          <option value="Deogarh Odisha India">Deogarh Odisha India</option>
          <option value="Damanjodi Odisha India">Damanjodi Odisha India</option>
          <option value="Gopalpur Odisha India">Gopalpur Odisha India</option>
          <option value="Ganjam Odisha India">Ganjam Odisha India</option>
          <option value="Jajpur Odisha India">Jajpur Odisha India</option>
          <option value="Jaleswar Odisha India">Jaleswar Odisha India</option>
          <option value="Jharsuguda Odisha India">Jharsuguda Odisha India</option>
          <option value="Jagdalpur Odisha India">Jagdalpur Odisha India</option>
          <option value="Jeypore Odisha India">Jeypore Odisha India</option>
          <option value="Jagatsinghpur Odisha India">Jagatsinghpur Odisha India</option>
          <option value="Khurdha Odisha India">Khurdha Odisha India</option>
          <option value="Konark Odisha India">Konark Odisha India</option>
          <option value="Kendrapara Odisha India">Kendrapara Odisha India</option>
          <option value="Koraput Odisha India">Koraput Odisha India</option>
          <option value="Keonjhar Odisha India">Keonjhar Odisha India</option>
          <option value="Mayurbhanj Odisha India">Mayurbhanj Odisha India</option>
          <option value="Muniguda Odisha India">Muniguda Odisha India</option>
          <option value="Malkangiri Odisha India">Malkangiri Odisha India</option>
          <option value="Nayagarh Odisha India">Nayagarh Odisha India</option>
          <option value="Nowrangpur Odisha India">Nowrangpur Odisha India</option>
          <option value="Nuapada Odisha India">Nuapada Odisha India</option>
          <option value="Puri Odisha India">Puri Odisha India</option>
          <option value="Paradip Odisha India">Paradip Odisha India</option>
          <option value="Phulbani Odisha India">Phulbani Odisha India</option>
          <option value="Paralakhemundi Odisha India">Paralakhemundi Odisha India</option>
          <option value="Rayagada Odisha India">Rayagada Odisha India</option>
          <option value="Rourkela Odisha India">Rourkela Odisha India</option>
          <option value="Satapada Odisha India">Satapada Odisha India</option>
          <option value="SakhiGopal Odisha India">SakhiGopal Odisha India</option>
          <option value="Sambalpur Odisha India">Sambalpur Odisha India</option>
          <option value="Sunabeda Odisha India">Sunabeda Odisha India</option>
          <option value="Sundargarh Odisha India">Sundargarh Odisha India</option>
          <option value="Sonepur Odisha India">Sonepur Odisha India</option>
          <option value="Simlipal Odisha India">Simlipal Odisha India</option>
          <option value="Titlagarh Odisha India">Titlagarh Odisha India</option>
          <option value="Talcher Odisha India">Talcher Odisha India</option>
          <option value="Taptapani Odisha India">Taptapani Odisha India</option>
          <option value="carfrom">Other Location (If Any)</option>
        </select></div></div><br>
            <div class="form-group col-md-12">
              <div class="row">
                <select name="dropme_taxi" id="dropmeselect" class="form-control"> 
          <option selected="selected">Drop Location</option>
          <option value=" Bhubaneswar Airport (BBI)"> Bhubaneswar Airport (BBI)</option>
          <option value="Bhubaneswar Railway Station ">Bhubaneswar Railway Station </option>
          <option value="Bhubaneswar Hotels">Bhubaneswar Hotels</option>
          <option value="Puri Railway Station ">Puri Railway Station </option>
          <option value="Puri Hotels">Puri Hotels</option>
          <option value="Puri Area">Puri Area</option>
          <option value="Khurdha Jn Railway Station">Khurdha Jn Railway Station</option>
          <option value="Cuttack Railway Station">Cuttack Railway Station</option>
          <option value="Berhampur Railway Station">Berhampur Railway Station</option>
          <option value="Angul Odisha India">Angul Odisha India</option>
          <option value="Bhubaneswar Odisha India">Bhubaneswar Odisha India</option>
          <option value="Brahmagiri Odisha India">Brahmagiri Odisha India</option>
          <option value="Bhitarkanika Odisha India">Bhitarkanika Odisha India</option>
          <option value="Brahmapur Odisha India">Brahmapur Odisha India</option>
          <optgroup label="----------------------------------------"></optgroup>
          <option value="Balangir Odisha India">Balangir Odisha India</option>
          <option value="Balasore Odisha India ">Balasore Odisha India </option>
          <option value="Barbil Odisha India">Barbil Odisha India</option>
          <option value="Bargarh Odisha India">Bargarh Odisha India</option>
          <option value="Baripada Odisha India">Baripada Odisha India</option>
          <option value="Bhadrak Odisha India">Bhadrak Odisha India</option>
          <option value="Balugaon Odisha India">Balugaon Odisha India</option>
          <option value="Bhawanipatna Odisha India">Bhawanipatna Odisha India</option>
          <option value="Balliguda Odisha India">Balliguda Odisha India</option>
          <option value="Boudh Odisha India">Boudh Odisha India</option>
          <option value="Chilika Lake Odisha India">Chilika Lake Odisha India</option>
          <option value="Cuttack Odisha India">Cuttack Odisha India</option>
          <option value="Chandipur Odisha India">Chandipur Odisha India</option>
          <option value="Chhatrapur Odisha India">Chhatrapur Odisha India</option>
          <option value="Daringbadi Odisha India">Daringbadi Odisha India</option>
          <option value="Dhabaleswar Odisha India">Dhabaleswar Odisha India</option>
          <option value="Dhenkanal Odisha India">Dhenkanal Odisha India</option>
          <option value="Deogarh Odisha India">Deogarh Odisha India</option>
          <option value="Damanjodi Odisha India">Damanjodi Odisha India</option>
          <option value="Gopalpur Odisha India">Gopalpur Odisha India</option>
          <option value="Ganjam Odisha India">Ganjam Odisha India</option>
          <option value="Jajpur Odisha India">Jajpur Odisha India</option>
          <option value="Jaleswar Odisha India">Jaleswar Odisha India</option>
          <option value="Jharsuguda Odisha India">Jharsuguda Odisha India</option>
          <option value="Jagdalpur Odisha India">Jagdalpur Odisha India</option>
          <option value="Jeypore Odisha India">Jeypore Odisha India</option>
          <option value="Jagatsinghpur Odisha India">Jagatsinghpur Odisha India</option>
          <option value="Khurdha Odisha India">Khurdha Odisha India</option>
          <option value="Konark Odisha India">Konark Odisha India</option>
          <option value="Kendrapara Odisha India">Kendrapara Odisha India</option>
          <option value="Koraput Odisha India">Koraput Odisha India</option>
          <option value="Keonjhar Odisha India">Keonjhar Odisha India</option>
          <option value="Mayurbhanj Odisha India">Mayurbhanj Odisha India</option>
          <option value="Muniguda Odisha India">Muniguda Odisha India</option>
          <option value="Malkangiri Odisha India">Malkangiri Odisha India</option>
          <option value="Nayagarh Odisha India">Nayagarh Odisha India</option>
          <option value="Nowrangpur Odisha India">Nowrangpur Odisha India</option>
          <option value="Nuapada Odisha India">Nuapada Odisha India</option>
          <option value="Puri Odisha India">Puri Odisha India</option>
          <option value="Paradip Odisha India">Paradip Odisha India</option>
          <option value="Phulbani Odisha India">Phulbani Odisha India</option>
          <option value="Paralakhemundi Odisha India">Paralakhemundi Odisha India</option>
          <option value="Rayagada Odisha India">Rayagada Odisha India</option>
          <option value="Rourkela Odisha India">Rourkela Odisha India</option>
          <option value="Satapada Odisha India">Satapada Odisha India</option>
          <option value="SakhiGopal Odisha India">SakhiGopal Odisha India</option>
          <option value="Sambalpur Odisha India">Sambalpur Odisha India</option>
          <option value="Sunabeda Odisha India">Sunabeda Odisha India</option>
          <option value="Sundargarh Odisha India">Sundargarh Odisha India</option>
          <option value="Sonepur Odisha India">Sonepur Odisha India</option>
          <option value="Simlipal Odisha India">Simlipal Odisha India</option>
          <option value="Titlagarh Odisha India">Titlagarh Odisha India</option>
          <option value="Talcher Odisha India">Talcher Odisha India</option>
          <option value="Taptapani Odisha India">Taptapani Odisha India</option>
          <option value="dropme">Other Location (If Any)</option>
        </select>
                </div>
              </div>
              <div class="form-group col-md-7">
              <div class="row">
                  <input type="text" name="arrivaldate_taxi" id="arrival" class="form-control datepicker" readonly="readonly" placeholder="Pick-Up Date (If Available) ..." value="" required="">
                  </div>
              </div>
              <div class="form-group col-md-5">
              <div class="row pdlft">
                  <select name="arrivaltime_taxi" class="form-control">
              <option value="00:00 AM" selected="selected">Pick-Up Time</option>
              <option value="00:30 AM">00:30 AM</option>
              <option value="01:00 AM">01:00 AM</option>
              <option value="01:30 AM">01:30 AM</option>
              <option value="02:00 AM">02:00 AM</option>
              <option value="02:30 AM">02:30 AM</option>
              <option value="03:00 AM">03:00 AM</option>
              <option value="03:30 AM">03:30 AM</option>
              <option value="04:00 AM">04:00 AM</option>
              <option value="04:30 AM">04:30 AM</option>
              <option value="05:00 AM">05:00 AM</option>
              <option value="05:30 AM">05:30 AM</option>
              <option value="06:00 AM">06:00 AM</option>
              <option value="06:30 AM">06:30 AM</option>
              <option value="07:00 AM">07:00 AM</option>
              <option value="07:30 AM">07:30 AM</option>
              <option value="08:00 AM">08:00 AM</option>
              <option value="08:30 AM">08:30 AM</option>
              <option value="09:00 AM">09:00 AM</option>
              <option value="09:30 AM">09:30 AM</option>
              <option value="10:00 AM">10:00 AM</option>
              <option value="10:30 AM">10:30 AM</option>
              <option value="11:00 AM">11:00 AM</option>
              <option value="11:30 AM">11:30 AM</option>
              <option value="12:00 PM">12:00 PM</option>
              <option value="12:30 PM">12:30 PM</option>
              <option value="01:00 PM">13:00 PM</option>
              <option value="01:30 PM">13:30 PM</option>
              <option value="02:00 PM">14:00 PM</option>
              <option value="02:30 PM">14:30 PM</option>
              <option value="03:00 PM">15:00 PM</option>
              <option value="03:30 PM">15:30 PM</option>
              <option value="04:00 PM">16:00 PM</option>
              <option value="04:30 PM">16:30 PM</option>
              <option value="05:00 PM">17:00 PM</option>
              <option value="05:30 PM">17:30 PM</option>
              <option value="06:00 PM">18:00 PM</option>
              <option value="06:30 PM">18:30 PM</option>
              <option value="07:00 PM">19:00 PM</option>
              <option value="07:30 PM">19:30 PM</option>
              <option value="08:00 PM">20:00 PM</option>
              <option value="08:30 PM">20:30 PM</option>
              <option value="09:00 PM">21:00 PM</option>
              <option value="09:30 PM">21:30 PM</option>
              <option value="10:00 PM">22:00 PM</option>
              <option value="10:30 PM">22:30 PM</option>
              <option value="11:00 PM">23:00 PM</option>
              <option value="11:30 PM">23:30 PM</option>
              <option value="12:00 AM">00:00 AM</option>
            </select>
                  </div>
              </div>
              <div class="form-group col-md-7">
              <div class="row">
                  <input type="text" name="arrivaldate_taxi" id="arrival" class="form-control datepicker" readonly="readonly" placeholder="Drop Date (If Available) ..." value="" required="">
                  </div>
              </div>
              <div class="form-group col-md-5">
              <div class="row pdlft">
                  <select name="arrivaltime_taxi" class="form-control">
              <option value="00:00 AM" selected="selected">Drop Time</option>
              <option value="00:30 AM">00:30 AM</option>
              <option value="01:00 AM">01:00 AM</option>
              <option value="01:30 AM">01:30 AM</option>
              <option value="02:00 AM">02:00 AM</option>
              <option value="02:30 AM">02:30 AM</option>
              <option value="03:00 AM">03:00 AM</option>
              <option value="03:30 AM">03:30 AM</option>
              <option value="04:00 AM">04:00 AM</option>
              <option value="04:30 AM">04:30 AM</option>
              <option value="05:00 AM">05:00 AM</option>
              <option value="05:30 AM">05:30 AM</option>
              <option value="06:00 AM">06:00 AM</option>
              <option value="06:30 AM">06:30 AM</option>
              <option value="07:00 AM">07:00 AM</option>
              <option value="07:30 AM">07:30 AM</option>
              <option value="08:00 AM">08:00 AM</option>
              <option value="08:30 AM">08:30 AM</option>
              <option value="09:00 AM">09:00 AM</option>
              <option value="09:30 AM">09:30 AM</option>
              <option value="10:00 AM">10:00 AM</option>
              <option value="10:30 AM">10:30 AM</option>
              <option value="11:00 AM">11:00 AM</option>
              <option value="11:30 AM">11:30 AM</option>
              <option value="12:00 PM">12:00 PM</option>
              <option value="12:30 PM">12:30 PM</option>
              <option value="01:00 PM">13:00 PM</option>
              <option value="01:30 PM">13:30 PM</option>
              <option value="02:00 PM">14:00 PM</option>
              <option value="02:30 PM">14:30 PM</option>
              <option value="03:00 PM">15:00 PM</option>
              <option value="03:30 PM">15:30 PM</option>
              <option value="04:00 PM">16:00 PM</option>
              <option value="04:30 PM">16:30 PM</option>
              <option value="05:00 PM">17:00 PM</option>
              <option value="05:30 PM">17:30 PM</option>
              <option value="06:00 PM">18:00 PM</option>
              <option value="06:30 PM">18:30 PM</option>
              <option value="07:00 PM">19:00 PM</option>
              <option value="07:30 PM">19:30 PM</option>
              <option value="08:00 PM">20:00 PM</option>
              <option value="08:30 PM">20:30 PM</option>
              <option value="09:00 PM">21:00 PM</option>
              <option value="09:30 PM">21:30 PM</option>
              <option value="10:00 PM">22:00 PM</option>
              <option value="10:30 PM">22:30 PM</option>
              <option value="11:00 PM">23:00 PM</option>
              <option value="11:30 PM">23:30 PM</option>
              <option value="12:00 AM">00:00 AM</option>
            </select>
                  </div>
              </div>
            <div class="form-group col-md-12">
              <div class="row">
                <select name="transportation_taxi" class="form-control">
          <option selected="selected">Vehicle Type - Cab / Mini Coach / Bus</option>
          <optgroup label="Mini &amp; Sedan Cars">
          <option value="AC Tata Indigo (4+1Driver)">AC Tata Indigo (4+1Driver)</option>
          <option value="AC Swift Dzire (4+1Driver)">AC Swift Dzire (4+1Driver)</option>
          <option value="AC Hyundai Xcent (4+1Driver)">AC Hyundai Xcent (4+1Driver)</option>
          <option value="AC Chevrolet Sail (4+1Driver)">AC Chevrolet Sail (4+1Driver)</option>
          <option value="AC Honda Amaze (4+1Driver)">AC Honda Amaze (4+1Driver)</option>
          <option value="AC Toyota Etios (4+1Driver)">AC Toyota Etios (4+1Driver)</option>
          <option value="AC Honda City (4+1Driver)">AC Honda City (4+1Driver)</option>
          <option value="AC Hyundai Verna (4+1Driver)">AC Hyundai Verna (4+1Driver)</option>
          <option value="AC Maruti Suzuki Ciaz (4+1Driver)">AC Maruti Suzuki Ciaz (4+1Driver)</option>
          <option value="AC Chevrolet Cruze (4+1Driver)">AC Chevrolet Cruze (4+1Driver)</option>
          <option value="AC Corolla Altis (4+1Driver)">AC Corolla Altis (4+1Driver)</option>
          </optgroup>
          <optgroup label="SUVs Cars">
          <option value="AC Chevrolet Enjoy (7+1Driver)">AC Chevrolet Enjoy (7+1Driver)</option>
          <option value="AC Mahindra Scorpio (6+1Driver)">AC Mahindra Scorpio (6+1Driver)</option>
          <option value="AC Chevrolet Tavera (9+1Driver)">AC Chevrolet Tavera (9+1Driver)</option>
          <option value="AC Toyota Innova (7+1Driver)">AC Toyota Innova (7+1Driver)</option>
          <option value="AC Toyota Innova Crysta (7+1Driver)">AC Toyota Innova Crysta (7+1Driver)</option>
                  <option value="AC Toyota Fortuner (7+1Driver)">AC Toyota Fortuner (7+1Driver)</option>
          </optgroup>
          <optgroup label="Premium Cars">
         <option value="Mercedes Benz E250">Mercedes Benz E250</option>
         <option value="Audi A6">Audi A6</option>
         <option value="Jaguar XF">Jaguar XF</option>
        
         <option value="Audi A4">Audi A4</option>
          <option value="Audi Q3">Audi Q3</option>
         <option value="BMW 3 Series">BMW 3 Series</option>
          </optgroup>
          
          <optgroup label="AC Winger">
  <option value="AC 9 Seater Luxury Winger Van">AC 9 Seater Luxury Winger Van</option>
  </optgroup>
  <optgroup label="AC Tempo Traveller">
  <option value="AC 13 Seater Tempo Traveller Cab">AC 13 Seater Tempo Traveller Cab</option>
  <option value="AC 17 Seater Tempo Traveller Cab">AC 17 Seater Tempo Traveller Cab</option>
  <option value="AC 26 Seater Tempo Traveller Cab">AC 26 Seater Tempo Traveller Cab</option>
  </optgroup>
  <optgroup label="AC Luxury SML COACH">
  <option value="AC 13 Seater SML Coach">AC 13 Seater SML Coach</option>
  <option value="AC 18 Seater SML Coach">AC 18 Seater SML Coach</option>
  <option value="AC 28 Seater SML Coach">AC 28 Seater SML Coach</option>
  </optgroup>
  <optgroup label="AC Luxury Coach">
  <option value="AC 44 Seater Luxury Bus">AC 44 Seater Luxury Bus</option>
  <option value="AC 45 Seater Volvo Bus">AC 45 Seater Volvo Bus</option>
  </optgroup>
         
          <optgroup label="AC Luxury Tempo Traveller">
          <option value="AC Luxury Force Traveller (15+1Driver)">AC Luxury Force Traveller (15+1Driver)</option>
          <option value="AC Luxury Force Traveller (18+1Driver)">AC Luxury Force Traveller (18+1Driver)</option>
          </optgroup>
          
        </select>
                </div>
              </div>
              <div class="form-group col-md-12">
                  <div class="row">
              Need Hotel Booking&nbsp;&nbsp;<input type="radio" name="yes" value="yes">&nbsp;Yes&nbsp;&nbsp;<input type="radio" name="yes" value="No">&nbsp;No
              </div></div>
            <textarea class="form-control" placeholder="Taxi Requirements & Preferences ( If Any )..."></textarea><br>
              <h4>Your Contact Information</h4>
              <div class="form-group col-md-12">
              <div class="row">
                  <input type="text" placeholder="Full Name..." class="form-control">
                  </div>
              </div>
               <div class="form-group col-md-12">
              <div class="row">
                  <input type="text" placeholder="Email Id..." class="form-control">
                  </div>
              </div>
              <div class="form-group col-md-12">
              <div class="row">
                  <input type="tel" placeholder="Contact Number..." class="form-control">
                  </div>
              </div>
              <div class="form-group col-md-4">
              <div class="row">
                  <input type="text" class="form-control" value="7654">
                  </div>
              </div>
               <div class="form-group col-md-8">
              <div class="row pdlft">
                  <input type="text" placeholder="captcha Code Here..." class="form-control">
                  </div>
              </div>
              <button type="submit" class="btn  btn-block custom">Submit</button>
            </form>
        </div>
        
        
    </div>
       
        <?php
include("footer.php");
?>
    </body>
    </html>