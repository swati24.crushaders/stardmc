<?php
session_start();
include ('dbcon.php');
include('header.php');
?>

   <div class="inner-banner">
       <img src="images/visa-banner.jpg" alt="" class="img-responsive">
       <h3>VISA</h3>
    </div>
 <div class="clearfix"></div>

<div class="head-bg">
<div class="container">
<ul class="breadcrumb">
  <li><a href="index.html">Home</a></li>
  <li><a href="#">Visa</a></li>
</ul></div>
    </div>
        <div class="clearfix"></div>
        <div class="container">
            <div class="magntb">
    <div class="col-md-12">
        <div class="row visa">
            <?php
            $sql="select * from add_page where status=1 and page_title='visa'";
            $res=mysqli_query($con,$sql);
            $rowcnt=mysqli_num_rows($res);
            $x=1;
        if($rowcnt>0){
            
                while($query=mysqli_fetch_array($res))
                {
                ?>
                 <?=$query['page_content']; ?>
        </div>
    </div>
            
            <?php
                }
                $x++;
        }
        else{
            echo 'Result not found';
        }
            ?>
           

<div class="col-md-12">
    <div class="row">
        <div class="mrntp">
            <button type="button" class="btn btn-info" id="pb" data-toggle="" data-target="#passport">Passport</button>
            <button type="button" class="btn btn-info" id="vb" data-toggle="" data-target="#visa">Visa</button>
    <!--<a href="#demo" class="btn btn-info" data-toggle="" data-parent="#demo">Passport</a>-->
    <!--<a href="#demo1" class="btn btn-info" data-toggle="" data-parent="#demo1">Visa</a>-->
  <div id="passport" class="collapse">
    <div class="psprt-design">
        <form name="passport_form" id="passport_form" method="POST">
            <div class="form-group">
                <div class="col-md-6">
                    <input type="text" placeholder="Full Name" class="form-control" name="f_name" id="f_name" required="required">
                </div>
                <div class="col-md-6">
                    <input type="email" placeholder="Email" class="form-control" name="email" id="email" required="required">
                </div>
                <div class="clearfix"></div>
            </div><br>
            <div class="form-group">
                <div class="col-md-6">
                    <input type="tel" placeholder="mobile Number" class="form-control" 
                    name="mobile_number" id="mobile_number" required="required">
                </div>
                <div class="col-md-6">
                    <input type="text" placeholder="Origin City" class="form-control" name="origin_city" id="origin_city" required="required">
                </div>
                <div class="clearfix"></div><br>
            </div>
              <div class="form-group">
                <div class="col-md-12">
                    <textarea placeholder="Requirement Message(if any...)" class="form-control" name="passport_msg" id="passport_msg"></textarea>
                </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <div class="form-group">
                <button type="submit" class="btn btn-info submit" name="passport_sbmt" id="passport_sbmt" style="margin-top:10px;">SUBMIT</button></div></div>
        </form>
        <div class="clearfix"></div>
    </div>
  </div>
  <div id="visa" class="collapse">
      <div class="vsa-design">
        <form name="visa_form" id="visa_form" method="POST">
            <div class="col-md-6">
                <div class="form-group">
                    <select id="country" name="country_visa" required="required" class="form-control">
              <option selected="selected" value="">Select Visa for</option>
              <option value="Afghanistan">Afghanistan</option>
              <option value="Albania">Albania</option>
              <option value="Algeria">Algeria</option>
              <option value="American Samoa">American Samoa</option>
              <option value="Angola">Angola</option>
              <option value="Anguilla">Anguilla</option>
              <option value="Antartica">Antartica</option>
              <option value="Antigua and Barbuda">Antigua and Barbuda</option>
              <option value="Argentina">Argentina</option>
              
            </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <select name="visa_type" id="visa_type" class="form-control">
                        <option selected="selected" value>Select Visa Type</option>
                        <option value="Tourist Visa">Tourist Visa</option>
                        <option value="Business Visa">Business Visa</option>
                        <option value="Student Visa">Student Visa</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <input type="text" placeholder="Full Name" class="form-control" name="visa_fname" id="visa_fname" required="required">
                </div></div>
                <div class="col-md-6">
                    <div class="form-group">
                    <input type="email" placeholder="Email" class="form-control" name="visa_email" id="visa_email" required="required">
                </div></div>
                <div class="clearfix"></div>

            <div class="col-md-6">
                <div class="form-group">
                    <input type="tel" placeholder="mobile Number" class="form-control" name="visa_number" id="visa_number" required="required">
                </div></div>
                <div class="col-md-6">
                    <div class="form-group">
                    <input type="text" placeholder="Passport issue place" class="form-control" name="issue_place" id="issue_place" required="required">
                </div></div>
                <div class="clearfix"></div><br>
              <div class="col-md-12">
                <div class="form-group">
                    <textarea placeholder="Requirement Message(if any...)" class="form-control" name="visa_msg" id="visa_msg" required="required"></textarea>
                </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                <button type="submit" class="btn btn-info submit" name="visa_sbmt" id="visa_sbmt" style="margin-top:10px;">SUBMIT</button></div></div>
        </form>
        <div class="clearfix"></div>
    </div>
</div></div></div>
    
</div>
        </div>
        </div>
        <div class="clearfix"></div>
        <?php
include("footer.php");
?>
<script>
   $( document ).ready(function() {
    $("#passport_form").submit(function (e) {
    	e.preventDefault();
		$.ajax({
			type: $('#passport_form').attr('method'),
			url: 'pasprt_ajax.php',
			data: $('#passport_form').serialize(),
			success: function (data) {
				alertify.alert(data);
				$("#passport_form")[0].reset();
				//document.getElementById("cform").reset();
			},
			error: function (data) {
				console.log('An error occurred.');
			},
		});
        return false;
    });
    
    $("#visa_form").submit(function (e) {
    	e.preventDefault();
		$.ajax({
			type: $('#visa_form').attr('method'),
			url: 'visa_ajax.php',
			data: $('#visa_form').serialize(),
			success: function (data) {
				alertify.alert(data);
				$("#visa_form")[0].reset();
				//document.getElementById("cform").reset();
			},
			error: function (data) {
				console.log('An error occurred.');
			},
		});
        return false;
    });
    
   });
     
    $(document).ready(function(){
           
           $('#pb').click(function(){
               //$('#passport').hide();
               
              $('#passport').slideToggle(1000);
               $('#visa').hide();
               
           });
           
           $('#vb').click(function(){
              $('#visa').slideToggle(1000);
               $('#passport').hide();
           });
           
       });
</script>
  
    </body>
    </html>