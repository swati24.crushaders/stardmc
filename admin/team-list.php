<?php
session_start();
include("dbcon.php");
if($_SESSION['login'])
{
include("header.php");
?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    <h1>Tables</h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Data table</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Sl. No.</th>
                  <th>Name</th>
                  <th>Designation</th>
                  <th>Contact-number</th>
                  <th>Email</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                 <?php
                  $qry=mysqli_query($con,"select * from our_team where status=1");
                  $x=1;
                  while($result=mysqli_fetch_array($qry))
                  {
                  ?>
                    <tr class="gradeX">
                      <td width="5%"><?php echo $x; ?></td>
                      <td width="20%"><?=$result['name']; ?></td>
                      <td width="20%"><?=$result['designation']; ?></td>
                      <td width="20%"><?=$result['phone']; ?></td>
                      <td width="20%"><?=$result['email']; ?></td>
                      <td width="15%"><img src="team-images/<?=$result['image']; ?>" alt="img" style="width:50px; height:50px;" /></td>
                      <td width="10%">
                        <a href="add-team.php?id=<?=$result['id']; ?>"><span class="edit"><i class="icon-edit"></i></span></a>
                        <a href="save.php?action=team_delete&id=<?=$result['id']; ?>"><span class="delete"><i class="icon-remove-circle"></i></span></a>
                      </td>
                    </tr>
                <?php
                $x++;
                  }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
 include("footer.php");
}
else
{
echo '<script> location.href="index.php"; </script>';
}
?>