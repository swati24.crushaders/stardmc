<?php
session_start();
if($_SESSION['login'])
{
include("header.php");
?>
<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
    <div class="quick-actions_homepage">
      <ul class="quick-actions">
        <li class="bg_lb"> <a href="index.html"> <i class="icon-dashboard"></i> <span class="label label-important">20</span> My Dashboard </a> </li>
        <li class="bg_lg span3"> <a href="charts.html"> <i class="icon-signal"></i> Charts</a> </li>
        <li class="bg_ly"> <a href="widgets.html"> <i class="icon-inbox"></i><span class="label label-success">101</span> Widgets </a> </li>
        <li class="bg_lo"> <a href="tables.html"> <i class="icon-th"></i> Tables</a> </li>
        <li class="bg_ls"> <a href="grid.html"> <i class="icon-fullscreen"></i> Full width</a> </li>
        <li class="bg_lo span3"> <a href="form-common.html"> <i class="icon-th-list"></i> Forms</a> </li>
        <li class="bg_ls"> <a href="buttons.html"> <i class="icon-tint"></i> Buttons</a> </li>
        <li class="bg_lb"> <a href="interface.html"> <i class="icon-pencil"></i>Elements</a> </li>
        <li class="bg_lg"> <a href="calendar.html"> <i class="icon-calendar"></i> Calendar</a> </li>
        <li class="bg_lr"> <a href="error404.html"> <i class="icon-info-sign"></i> Error</a> </li>

      </ul>
    </div>
<!--End-Action boxes-->    

<!--Chart-box-->    
    <div class="row-fluid">
  
    </div>
<!--End-Chart-box--> 
    <hr/>
   <!--<div class="row-fluid">-->
   <!--   <div class="span12">-->
   <!--     <div class="widget-box widget-calendar">-->
   <!--       <div class="widget-title"> <span class="icon"><i class="icon-calendar"></i></span>-->
   <!--         <h5>Calendar</h5>-->
   <!--         <div class="buttons"> <a id="add-event" data-toggle="modal" href="#modal-add-event" class="btn btn-inverse btn-mini"><i class="icon-plus icon-white"></i> Add new event</a>-->
   <!--           <div class="modal hide" id="modal-add-event">-->
   <!--             <div class="modal-header">-->
   <!--               <button type="button" class="close" data-dismiss="modal">×</button>-->
   <!--               <h3>Add a new event</h3>-->
   <!--             </div>-->
   <!--             <div class="modal-body">-->
   <!--               <p>Enter event name:</p>-->
   <!--               <p>-->
   <!--                 <input id="event-name" type="text" />-->
   <!--               </p>-->
   <!--             </div>-->
   <!--             <div class="modal-footer"> <a href="#" class="btn" data-dismiss="modal">Cancel</a> <a href="#" id="add-event-submit" class="btn btn-primary">Add event</a> </div>-->
   <!--           </div>-->
   <!--         </div>-->
   <!--       </div>-->
   <!--       <div class="widget-content">-->
   <!--         <div class="panel-left">-->
   <!--           <div id="fullcalendar"></div>-->
   <!--         </div>-->
   <!--         <div id="external-events" class="panel-right">-->
   <!--           <div class="panel-title">-->
   <!--             <h5>Drag Events to the calander</h5>-->
   <!--           </div>-->
   <!--           <div class="panel-content">-->
   <!--             <div class="external-event ui-draggable label label-inverse">My Event 1</div>-->
   <!--             <div class="external-event ui-draggable label label-inverse">My Event 2</div>-->
   <!--             <div class="external-event ui-draggable label label-inverse">My Event 3</div>-->
   <!--             <div class="external-event ui-draggable label label-inverse">My Event 4</div>-->
   <!--             <div class="external-event ui-draggable label label-inverse">My Event 5</div>-->
   <!--           </div>-->
   <!--         </div>-->
   <!--       </div>-->
   <!--     </div>-->
   <!--   </div>-->
   <!-- </div>-->
  </div>
</div>

<!--end-main-container-part-->
<?php
 include("footer.php");
}
else
{
echo '<script> location.href="index.php"; </script>';
}
?>