<?php
session_start();
include("dbcon.php");
if($_SESSION['login'])
{
    if(isset($_GET['id']) && $_GET['id']!=''){
	$iti_id = $_GET['id'];
	$iti_query = mysqli_query($con,"select * from itinerary_list where id='$iti_id'");
	$iti_result = mysqli_fetch_assoc($iti_query);
	$id = $iti_result['id'];
	$iti_titile = $iti_result['iti_title'];
	$iti_image = $iti_result['iti_img'];
	$iti_destination = $iti_result['iti_destination'];
	$iti_desc = $iti_result['iti_desc'];
	$btn_name = "edit_itinerary";
	$btn_value="Update";
	$form_id="itinerary_form";
}else{
	$id = "";
	$iti_titile = "";
	$iti_image = "";
	$iti_destination = "";
	$iti_desc = "";
	$btn_name = "add_itinerary";
	$btn_value="Submit";
	$form_id="itinerary_validate";
}

include("header.php");
?>
<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="tip-bottom">Itinerary</a> <a class="current">Add Itinerary</a> </div>
  <h1>Add Itinerary</h1>
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Personal-info</h5>
        </div>
        <div class="widget-content">
          <form action="save.php?action=<?php echo $btn_name; ?>" method="post" class="form-horizontal" id="<?php echo $form_id; ?>" enctype="multipart/form-data">
              <input type="hidden" name="req_id" value="<?php echo $_REQUEST['id']; ?>" />
            <div class="control-group">
              <label class="control-label">Itinerary Title</label>
              <div class="controls">
                <input type="text" class="span12" name="iti_name" id="iti_name" value="<?php echo $iti_titile; ?>" required="" />
              </div>
            </div>
            <div class="row-fluid">
                <div class="control-group">
                  <label class="control-label">Destination</label>
                  <div class="controls">
                      <?php
                      if($iti_destination!="")
                      {
                      ?>
                    <select class="span6" name="iti_route" id="iti_route" required="">
                      <option value="">Select Destination</option>
                      <?php
                      $qry=mysqli_query($con,"Select * from add_destination");
                      while($result=mysqli_fetch_array($qry))
                      {
                      ?>
                        <option value="<?=$result['id']; ?>" <?php if($result['id']==$iti_destination){ echo "Selected"; } ?>><?=$result['destination_name']; ?></option>
                      <?php
                      }
                      ?>
                    </select>
                    <?php
                      }
                      else{
                    ?>
                     <select class="span6" name="iti_route" id="iti_route" required="">
                      <option value="">Select Destination</option>
                      <?php
                      $qry=mysqli_query($con,"Select * from add_destination");
                      while($result=mysqli_fetch_array($qry))
                      {
                      ?>
                        <option value="<?=$result['id']; ?>"><?=$result['destination_name']; ?></option>
                      <?php
                      }
                      ?>
                    </select>
                    <?php
                      }
                    ?>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Image Upload</label>
                  <div class="controls">
                    <input type="file" class="span6" name="image" id="image" value="<?php echo $iti_image; ?>"/>
                     <?php
                      if($iti_image!="")
                      {
                      ?>
                     <span>&nbsp;&nbsp;<img src="itinerary-images/<?php echo $iti_image; ?>" style="width:150px;height:auto;" /></span>
                     <input type="hidden" value="<?php echo $iti_image; ?>" name="prev_image" />
                     <?php
                      }
                     ?>
                  </div>
                </div>
            </div>
            <div class="control-group">
                 <label class="control-label">Descriptions</label>
                <div class="controls">
                    <textarea class="textarea_editor span12" rows="6" placeholder="Enter text ..." name="desc" id="desc" required=""><?php echo $iti_desc; ?></textarea>
                </div>
            </div>
            <div class="form-actions">
              <button type="submit" name="<?php echo $btn_name; ?>" class="btn btn-success"><?php echo $btn_value; ?></button>
            </div>
          </form>
        </div>
      </div>
  </div>
</div></div>
<?php
 include("footer.php");
}
else
{
echo '<script> location.href="index.php"; </script>';
}
?>