<?php
session_start();
include("dbcon.php");
if($_SESSION['login'])
{
if(isset($_GET['id']) && $_GET['id']!=''){
	$tour_id = $_GET['id'];
	$tour_query = mysqli_query($con,"select * from tour_table where id='$tour_id'");
	$tour_result = mysqli_fetch_assoc($tour_query);
	$id = $tour_result['id'];
	$destination = $tour_result['destination'];
	$deparature = $tour_result['deparature'];
	$desription = $tour_result['desription'];
	$main_image= $tour_result['main_image'];
	$btn_name = "edit_tour";
	$btn_value="Update";
	$form_id="tour_validate";
}else{
	$id = "";
	$destination = "";
	$deparature = "";
	$desription = "";
	$main_image = "";
	$btn_name = "add_tour";
	$btn_value="Submit";
}
include("header.php");
?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="#" class="current">Tour</a> </div>
    <h1>Add Tour</h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
        <div class="span2"></div>
        <div class="span8">
            <div class="widget-box">
                <div class="widget-content nopadding">
                    <form  action="save.php?action=<?php echo $btn_name; ?>" class="form-horizontal" method="post" name="select-multiple" enctype="multipart/form-data" id="<?php echo $form_id; ?>">
                        <input type="hidden" name="id" id="route_id" value="<?=$id;?>" required="">
                        <div class="control-group">
                            <label class="control-label">Destination</label>
                            <div class="controls">
                                <input type="text" name="tour_dest" id="tour_dest" value="<?=$destination; ?>" class="span8" required>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Deparature</label>
                            <div class="controls">
                                <input type="text" name="deparature" id="deparature" value="<?=$deparature; ?>" class="span8" required/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Description</label>
                            <div class="controls">
                                <textarea class="textarea_editor span12" rows="6" placeholder="Enter text ..." name="desc" id="desc" required><?php echo  $desription; ?></textarea>
                            </div>
                        </div>
                         <div class="control-group">
                  <label class="control-label">Main-Image</label>
                  <div class="controls">
                    <input type="file" class="span6" name="main_image" id="main_image" value="<?php echo $main_image; ?>"/>
                     <?php
                      if($main_image!="")
                      {
                      ?>
                     <span>&nbsp;&nbsp;<img src="team-images/<?php echo $main_image; ?>" style="width:150px;height:auto;" /></span>
                     <input type="hidden" value="<?php echo $main_image; ?>" name="prev_image" />
                     <?php
                      }
                     ?>
                  </div>
                </div> 
                <!--<div class="control-group">-->
                    
                <!--</div>-->
                        <div class="form-actions">
                            <input type="submit" value="<?=$btn_value; ?>" name="<?=$btn_name; ?>" id="dest_add" class="btn btn-success">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<?php
 include("footer.php");
}
else
{
echo '<script> location.href="index.php"; </script>';
}
?>
<script>
var arr = [];

function removeRow(r) {
  var index = arr.indexOf(r);
  if (index > -1) {
    arr.splice(index, 1);
  }
}
$('#multiple-date-select').multiDatesPicker({
  onSelect: function(datetext) {
    let table = $('#table-data');
    let rowLast = table.data('lastrow');
    let rowNext = rowLast + 1;
    let r = table.find('tr').filter(function() {
      return ($(this).data('date') == datetext);
    }).eq(0);
    // a little redundant checking both here 
    if (!!r.length && arr.includes(datetext)) {
      removeRow(datetext);
      r.remove();
    } else {
      // not found so add it
      let col = $('<td></td>').html(datetext);
      let row = $('<tr></tr>');
      row.data('date', datetext);
      row.attr('id', 'newrow' + rowNext);
      row.append(col).appendTo(table);
      table.data('lastrow', rowNext);
      arr.push(datetext);
    }
  }
});
</script>