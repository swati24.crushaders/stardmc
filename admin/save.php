<?php
session_start();
include("dbcon.php");
require 'class/SMTPMailer.php';
$mail = new SMTPMailer();

/***************************Destination********************************/
if(isset($_REQUEST['action']) && $_REQUEST['action']=="route"){
    $destination=htmlspecialchars($_POST['dest']);
    $price=htmlspecialchars($_POST['price']);
    $sql = mysqli_query($con,"INSERT INTO add_destination (destination_name, price) VALUES ('$destination', '$price')");
    if($sql){
		echo "Data Inserted Successfully";
    }
    else{
       
		echo "Data Failed";
    }
}

if(isset($_REQUEST['action']) && $_REQUEST['action']=="update_route"){
    $destination=htmlspecialchars($_POST['dest']);
    $price=htmlspecialchars($_POST['price']);
    $route_id=htmlspecialchars($_POST['route_id']);
    
    $sql = mysqli_query($con,"Update add_destination SET destination_name='$destination', price='$price' where id='$route_id'");
    if($sql){
		echo "Data Updated Successfully";
    }
    else{
       
		echo "Data Failed";
    }
}
if(isset($_REQUEST['action']) && $_REQUEST['action']=="dest_delete"){
    $req_id=$_REQUEST["id"];
    $sql=mysqli_query($con,"DELETE FROM add_destination WHERE id='$req_id'");
    if($sql){
		echo "<script>alert('Data Deleted Successfully')</script>";
		header("Location:add-destination.php");
    }
    else{
       echo "<script>alert('Data Failed')</script>";
		header("Location:add-destination.php");
    }
}

if(isset($_REQUEST['action']) && $_REQUEST['action']=="add_itinerary"){
    extract($_POST);
    $target_dir = "itinerary-images/";
    $target_file = $target_dir . basename($_FILES["image"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $adimage=$_FILES["image"]["name"];
    $chk=move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
    if($chk){
        $sql = mysqli_query($con,"INSERT INTO itinerary_list (iti_title, iti_img, iti_destination, iti_desc) VALUES ('$iti_name', '$adimage', '$iti_route', '$desc')");
        if($sql){
    		echo "<script>alert('Data Inserted Successfully')</script>";
    		echo "<script>location.href='add-itinerary.php'</script>";
        }
        else{
            echo "<script>alert('Data Failed')</script>";
    		echo "<script>location.href='add-itinerary.php'</script>";
        }
    }
}

if(isset($_REQUEST['action']) && $_REQUEST['action']=="edit_itinerary"){
    extract($_POST);
    if(isset($_FILES["image"]["name"])){
        $target_dir = "itinerary-images/";
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $adimage=$_FILES["image"]["name"];
        $chk=move_uploaded_file($_FILES["image"]["tmp_name"], $target_file); 
    }
    else{
        $adimage=$prev_image;
    }
    $sql = mysqli_query($con,"Update itinerary_list SET iti_title='$iti_name', iti_img='$adimage', iti_destination='$iti_route', iti_desc='$desc' where id='$req_id'");
    if($sql){
	echo "Data Updated Successfully";
	echo "<script>location.href='itinerary-list.php'</script>";
    }
    else{
		echo "Data Failed";
		echo "<script>location.href='itinerary-list.php'</script>";
    }
}
/*****************add-team********/
if(isset($_REQUEST['action']) && $_REQUEST['action']=="add_team"){
    extract($_POST);
    
    $target_dir = "team-images/";
    $target_file = $target_dir . basename($_FILES["profile"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $adimage=$_FILES["profile"]["name"];
    $chk=move_uploaded_file($_FILES["profile"]["tmp_name"], $target_file);
    if($chk){
        $sql = mysqli_query($con,"INSERT INTO our_team (name, designation, phone, email, image) VALUES ('$name', '$designation', '$phn', '$email','$adimage')");
        //echo "INSERT INTO our_team (name, designation, phone, email, image) VALUES ('$name', '$designation', '$phn', '$email','$adimage')";
        //exit;
        if($sql){
    		echo "<script>alert('Data Inserted Successfully')</script>";
    		echo "<script>location.href='add-team.php'</script>";
        }
        else{
            echo "<script>alert('Data Failed')</script>";
    		echo "<script>location.href='add-team.php'</script>";
        }
    }
}

if(isset($_REQUEST['action']) && $_REQUEST['action']=="edit_team"){
    extract($_POST);
    
    //var_dump($_FILES["profile"]["name"]);exit;
    if(isset($_FILES["profile"]["name"]) && $_FILES["profile"]["name"]!="" ){
        $target_dir = "team-images/";
        $target_file = $target_dir . basename($_FILES["profile"]["name"]);
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $adimage=$_FILES["profile"]["name"];
        $chk=move_uploaded_file($_FILES["profile"]["tmp_name"], $target_file); 
    }
    else{
        $adimage=$prev_image;
    }
    $sql = mysqli_query($con,"Update our_team SET name='$name', designation='$designation', phone='$phn', email='$email',image='$adimage' where id='$req_id'");
    if($sql){
	echo "Data Updated Successfully";
	echo "<script>location.href='team-list.php'</script>";
    }
    else{
		echo "Data Failed";
		echo "<script>location.href='team-list.php'</script>";
    }
}
if(isset($_REQUEST['action']) && $_REQUEST['action']=="team_delete"){
    $req_id=$_REQUEST["id"];
    $sql=mysqli_query($con,"Update our_team SET status=0 WHERE id='$req_id'");
    if($sql){
		echo "<script>alert('Data Deleted Successfully')</script>";
		header("Location:team-list.php");
    }
    else{
       echo "<script>alert('Data Failed')</script>";
		header("Location:team-list.php");
    }
}
/*********add pages**************/
if(isset($_REQUEST['action']) && $_REQUEST['action']=="add_page"){
    extract($_POST);
    $sql=mysqli_query($con,"INSERT INTO add_page (page_title, page_content) VALUES ('$page_title', '$page_desc')");
    if($sql){
    		echo "<script>alert('Data Inserted Successfully')</script>";
    		echo "<script>location.href='add-page.php'</script>";
        }
        else{
            echo "<script>alert('Data Failed')</script>";
    		echo "<script>location.href='add-page.php'</script>";
        }
}
if(isset($_REQUEST['action']) && $_REQUEST['action']=="edit_page"){
    extract($_POST);
    $sql = mysqli_query($con,"Update add_page SET page_title='$page_title', page_content='$page_desc' where id='$req_id'");
    if($sql){
	echo "Data Updated Successfully";
	echo "<script>location.href='page-list.php'</script>";
    }
    else{
		echo "Data Failed";
		echo "<script>location.href='page-list.php'</script>";
    }
}
if(isset($_REQUEST['action']) && $_REQUEST['action']=="page_delete"){
    $req_id=$_REQUEST["id"];
    $sql=mysqli_query($con,"Update add_page SET status=0 WHERE id='$req_id'");
    if($sql){
		echo "<script>alert('Data Deleted Successfully')</script>";
		header("Location:page-list.php");
    }
    else{
       echo "<script>alert('Data Failed')</script>";
		header("Location:page-list.php");
    }
}
/****************add-tour********/
if(isset($_REQUEST['action']) && $_REQUEST['action']=="add_tour"){
    extract($_POST);
    var_dump($_POST);exit;
    $target_dir = "itinerary-images/";
    $target_file = $target_dir . basename($_FILES["main_image"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $adimage=$_FILES["main_image"]["name"];
    $chk=move_uploaded_file($_FILES["main_image"]["tmp_name"], $target_file);
    if($chk){
        $sql = mysqli_query($con,"INSERT INTO tour_table ( destination , deparature, desription, main_image) VALUES ('$destination', '$deparature', '$desc', '$adimage')");
        if($sql){
    		echo "<script>alert('Data Inserted Successfully')</script>";
    		echo "<script>location.href='add-tour.php'</script>";
        }
        else{
            echo "<script>alert('Data Failed')</script>";
    		echo "<script>location.href='add-tour.php'</script>";
        }
    }
}
?>