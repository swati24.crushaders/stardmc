<!--Footer-part-->
<div class="row-fluid">
  <div id="footer" class="span12"> 2019 &copy; StarDMC </div>
</div>

<!--end-Footer-part-->
<!--<script src="js/excanvas.min.js"></script> -->
<!--<script src="js/matrix.popover.js"></script> -->
<!--<script src="js/jquery.flot.min.js"></script> -->
<!--<script src="js/jquery.flot.resize.min.js"></script> -->
<!--<script src="js/jquery.peity.min.js"></script> -->
<!--<script src="js/matrix.chat.js"></script> -->
<!--<script src="js/jquery.wizard.js"></script> -->
<script src="js/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdn.rawgit.com/dubrox/Multiple-Dates-Picker-for-jQuery-UI/master/jquery-ui.multidatespicker.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script>
<!--<script src="js/bootstrap-datepicker.js"></script> -->
<script src="js/matrix.js"></script> 
<!--<script src="js/fullcalendar.min.js"></script> -->
<!--<script src="js/matrix.calendar.js"></script> -->
<script src="js/jquery.validate.js"></script> 
<script src="js/matrix.form_validation.js"></script> 
<script src="js/jquery.uniform.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/jquery.dataTables.min.js"></script> 
<script src="js/matrix.tables.js"></script> 
<script src="js/matrix.interface.js"></script> 
<script src="js/alertify.min.js"></script>
<script src="js/bootstrap-colorpicker.js"></script> 
<script src="js/jquery.toggle.buttons.js"></script> 
<script src="js/masked.js"></script> 
<script src="js/matrix.form_common.js"></script> 
<script src="js/wysihtml5-0.3.0.js"></script> 
<script src="js/bootstrap-wysihtml5.js"></script> 
<script src="js/jquery.peity.min.js"></script> 

<script type="text/javascript">
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }
  

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}

$( "#basic_validate" ).submit(function( event ) {
    event.preventDefault();
    var dest = document.getElementById("destination").value;
    var price = document.getElementById("price").value;
    if(dest !="" || price!="")
    {
        $.ajax({
              type: "POST",
              url: 'save.php?action=route',
              data:{dest:dest, price:price},
              success: function(data)
                {
                  alertify.alert(data).setHeader('<img src="http://demo.ctslproject.com/stardmc.com/images/logo.jpg" alt="Logo" title="Logo">');
                  window.setTimeout(function(){location.reload()},2000)
                }
        });
    }
});

$( "#update_form" ).submit(function( event ) {
    event.preventDefault();
    var route_id = document.getElementById("route_id").value;
    var dest = document.getElementById("destination").value;
    var price = document.getElementById("price").value;
    if(dest !="" || price!="")
    {
        $.ajax({
              type: "POST",
              url: 'save.php?action=update_route',
              data:{dest:dest, price:price, route_id:route_id},
              success: function(data)
                {
                  alertify.alert(data).setHeader('<img src="http://demo.ctslproject.com/stardmc.com/images/logo.jpg" alt="Logo" title="Logo">');
                  setTimeout(function() {window.location.href = "add-destination.php";}, 2000);
                }
        });
    }
});

</script>
<script>
	$('.textarea_editor').wysihtml5();
</script>
</body>
</html>
