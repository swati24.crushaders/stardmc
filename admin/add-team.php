<?php
session_start();
include("dbcon.php");
if($_SESSION['login'])
{
    if(isset($_GET['id']) && $_GET['id']!=''){
	$team_id = $_GET['id'];
	$team_query = mysqli_query($con,"select * from our_team where id='$team_id'");
	$team_result = mysqli_fetch_assoc($team_query);
	$id = $team_result['id'];
	$name = $team_result['name'];
	$designation = $team_result['designation'];
	$contact = $team_result['phone'];
	$email = $team_result['email'];
	$image = $team_result['image'];
	$btn_name = "edit_team";
	$btn_value="Update";
	$form_id="team_form";
}else{
	$id = "";
	$name = "";
	$designation = "";
	$contact = "";
	$email = "";
	$image = "";
	$btn_name="add_team";
	$btn_value="Submit";
	$form_id="team_validate";
}

include("header.php");
?>
<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="tip-bottom">Our Team</a> <a class="current">Add Team</a> </div>
  <h1>Add Team</h1>
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Personal-info</h5>
        </div>
        <div class="widget-content">
          <form action="save.php?action=<?php echo $btn_name; ?>" method="post" class="form-horizontal" id="<?php echo $form_id; ?>" enctype="multipart/form-data">
              <input type="hidden" name="req_id" value="<?php echo $id; ?>" />
            <div class="control-group">
              <label class="control-label">Name</label>
              <div class="controls">
                <input type="text" class="span12" name="name" id="name" value="<?php echo $name; ?>" required="" />
              </div>
            </div>
            <div class="row-fluid">
                <div class="control-group">
                  <label class="control-label">Designation</label>
             <div class="controls">
                 <input type="text" class="span12" name="designation" id="designation" value="<?php echo $designation; ?>" required="" />
             </div>
                </div>
            </div>
            <div class="control-group">
              <label class="control-label">Phone Number</label>
              <div class="controls">
                <input type="number" class="span12" name="phn" id="phn" value="<?php echo $contact; ?>" required="" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Email</label>
              <div class="controls">
                <input type="email" class="span12" name="email" id="email" value="<?php echo $email; ?>" required="" />
              </div>
            </div>
            <div class="control-group">
                  <label class="control-label">Image Upload</label>
                  <div class="controls">
                    <input type="file" class="span6" name="profile" id="image" value="<?php echo $image; ?>"/>
                     <?php
                      if($image!="")
                      {
                      ?>
                     <span>&nbsp;&nbsp;<img src="team-images/<?php echo $image; ?>" style="width:150px;height:auto;" /></span>
                     <input type="hidden" value="<?php echo $image; ?>" name="prev_image" />
                     <?php
                      }
                     ?>
                  </div>
                </div>
            <div class="form-actions">
              <button type="submit" name="<?php echo $btn_name; ?>" class="btn btn-success"><?php echo $btn_value; ?></button>
            </div>
          </form>
        </div>
      </div>
  </div>
</div></div>
<?php
 include("footer.php");
}
else
{
echo '<script> location.href="index.php"; </script>';
}
?>