<?php
session_start();
include("dbcon.php");
if($_SESSION['login'])
{
if(isset($_GET['id']) && $_GET['id']!=''){
	$dest_id = $_GET['id'];
	$dest_query = mysqli_query($con,"select * from add_destination where id='$dest_id'");
	$dest_result = mysqli_fetch_assoc($dest_query);
	$id = $dest_result['id'];
	$dest_name = $dest_result['destination_name'];
	$dest_price = $dest_result['price'];
	$btn_name = "editdestination";
	$btn_value="Update";
	$form_id="update_form";
}else{
	$id = "";
	$dest_name = "";
	$dest_price = "";
	$btn_name = "adddestination";
	$btn_value="Submit";
	$form_id="basic_validate";
}
include("header.php");
?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="#" class="current">Destination</a> </div>
    <h1>Add Destination</h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
        <div class="span2"></div>
        <div class="span8">
            <div class="widget-box">
                <div class="widget-content nopadding">
                    <form class="form-horizontal" method="post" name="basic_validate" id="<?=$form_id; ?>" novalidate="novalidate">
                        <input type="hidden" name="id" id="route_id" value="<?=$id;?>" required="">
                        <div class="control-group">
                            <label class="control-label">Destination</label>
                            <div class="controls">
                                <input type="text" name="destination" id="destination" value="<?=$dest_name; ?>" class="span8">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Price</label>
                            <div class="controls">
                                <input type="text" name="price" id="price" value="<?=$dest_price; ?>" class="span8" />
                            </div>
                        </div>
                        <div class="form-actions">
                            <input type="submit" value="<?=$btn_value; ?>" name="<?=$btn_name; ?>" id="dest_add" class="btn btn-success">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Destination Table</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                    <th>Sl. No.</th>
                    <th>Destination Name</th>
                    <th>Price</th>
                    <th>Create Date</th>
                    <th>Action</th>
                </tr>
              </thead>
              <tbody>
                  <?php
                  $qry=mysqli_query($con,"select * from add_destination");
                  $x=1;
                  while($result=mysqli_fetch_array($qry))
                  {
                  ?>
                <tr class="gradeX">
                  <td><?php echo $x; ?></td>
                  <td><?=$result['destination_name']; ?></td>
                  <td><?=$result['price']; ?></td>
                  <td class="text-center"><?=$result['create_date']; ?></td>
                  <td>
                    <a href="add-destination.php?id=<?=$result['id']; ?>"><span class="edit"><i class="icon-edit"></i></span></a>
                    <a href="save.php?action=dest_delete&id=<?=$result['id']; ?>"><span class="delete"><i class="icon-remove-circle"></i></span></a>
                  </td>
                </tr>
                <?php
                $x++;
                  }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>
<?php
 include("footer.php");
}
else
{
echo '<script> location.href="index.php"; </script>';
}
?>