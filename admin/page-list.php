<?php
session_start();
include("dbcon.php");
if($_SESSION['login'])
{
include("header.php");
?>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">pages</a> </div>
    <h1>Pages</h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Data table</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Sl. No.</th>
                  <th>Title</th>
                  <th>Content</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                 <?php
                  $qry=mysqli_query($con,"select * from add_page where status=1");
                  $x=1;
                  while($result=mysqli_fetch_array($qry))
                  {
                  ?>
                    <tr class="gradeX">
                      <td width="5%"><?php echo $x; ?></td>
                      <td width="20%"><?=$result['page_title']; ?></td>
                      <td width="20%"><?=$result['page_content']; ?></td>
                      <td width="10%">
                        <a href="add-page.php?id=<?=$result['id']; ?>"><span class="edit">edit</span></a>
                        <a href="save.php?action=page_delete&id=<?=$result['id']; ?>"><span class="delete">Trash</span></a>
                      </td>
                    </tr>
                <?php
                $x++;
                  }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
 include("footer.php");
}
else
{
echo '<script> location.href="index.php"; </script>';
}
?>