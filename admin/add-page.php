<?php
session_start();
include("dbcon.php");
if($_SESSION['login'])
{
    if(isset($_GET['id']) && $_GET['id']!=''){
	$page_id = $_GET['id'];
	$query = mysqli_query($con,"select * from add_page where id='$page_id'");
	$result = mysqli_fetch_assoc($query);
	$id = $result['id'];
	$page_title = $result['page_title'];
	$page_desc = $result['page_content'];
	$btn_name = "edit_page";
	$btn_value="Update";
	$form_id="page_form";
}else{
	$id = "";
	$page_title = "";
	$page_desc = "";
	$btn_name = "add_page";
	$btn_value="publish";
	$form_id="page_validate";
}

include("header.php");
?>
<div id="content">
<div id="content-header">
  <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="tip-bottom">Pages</a> <a class="current">Add Page</a> </div>
  <h1>Add Page</h1>
</div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Page-information</h5>
        </div>
        <div class="widget-content">
          <form action="save.php?action=<?php echo $btn_name; ?>" method="post" class="form-horizontal" id="<?php echo $form_id; ?>" enctype="multipart/form-data">
              <input type="hidden" name="req_id" value="<?php echo $_REQUEST['id']; ?>" />
            <div class="control-group">
              <label class="control-label">Page Title</label>
              <div class="controls">
                <input type="text" class="span12" name="page_title" id="page_title" value="<?php echo $page_title; ?>" required="" />
              </div>
            </div>
            <div class="control-group">
                 <label class="control-label">page Description</label>
                <div class="controls">
                    <textarea class="textarea_editor span12" rows="6" placeholder="Enter text ..." name="page_desc" id="page_desc" required=""><?php echo $page_desc; ?></textarea>
                </div>
            </div>
            <div class="form-actions">
              <button type="submit" name="<?php echo $btn_name; ?>" class="btn btn-success"><?php echo $btn_value; ?></button>
            </div>
          </form>
        </div>
      </div>
  </div>
</div></div>
<?php
 include("footer.php");
}
else
{
echo '<script> location.href="index.php"; </script>';
}
?>