<!DOCTYPE html>
<html lang="en">
<head>
<title>StarDMC</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="css/fullcalendar.css" />
<link rel="stylesheet" href="css/matrix-style.css" />
<link rel="stylesheet" href="css/matrix-media.css" />
<link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
<link rel="stylesheet" href="css/jquery.gritter.css" />
<link rel="stylesheet" href="css/colorpicker.css" />
<!--<link rel="stylesheet" href="css/datepicker.css" />-->
<link rel="stylesheet" href="css/uniform.css" />
<link rel="stylesheet" href="css/select2.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/bootstrap-wysihtml5.css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<link href="css/alertify.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/pepper-grinder/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/dubrox/Multiple-Dates-Picker-for-jQuery-UI/master/jquery-ui.multidatespicker.css">
</head>
<body>
<div id="header">
  <h1><a href="dashboard.php">StarDMC</a></h1>
</div>
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav  pull-right">
    <li class=""><a title="" href="login.html"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
  </ul>
</div>
<div id="sidebar">
  <ul>
    <li class="active"><a href="dashboard.php"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li><a href="add-destination.php"><i class="icon icon-map-marker"></i> <span>Destination</span></a> </li>
    <li class="submenu"><a href="#"><i class="icon icon-link"></i> <span>Itinerary</span> <i class="icon icon-chevron-down" style="font-size:12px;font-weight:500; margin-left:60px"></i></a>
        <ul>
        <li><a href="add-itinerary.php">Add Itinerary</a></li>
        <li><a href="itinerary-list.php">View All Itinerary</a></li>
      </ul>
    </li>
    <li class="submenu"><a href="#"><i class="icon icon-link"></i> <span>Tour</span> <i class="icon icon-chevron-down" style="font-size:12px;font-weight:500; margin-left:60px"></i></a>
        <ul>
        <li><a href="add-tour.php">Add Tour</a></li>
        <li><a href="tour-list.php">View All Tour</a></li>
      </ul>
    </li>
    <li class="submenu"><a href="#"><i class="icon icon-user"></i> <span>Our Team</span> <i class="icon icon-chevron-down" style="font-size:12px;font-weight:500; margin-left:60px"></i></a>
        <ul>
        <li><a href="add-team.php">Add Team</a></li>
        <li><a href="team-list.php">View All Team members</a></li>
      </ul>
    </li>
    <li class="submenu"><a href="#"><i class="icon icon-user"></i> <span>Pages</span> <i class="icon icon-chevron-down" style="font-size:12px;font-weight:500; margin-left:60px"></i></a>
        <ul>
        <li><a href="add-page.php">Add New</a></li>
        <li><a href="page-list.php">All Pages</a></li>
      </ul>
    </li>
    <li><a href="tables.php"><i class="icon icon-th"></i> <span>Tables</span></a></li>
    <li><a href="gallery.php"><i class="icon icon-picture"></i> <span>Gallery</span></a></li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Forms</span> <i class="icon icon-chevron-down" style="font-size:12px;font-weight:500; margin-left:60px"></i></a>
      <ul>
        <li><a href="form-common.php">Basic Form</a></li>
        <li><a href="form-validation.php">Form with Validation</a></li>
      </ul>
    </li>
  </ul>
</div>