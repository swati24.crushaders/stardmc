<?php
session_start();
include('header.php');
?>
<div class="head-bg">
<div class="container">
<ul class="breadcrumb">
  <li><a href="index.html">Home</a></li>
  <li><a href="odisha-package.html">Holidays</a></li>
</ul></div>
    </div>
        <div class="clearfix"></div>
        <div class="container">
    <div class="about">
    <div class="col-md-8">
    <div class="row">
        <!--<div class="col-md-12"><h2>Culture of Orissa</h2></div>-->
        <div class="col-md-5 gallery">
              <div class="mySlides-details">   
    <img src="images/demo-detail-1.jpg" class="img-responsive" >
  </div>
  <div class="mySlides-details">
    <img src="images/demo-detail-2.jpg" class="img-responsive">
  </div>
  <div class="mySlides-details"> 
    <img src="images/demo-detail-3.jpg" class="img-responsive">
  </div>
  <div class="mySlides-details">
    <img src="images/demo-detail-4.jpg" class="img-responsive">
  </div>
  <div class="mySlides-details">
    <img src="images/demo-detail-5.jpg" class="img-responsive">
  </div>
  <div class="thumbnails">
    <div class="column">
      <img class="demo-details cursor img-responsive" src="images/thumb-1.jpg" onclick="currentSlide(1)" alt="The Woods">
    </div>
    <div class="column">
      <img class="demo-details cursor img-responsive" src="images/thumb-2.jpg" onclick="currentSlide(2)" alt="Cinque Terre">
    </div>
    <div class="column">
      <img class="demo-details cursor img-responsive" src="images/thumb-3.jpg" onclick="currentSlide(3)" alt="Mountains and fjords">
    </div>
    <div class="column">
      <img class="demo-details cursor img-responsive" src="images/thumb-4.jpg" onclick="currentSlide(4)" alt="Northern Lights">
    </div>
    <div class="column">
      <img class="demo-details cursor img-responsive" src="images/thumb-5.jpg" onclick="currentSlide(5)" alt="Nature and sunrise">
    </div>
  </div></div>
        <div class="col-md-7">
        <p><i class="far fa-clock"></i>&nbsp;&nbsp;<Span class="tour-detail">Tour Duration:</Span>Lorem Ipsum</p>
            <p><i class="fas fa-map-marker-alt"></i>&nbsp;&nbsp;<Span class="tour-detail">Tour Destination:</Span>Lorem Ipsum</p>
            <p><Span class="tour-detail">Departures :</Span> Lorem Ipsum Lorem Ipsum</p>
            <p><Span class="tour-detail">Destination Information</Span></p>
        <p>Odisha (formerly Orissa), an eastern Indian state on the Bay of Bengal, is known for its tribal cultures and its many ancient Hindu temples. The capital, Bhubaneswar, is home to hundreds of temples, notably the Nagara-style Mukteswar and the Lingaraj, both built in the 11th century and set around sacred Bindusagar Lake. The Odisha State Museum is dedicated to the area&#39;s indigenous history.</p>
            <div class="email">
            <button class=" btn email-package">Email Package</button>
            &nbsp;&nbsp;&nbsp;<button type="button" class="btn email-package" data-toggle="modal" data-target="#myModal">
  Book Now</button>
                <div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Booking Request Form</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       <form>
           <div class="form-group">
          <input type="text" placeholder="name" class="form-control">
          </div>
           <div class="form-group">
          <input type="text" placeholder="email" class="form-control">
          </div>
           <div class="form-group">
          <input type="text" placeholder="phone" class="form-control">
          </div>
           <div class="form-group">
          <textarea class="form-control" placeholder="message"></textarea>
          </div>
           <button class="btn submit">submit</button>
          </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
            </div>
            </div>
        </div><br><br>
        <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#Itinerary">Itinerary</a></li>
    <li><a data-toggle="tab" href="#Package-Inclusion">Inclusion & Enclusion</a></li>
    <li><a data-toggle="tab" href="#Package-Cost">Package Cost</a></li>
<!--    <li><a data-toggle="tab" href="#Book-package">Book this package</a></li>-->
  </ul>

  <div class="tab-content detail">
    <div id="Itinerary" class="tab-pane fade in active">
      <div class="details">
      <p><a class="day">DAY 01</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="tour-infm">Arrive Bhubaneswar</span></p>
          <p>Arrive Bhubaneswar, the Capital city of the modern State and the ancient kingdom of Kalinga. You will be met upon arrival & transferred to your hotel. This afternoon you will proceed for the city sightseeing tour of Bhubaneswar, visit Lingaraj Temple dedicated to Lord Shiva built in the 11th century, proceed to Mukteswara temple built in the 10th century known for its sculpted tales from the Panchatantra carved on it, the temple is a magnificent example of Orissan architecture continue to The Rajarani temple set in picturesque surrounding is noted for its intricate carvings of floral, animal and human figures built in the 11th century. Rest of the evening is at leisure for individual activities, overnight at the hotel.</p>
        </div>
        <div class="clearfix"></div>
        <div class="details">
      <p><a class="day">DAY 02</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="tour-infm">Bhubaneswar</span></p>
          <p>This morning you will visit Nandankanan Bilogical Park, The picturesque Nandankanan, or the Garden of the Gods, is a beautiful biological park, 20 kms from Bhubaneshwar, established in 1960. The park houses the very first captive gharial breeding centre of the country. The zoo at Nandakanan is world-renowned for its white tigers, enjoy an optional aerial ropeway and cable car on own. Also visit the Jain Caves; Khandagiri & Udayagiri. Overnight at the hotel.</p>
        </div>
        <div class="clearfix"></div>
        <div class="details">
      <p><a class="day">DAY 03</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="tour-infm">Bhubaneswar - Dhauli - Pipli - Konarak - Puri (120 Kms/3-4 Hrs)</span></p>
          <p>Today you will proceed to Puri, enroute you will visit, Dhauli-The battleground of Kalinga war during 3rd century BC, which transformed Emperor Ashoka into Dharmashoka.Later you will visit Konarak- world heritage site -The temple chariot of the Sun God on the sands of Bay of Bengal is a 13th century architectural marvel. It is designed as a celestial chariot of Sun God, complete with twelve pairs of wheel and seven horses. This legendary temple has sculptures of great beauty covering all aspects of life. It is most famous for its erotic art. Later drive to Puri and proceed to your hotel. Overnight at the hotel.</p>
        </div>
        <div class="clearfix"></div>
        <div class="details">
      <p><a class="day">DAY 04</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="tour-infm">Puri</span></p>
          <p>This morning you will visit Puri-the city by the sea- is a major pilgrim center in India, founded by .Adi Shankaracharya and is one of the peethas here. You can witness Mangala Arati at the Jagannath Temple in the early morning. Puri is also famous for its Golden beach ideal for swimming and surfing. But the fame of Puri emanates most from the Jagannath Temple, which contributed the word Jurrernaut to the English language. The 12th century temple is known for its annual Car Festival. This afternoon you visit Raghurajpur pattachitra painting village. Raghurajpur- half hour drive from Puri. Overnight at the hotel.</p>
        </div>
        <div class="clearfix"></div>
        <div class="details">
      <p><a class="day">DAY 05</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="tour-infm">Puri - Bhitarkanika</span></p>
          <p>In the morning, you will leave for Dangamal Island in Bhitarkanika. Enroute visit Cuttack to witness the Subhaschandra Bose Museum and the city is famous for Filgri work. Enroute also visit Lalitgiri the site for Kalachakra Tantra in Buddhisim. Then you will be landed at River mouth and you will take a motorboat cruise to reach the island. After arriving at Dangamal, you will check in at the forest rest house and stay overnight. In the evening hundreds of spotted deer and wild boars very friendly roaming around your forest lodge.</p>
        </div>
        <div class="clearfix"></div>
        <div class="details">
      <p><a class="day">DAY 06</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="tour-infm">Inside The Sanctuary</span></p>
          <p>Morning cruise to the Bird Sanctuary & trekking into the Hunting tower of the ancient King of kanika. Later visit to Crocodile museum & Interpretation centre. Dinner & Overnight at Forest cottage After breakfast cruise back to the Chandabali and later drive to Chandipur known as Vanishing Sea. After check in you can visit Fishing Jetty and relaxing at the beach. Overnight at the Beach.</p>
        </div>
        <div class="clearfix"></div>
        <div class="details">
      <p><a class="day">DAY 07</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="tour-infm">In & Around Chandipur - Balasore</span></p>
          <p>Early morning you can walk along 05 Kms when the sea water is receding it is called Vanishing sea. After breakfast you can visit Panchalingeswar on the scenic mountain Nilagiri. Very calm & quiet surrounding will give you the peace of mind at the waterfall and witness the Pncha Lingam on the hilltop. Later visit Khirachura Gopinath at Remuna (Lord Krishna Temple) and have Prasad along with famous Rabidi made of mailk. Overnight stay at Balasore</p>
        </div>
        <div class="clearfix"></div>
        <div class="details">
      <p><a class="day">DAY 08</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="tour-infm">Balasore - Bhubaneswar</span></p>
          <p>After breakfast check out from your hotel & transfer to Bhubaneswar for board flight/train for your return journey with sweet memories.</p>
        </div>
    </div>
    <div id="Package-Inclusion" class="tab-pane fade">
    <div class="details">
        <label>
        <input type="radio" name="x" id="include">&nbsp;What Your Package Include
        </label>&nbsp;&nbsp;&nbsp;&nbsp;
        <label>
        <input type="radio" id="exclude" name="x">&nbsp;What Your Package Exclude
        </label>
        </div>
        <div class="include-content">
        <p><i class="fas fa-arrow-right"></i>&nbsp;&nbsp;Assistance on Arrival & Departure.</p>
        <p><i class="fas fa-arrow-right"></i>&nbsp;&nbsp;Accommodation on twin Sharing Basis as Per Plan.</p>
        <p><i class="fas fa-arrow-right"></i>&nbsp;&nbsp;Exclusive Non Ac & A/c vehicle for transfers & sightseeing. Please brief to guest that vehicle will not be at disposable it will available to guest as per itinerary only.</p>
        <p><i class="fas fa-arrow-right"></i>&nbsp;&nbsp;All permit fees & hotel taxes.</p>
        </div>
        <div class="exclude-content">
        <p><i class="fas fa-arrow-right"></i>&nbsp;&nbsp;Air Fare/Train fare.</p>
        <p><i class="fas fa-arrow-right"></i>&nbsp;&nbsp;Personal Expenses such as Laundry, telephone calls, tips. Liquor, boating & joy rides.</p>
        <p><i class="fas fa-arrow-right"></i>&nbsp;&nbsp;Any other item not specified in "Cost Includes".</p>
        <p><i class="fas fa-arrow-right"></i>&nbsp;&nbsp;Additional sightseeing or usages of vehicle not mention in the itinerary.</p>
        <p><i class="fas fa-arrow-right"></i>&nbsp;&nbsp;Guide & Entrance Fees.</p>
        <p><i class="fas fa-arrow-right"></i>&nbsp;&nbsp;Any cost arising due to natural calamities like, landslides, road blocks etc (to be borne by the clients directly on the spot).</p>
        <p><i class="fas fa-arrow-right"></i>&nbsp;&nbsp;Govt. Service Tax. 5%</p>
        </div>
       
    </div>
    <div id="Package-Cost" class="tab-pane fade">
        <p style="padding:20px 12px;">Package Cost 25000/- per person</p>
    </div>

  </div>
        </div>
        <div class="col-md-4">
      <h3 class="get-in-touch">GET IN TOUCH</h3>
          <form class="sidebar-form">
           <input type="text" placeholder="Full Name" class="form-control"><br>
            <input type="text" placeholder="Email Id" class="form-control"><br>
            <input type="tel" placeholder="Contact Number" class="form-control"> <br>
            <textarea class="form-control" placeholder="Message..."></textarea><br>
            <button type="submit" class="btn  btn-block custom">Submit</button>
            </form>
        </div>
    </div>
        </div>
        <?php
include("footer.php");
?>
      <script>
          $(function() {
   $("input[name='x']").click(function() {
     if ($("#include").is(":checked")) {
       $(".include-content").show();
         $(".exclude-content").hide();
     } else {
       $(".include-content").hide();
          $(".exclude-content").show();
     }
   });
 });
      
      var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides-details");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}    
      </script>  
  