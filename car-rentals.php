<?php
session_start();
include('header.php');
?>

   <div class="inner-banner">
       <img src="images/car-rental-banner.jpg" alt="" class="img-responsive">
       <h3>car rentals</h3>
    </div>
 <div class="clearfix"></div>

<div class="head-bg">
<div class="container">
<ul class="breadcrumb">
  <li><a href="index.html">Home</a></li>
  <li><a>Car-Rentals</a></li>
</ul></div>
    </div>
        <div class="clearfix"></div>
        <div class="container">
    <div class="about">
    <div class="col-md-8">
       <div id="slide3" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#slide3" data-slide-to="0" class="active"></li>
      <li data-target="#slide3" data-slide-to="1"></li>
      <li data-target="#slide3" data-slide-to="2"></li>
      <li data-target="#slide3" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active min-hgt">
        <img src="images/car-slider-image1.jpg" alt="Los Angeles">
      </div>

      <div class="item min-hgt">
        <img src="images/car-rental-slider-image2.jpg" alt="Chicago">
      </div>
    
      <div class="item min-hgt">
        <img src="images/car-rental-slider-image3.jpg" alt="New york">
      </div>
        <div class="item min-hgt">
        <img src="images/car-rental-slider-images4.jpg" alt="New york">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#slide3" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#slide3" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
     <span class="sr-only">Next</span>
    </a>
  </div> 
        <br>
      <div class="col-md-12">
        <div class="row">
        <p>Hire Car Rental in Odisha, <strong>Star Dmc</strong>, Odisha deals with <strong>Odisha Taxi Packages</strong>, <strong>Luxury Odisha cabs</strong>, <strong>Airport transfer, Railway Station Transfer</strong>. Our <strong>Taxi Services in Odisha</strong> has a very wide network. Naturally, when it comes to hiring a car or other vehicle your primary concern should be with its quality. After reaching Odisha you will get Car &amp; Coach Rental Services in Odisha with Wide Variants of Vehicles&nbsp;<strong>Mini &amp; Sedan Cars &#45;</strong>&nbsp;<a href="#">AC Indigo</a>, <a href="#">AC Swift Dzire</a>, <a href="#">AC Hyundai Xcent</a>, <a href="#">AC Chevrolet Sail</a>, <a href="#">AC Honda Amaze</a>, <a href="#">AC Toyota Etios</a>, <a href="#">AC Honda City</a>, <a href="#">AC Hyundai Verna</a>, <a href="#">AC Maruti Ciaz</a>, <a href="#">AC Chevrolet Cruze</a>, <a href="#">AC Toyota Corolla</a>&nbsp;<strong>SUVS Cars &#45;</strong>&nbsp;AC Chevrolet Enjoy, AC Scorpio, AC Tavera, AC Innova, AC Innova Crysta, AC Toyota Fortuner&nbsp;<strong>Luxury Cars &#45;</strong> Mercedes E250, Audi A6, Jaguar XF, Audi A4, Audi Q3 and BMW 3 Series&nbsp;<strong>AC Mini Coaches</strong>&nbsp;&#45; 13 Seater Tata Winger Mini Van, 13 Seater Force Traveller, 17 Seater Force Traveller, 26 Seater Force Traveller (26+1 Driver)&nbsp;<strong>Luxury Mini Coaches &#45;</strong>&nbsp;15 Seater Luxury Force Traveller, 18 Seater Luxury Force Traveller&nbsp;<strong>SML Coaches-&nbsp;</strong>AC 18 Seater SML Coach, <a href="#">AC 28 Seater SML Coach</a>&nbsp;<strong>Luxury AC Bus Coaches &#45;&nbsp;</strong>45 Seater Volvo Bus<strong>,&nbsp;</strong>45 Seater AC Bus (45 +1 Driver),&nbsp;41 Seater + 14 Sleeper Seat AC Bus (41 +14 + 1 Driver).</p>
            <p><a href="index.html"><strong>Star Dmc</strong></a> provides Car And Coach Rental Services in whole Odisha (Orissa) i.e. Bhubaneswar, Bhubaneswar Airport (BBI), Cuttack, Puri, Sakhigopal, Konark, Chilika, Satapada, Khurda Road Junction, Chhatrapur, Berhampur, Ganjam, Gopalpur, Taptapani, Bhitarkanika National Park,Rourkela, Sambalpur, Angul, Balangir, Balasore, Chandipur, Bargarh, Bhadrak, Boudh, Deogarh, Dhenkanal, Paralakhemundi, Jagatsinghpur, Jajpur, Jharsuguda, Bhawanipatna, Phulbani, Kendrapara, Keonjhar, Koraput, Malkangiri, Baripada, Nowrangpur, Nayagarh, Nuapada, Rayagada, Sonepur, Simlipal, Paradip, Dhabaleswar, Sundergarh, Barbil, Titlagarh, Damanjodi.</p>
            
             <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#Local">Local & Outstation Tariff</a></li>
    <li><a data-toggle="tab" href="#Special">Special Transport Packages</a></li>
    <li><a data-toggle="tab" href="#Wedding">Wedding Car Tariff</a></li>
  </ul>
    <div class="tab-content car-rental">
    <div id="Local" class="tab-pane fade in active">
<div class="car-details">
      <div class="row">
    <table class="table table-bordered table-responsive">
    <thead>
      <tr>
        <th class="heading">Variants Of Vehicle</th>
        <th class="heading" colspan="4">Local Trip</th>
        <th class="heading">Outstation / Long Distance<br>
(Min 250Km Per Day)</th>
      </tr>
           <tr>
        <th></th>
<th>80km/8Hrs.(Rs.)</th>
<th>120km/12Hrs.(Rs.)</th>
<th>Extra Hrs. (Rs.)</th>
<th>Extra Km (Rs.)</th>
<th>Per Km (Rs.)</th>
      </tr>
        </thead>
        
    <tbody>
        <tr>
        <td class="title" colspan="6">Mini & Sedan Cars</td>
<!--        <td colspan="4"></td>-->
        </tr>
          <tr>
        <td class="car-images">
            <a href="#"><img src="images/car-image-t1.jpg" alt=""><br>
           <p>AC Tata Indigo (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>1,350</p></td>  
        <td class="wedding-table"><p>1,800</p></td>
        <td class="wedding-table"><p>90</p></td>
        <td class="wedding-table"><p>11</p></td>
        <td class="wedding-table"><p>11</p></td>
        </tr>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/swift-dzire.png" alt=""><br>
           <p>AC Swift Dzire (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>1,500</p></td>  
        <td class="wedding-table"><p>2000</p></td>
        <td class="wedding-table"><p>120</p></td>
        <td class="wedding-table"><p>12</p></td>
        <td class="wedding-table"><p>12</p></td>
        </tr>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/hyundai-xcent.jpg" alt=""><br>
           <p>AC Hyundai Xcent (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>1,600</p></td>  
        <td class="wedding-table"><p>2,100</p></td>
        <td class="wedding-table"><p>120</p></td>
        <td class="wedding-table"><p>12</p></td>
        <td class="wedding-table"><p>12</p></td>
        </tr>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/chevrolet-sail.jpg" alt=""><br>
           <p>AC Chevrolet Sail (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>1,600</p></td>  
        <td class="wedding-table"><p>2,100</p></td>
        <td class="wedding-table"><p>120</p></td>
        <td class="wedding-table"><p>12</p></td>
        <td class="wedding-table"><p>12</p></td>
        </tr>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/honda-amaze.jpg" alt=""><br>
           <p>AC Honda Amaze (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>1,700</p></td>  
        <td class="wedding-table"><p>2,250</p></td>
        <td class="wedding-table"><p>150</p></td>
        <td class="wedding-table"><p>13</p></td>
        <td class="wedding-table"><p>13</p></td>
        </tr>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/toyota-etios.jpg" alt=""><br>
           <p>AC Toyota Etios (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>1,750</p></td>  
        <td class="wedding-table"><p>2,350</p></td>
        <td class="wedding-table"><p>150</p></td>
        <td class="wedding-table"><p>14</p></td>
        <td class="wedding-table"><p>14</p></td>
        </tr>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/honda-city.png" alt=""><br>
           <p>AC Honda City (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>3,000</p></td>  
        <td class="wedding-table"><p>3,650</p></td>
        <td class="wedding-table"><p>200</p></td>
        <td class="wedding-table"><p>16</p></td>
        <td class="wedding-table"><p>16</p></td>
        </tr>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/hyundai-verna.jpg" alt=""><br>
           <p>AC Hyundai Verna (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>3,000</p></td>  
        <td class="wedding-table"><p>3,650</p></td>
        <td class="wedding-table"><p>200</p></td>
        <td class="wedding-table"><p>16</p></td>
        <td class="wedding-table"><p>16</p></td>
        </tr>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/maruti-suzuki-ciaz.jpg" alt=""><br>
           <p>AC Maruti Suzuki Ciaz (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>3,000</p></td>  
        <td class="wedding-table"><p>3,650</p></td>
        <td class="wedding-table"><p>200</p></td>
        <td class="wedding-table"><p>16</p></td>
        <td class="wedding-table"><p>16</p></td>
        </tr>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/chevrolet-cruze.jpg" alt=""><br>
           <p>AC Chevrolet Cruze (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>4,000</p></td>  
        <td class="wedding-table"><p>4,800</p></td>
        <td class="wedding-table"><p>300</p></td>
        <td class="wedding-table"><p>19</p></td>
        <td class="wedding-table"><p>19</p></td>
        </tr>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/corolla-altis.jpg" alt=""><br>
           <p>AC Corolla Altis (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>4,000</p></td>  
        <td class="wedding-table"><p>4,800</p></td>
        <td class="wedding-table"><p>300</p></td>
        <td class="wedding-table"><p>19</p></td>
        <td class="wedding-table"><p>19</p></td>
        </tr>
        
        <tr>
        <td class="title" colspan="6">Suv cars</td>
<!--        <td colspan="4"></td>-->
        </tr>
          <tr>
        <td class="car-images">
            <a href="#"><img src="images/car-image-t1.jpg" alt=""><br>
           <p>AC Tata Indigo (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>1,350</p></td>  
        <td class="wedding-table"><p>1,800</p></td>
        <td class="wedding-table"><p>90</p></td>
        <td class="wedding-table"><p>11</p></td>
        <td class="wedding-table"><p>11</p></td>
        </tr>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/swift-dzire.png" alt=""><br>
           <p>AC Swift Dzire (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>1,500</p></td>  
        <td class="wedding-table"><p>2000</p></td>
        <td class="wedding-table"><p>120</p></td>
        <td class="wedding-table"><p>12</p></td>
        <td class="wedding-table"><p>12</p></td>
        </tr>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/hyundai-xcent.jpg" alt=""><br>
           <p>AC Hyundai Xcent (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>1,600</p></td>  
        <td class="wedding-table"><p>2,100</p></td>
        <td class="wedding-table"><p>120</p></td>
        <td class="wedding-table"><p>12</p></td>
        <td class="wedding-table"><p>12</p></td>
        </tr>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/chevrolet-sail.jpg" alt=""><br>
           <p>AC Chevrolet Sail (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>1,600</p></td>  
        <td class="wedding-table"><p>2,100</p></td>
        <td class="wedding-table"><p>120</p></td>
        <td class="wedding-table"><p>12</p></td>
        <td class="wedding-table"><p>12</p></td>
        </tr>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/honda-amaze.jpg" alt=""><br>
           <p>AC Honda Amaze (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>1,700</p></td>  
        <td class="wedding-table"><p>2,250</p></td>
        <td class="wedding-table"><p>150</p></td>
        <td class="wedding-table"><p>13</p></td>
        <td class="wedding-table"><p>13</p></td>
        </tr>
        
        <tr>
        <td class="title" colspan="6">Premium Cars</td>
<!--        <td colspan="4"></td>-->
        </tr>
        
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/toyota-etios.jpg" alt=""><br>
           <p>AC Toyota Etios (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>1,750</p></td>  
        <td class="wedding-table"><p>2,350</p></td>
        <td class="wedding-table"><p>150</p></td>
        <td class="wedding-table"><p>14</p></td>
        <td class="wedding-table"><p>14</p></td>
        </tr>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/honda-city.png" alt=""><br>
           <p>AC Honda City (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>3,000</p></td>  
        <td class="wedding-table"><p>3,650</p></td>
        <td class="wedding-table"><p>200</p></td>
        <td class="wedding-table"><p>16</p></td>
        <td class="wedding-table"><p>16</p></td>
        </tr>
        <tr>
        <td class="car-images">
            <a href="#"><img src="images/hyundai-verna.jpg" alt=""><br>
           <p>AC Hyundai Verna (4+1Driver)</p></a>
              </td>
        <td class="wedding-table"><p>3,000</p></td>  
        <td class="wedding-table"><p>3,650</p></td>
        <td class="wedding-table"><p>200</p></td>
        <td class="wedding-table"><p>16</p></td>
        <td class="wedding-table"><p>16</p></td>
        </tr>
        
    </tbody>
        
  </table>
          
    </div>
        </div>
      </div>
    <div id="Special" class="tab-pane fade">
           <div class="car-rental-detail-info">
        <p>For Booking <strong>Car &amp; Coach Hire Services</strong>, Just Fill the <strong>Enquiry Now Form</strong> &amp; <strong>Get a Instant Free Taxi Package Quotation</strong>. <span>OR</span> <strong><i class="fa fa-phone"></i> Call Us On: 1800 120 8464, +91 83379 11111</strong> <small><i class="fa fa-whatsapp"></i></small> </p></div>
    </div>
    <div id="Wedding" class="tab-pane fade">
        <div class="car-details">
            <div class="row">
            <table class="table table-bordered table-responsive wedding-tarif">
                <thead>
                <tr>
                    <th>Name of Wedding Car</th>
                    <th>Per Day (With Procession<br>(100 km/12 Hrs.) (Rs.)</th>
                    <th>Extra Hrs. (Rs.)</th>
                    <th>Extra Km (Rs.)</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="wedding-table">
                        <a hrfe="#"><img src="images/mercedes-c220-wedding.jpg" alt="" class="img-responsive"><p>Mercedes E250</p></a>
                    </td>
                    <td class="wedding-table"><p>Rs. 35,000</p></td>
                    <td class="wedding-table"><p>1200</p></td>
                    <td class="wedding-table"><p>110</p></td>
                    </tr>
                    
                    <tr>
                    <td class="wedding-table">
                        <a hrfe="#"><img src="images/mercedes-c220-wedding.jpg" alt="" class="img-responsive"><p>AUDI A6</p></a>
                    </td>
                    <td class="wedding-table"><p>Rs. 30,000</p></td>
                    <td class="wedding-table"><p>900</p></td>
                    <td class="wedding-table"><p>100</p></td>
                    </tr>
                    
                    <tr>
                    <td class="wedding-table">
                        <a hrfe="#"><img src="images/jaguar-xf-wedding.jpg" alt="" class="img-responsive"><p>JAGUAR XF</p></a>
                    </td>
                    <td class="wedding-table"><p>Rs. 30,000</p></td>
                    <td class="wedding-table"><p>1200</p></td>
                    <td class="wedding-table"><p>110</p></td>
                    </tr>
                    
                    <tr>
                    <td class="wedding-table">
                        <a hrfe="#"><img src="images/audi-a4-wedding.jpg" alt="" class="img-responsive"><p>Mercedes E250</p></a>
                    </td>
                    <td class="wedding-table"><p>Rs. 30,000</p></td>
                    <td class="wedding-table"><p>1200</p></td>
                    <td class="wedding-table"><p>110</p></td>
                    </tr>
                    
                    <tr>
                    <td class="wedding-table">
                        <a hrfe="#"><img src="images/mercedes-c220-wedding.jpg" alt="" class="img-responsive"><p>Mercedes E250</p></a>
                    </td>
                    <td class="wedding-table"><p>Rs. 30,000</p></td>
                    <td class="wedding-table"><p>1200</p></td>
                    <td class="wedding-table"><p>110</p></td>
                    </tr>
                    
                    <tr>
                    <td class="wedding-table">
                        <a hrfe="#"><img src="images/audi-a6-wedding.jpg" alt="" class="img-responsive"><p>Mercedes E250</p></a>
                    </td>
                    <td class="wedding-table"><p>Rs. 30,000</p></td>
                    <td class="wedding-table"><p>1200</p></td>
                    <td class="wedding-table"><p>110</p></td>
                    </tr>
                </tbody>
                
                </table>
            </div>
    </div>
       </div>
            </div>
          </div>
        </div>
        </div>  
        </div>
        <div class="col-md-4">
      <h3 class="get-in-touch">Enquiry Now</h3>
          <form class="sidebar-form">
              <div class="form-group col-md-12">
                  <div class="row">
              <select class="form-control">
          <option selected="selected">Pick up Location</option>
          <option value=" Bhubaneswar Airport (BBI)"> Bhubaneswar Airport (BBI)</option>
          <option value="Bhubaneswar Railway Station ">Bhubaneswar Railway Station </option>
          <option value="Bhubaneswar Hotels">Bhubaneswar Hotels</option>
          <option value="Puri Railway Station ">Puri Railway Station </option>
          <option value="Puri Hotels">Puri Hotels</option>
          <option value="Puri Area">Puri Area</option>
          <option value="Khurdha Jn Railway Station">Khurdha Jn Railway Station</option>
          <option value="Cuttack Railway Station">Cuttack Railway Station</option>
          <option value="Berhampur Railway Station">Berhampur Railway Station</option>
          <option value="Angul Odisha India">Angul Odisha India</option>
          <option value="Bhubaneswar Odisha India">Bhubaneswar Odisha India</option>
          <option value="Brahmagiri Odisha India">Brahmagiri Odisha India</option>
          <option value="Bhitarkanika Odisha India">Bhitarkanika Odisha India</option>
          <option value="Brahmapur Odisha India">Brahmapur Odisha India</option>
          <optgroup label="----------------------------------------"></optgroup>
          <option value="Balangir Odisha India">Balangir Odisha India</option>
          <option value="Balasore Odisha India ">Balasore Odisha India </option>
          <option value="Barbil Odisha India">Barbil Odisha India</option>
          <option value="Bargarh Odisha India">Bargarh Odisha India</option>
          <option value="Baripada Odisha India">Baripada Odisha India</option>
          <option value="Bhadrak Odisha India">Bhadrak Odisha India</option>
          <option value="Balugaon Odisha India">Balugaon Odisha India</option>
          <option value="Bhawanipatna Odisha India">Bhawanipatna Odisha India</option>
          <option value="Balliguda Odisha India">Balliguda Odisha India</option>
          <option value="Boudh Odisha India">Boudh Odisha India</option>
          <option value="Chilika Lake Odisha India">Chilika Lake Odisha India</option>
          <option value="Cuttack Odisha India">Cuttack Odisha India</option>
          <option value="Chandipur Odisha India">Chandipur Odisha India</option>
          <option value="Chhatrapur Odisha India">Chhatrapur Odisha India</option>
          <option value="Daringbadi Odisha India">Daringbadi Odisha India</option>
          <option value="Dhabaleswar Odisha India">Dhabaleswar Odisha India</option>
          <option value="Dhenkanal Odisha India">Dhenkanal Odisha India</option>
          <option value="Deogarh Odisha India">Deogarh Odisha India</option>
          <option value="Damanjodi Odisha India">Damanjodi Odisha India</option>
          <option value="Gopalpur Odisha India">Gopalpur Odisha India</option>
          <option value="Ganjam Odisha India">Ganjam Odisha India</option>
          <option value="Jajpur Odisha India">Jajpur Odisha India</option>
          <option value="Jaleswar Odisha India">Jaleswar Odisha India</option>
          <option value="Jharsuguda Odisha India">Jharsuguda Odisha India</option>
          <option value="Jagdalpur Odisha India">Jagdalpur Odisha India</option>
          <option value="Jeypore Odisha India">Jeypore Odisha India</option>
          <option value="Jagatsinghpur Odisha India">Jagatsinghpur Odisha India</option>
          <option value="Khurdha Odisha India">Khurdha Odisha India</option>
          <option value="Konark Odisha India">Konark Odisha India</option>
          <option value="Kendrapara Odisha India">Kendrapara Odisha India</option>
          <option value="Koraput Odisha India">Koraput Odisha India</option>
          <option value="Keonjhar Odisha India">Keonjhar Odisha India</option>
          <option value="Mayurbhanj Odisha India">Mayurbhanj Odisha India</option>
          <option value="Muniguda Odisha India">Muniguda Odisha India</option>
          <option value="Malkangiri Odisha India">Malkangiri Odisha India</option>
          <option value="Nayagarh Odisha India">Nayagarh Odisha India</option>
          <option value="Nowrangpur Odisha India">Nowrangpur Odisha India</option>
          <option value="Nuapada Odisha India">Nuapada Odisha India</option>
          <option value="Puri Odisha India">Puri Odisha India</option>
          <option value="Paradip Odisha India">Paradip Odisha India</option>
          <option value="Phulbani Odisha India">Phulbani Odisha India</option>
          <option value="Paralakhemundi Odisha India">Paralakhemundi Odisha India</option>
          <option value="Rayagada Odisha India">Rayagada Odisha India</option>
          <option value="Rourkela Odisha India">Rourkela Odisha India</option>
          <option value="Satapada Odisha India">Satapada Odisha India</option>
          <option value="SakhiGopal Odisha India">SakhiGopal Odisha India</option>
          <option value="Sambalpur Odisha India">Sambalpur Odisha India</option>
          <option value="Sunabeda Odisha India">Sunabeda Odisha India</option>
          <option value="Sundargarh Odisha India">Sundargarh Odisha India</option>
          <option value="Sonepur Odisha India">Sonepur Odisha India</option>
          <option value="Simlipal Odisha India">Simlipal Odisha India</option>
          <option value="Titlagarh Odisha India">Titlagarh Odisha India</option>
          <option value="Talcher Odisha India">Talcher Odisha India</option>
          <option value="Taptapani Odisha India">Taptapani Odisha India</option>
          <option value="carfrom">Other Location (If Any)</option>
        </select></div></div><br>
            <div class="form-group col-md-12">
              <div class="row">
                <select name="dropme_taxi" id="dropmeselect" class="form-control"> 
          <option selected="selected">Drop Location</option>
          <option value=" Bhubaneswar Airport (BBI)"> Bhubaneswar Airport (BBI)</option>
          <option value="Bhubaneswar Railway Station ">Bhubaneswar Railway Station </option>
          <option value="Bhubaneswar Hotels">Bhubaneswar Hotels</option>
          <option value="Puri Railway Station ">Puri Railway Station </option>
          <option value="Puri Hotels">Puri Hotels</option>
          <option value="Puri Area">Puri Area</option>
          <option value="Khurdha Jn Railway Station">Khurdha Jn Railway Station</option>
          <option value="Cuttack Railway Station">Cuttack Railway Station</option>
          <option value="Berhampur Railway Station">Berhampur Railway Station</option>
          <option value="Angul Odisha India">Angul Odisha India</option>
          <option value="Bhubaneswar Odisha India">Bhubaneswar Odisha India</option>
          <option value="Brahmagiri Odisha India">Brahmagiri Odisha India</option>
          <option value="Bhitarkanika Odisha India">Bhitarkanika Odisha India</option>
          <option value="Brahmapur Odisha India">Brahmapur Odisha India</option>
          <optgroup label="----------------------------------------"></optgroup>
          <option value="Balangir Odisha India">Balangir Odisha India</option>
          <option value="Balasore Odisha India ">Balasore Odisha India </option>
          <option value="Barbil Odisha India">Barbil Odisha India</option>
          <option value="Bargarh Odisha India">Bargarh Odisha India</option>
          <option value="Baripada Odisha India">Baripada Odisha India</option>
          <option value="Bhadrak Odisha India">Bhadrak Odisha India</option>
          <option value="Balugaon Odisha India">Balugaon Odisha India</option>
          <option value="Bhawanipatna Odisha India">Bhawanipatna Odisha India</option>
          <option value="Balliguda Odisha India">Balliguda Odisha India</option>
          <option value="Boudh Odisha India">Boudh Odisha India</option>
          <option value="Chilika Lake Odisha India">Chilika Lake Odisha India</option>
          <option value="Cuttack Odisha India">Cuttack Odisha India</option>
          <option value="Chandipur Odisha India">Chandipur Odisha India</option>
          <option value="Chhatrapur Odisha India">Chhatrapur Odisha India</option>
          <option value="Daringbadi Odisha India">Daringbadi Odisha India</option>
          <option value="Dhabaleswar Odisha India">Dhabaleswar Odisha India</option>
          <option value="Dhenkanal Odisha India">Dhenkanal Odisha India</option>
          <option value="Deogarh Odisha India">Deogarh Odisha India</option>
          <option value="Damanjodi Odisha India">Damanjodi Odisha India</option>
          <option value="Gopalpur Odisha India">Gopalpur Odisha India</option>
          <option value="Ganjam Odisha India">Ganjam Odisha India</option>
          <option value="Jajpur Odisha India">Jajpur Odisha India</option>
          <option value="Jaleswar Odisha India">Jaleswar Odisha India</option>
          <option value="Jharsuguda Odisha India">Jharsuguda Odisha India</option>
          <option value="Jagdalpur Odisha India">Jagdalpur Odisha India</option>
          <option value="Jeypore Odisha India">Jeypore Odisha India</option>
          <option value="Jagatsinghpur Odisha India">Jagatsinghpur Odisha India</option>
          <option value="Khurdha Odisha India">Khurdha Odisha India</option>
          <option value="Konark Odisha India">Konark Odisha India</option>
          <option value="Kendrapara Odisha India">Kendrapara Odisha India</option>
          <option value="Koraput Odisha India">Koraput Odisha India</option>
          <option value="Keonjhar Odisha India">Keonjhar Odisha India</option>
          <option value="Mayurbhanj Odisha India">Mayurbhanj Odisha India</option>
          <option value="Muniguda Odisha India">Muniguda Odisha India</option>
          <option value="Malkangiri Odisha India">Malkangiri Odisha India</option>
          <option value="Nayagarh Odisha India">Nayagarh Odisha India</option>
          <option value="Nowrangpur Odisha India">Nowrangpur Odisha India</option>
          <option value="Nuapada Odisha India">Nuapada Odisha India</option>
          <option value="Puri Odisha India">Puri Odisha India</option>
          <option value="Paradip Odisha India">Paradip Odisha India</option>
          <option value="Phulbani Odisha India">Phulbani Odisha India</option>
          <option value="Paralakhemundi Odisha India">Paralakhemundi Odisha India</option>
          <option value="Rayagada Odisha India">Rayagada Odisha India</option>
          <option value="Rourkela Odisha India">Rourkela Odisha India</option>
          <option value="Satapada Odisha India">Satapada Odisha India</option>
          <option value="SakhiGopal Odisha India">SakhiGopal Odisha India</option>
          <option value="Sambalpur Odisha India">Sambalpur Odisha India</option>
          <option value="Sunabeda Odisha India">Sunabeda Odisha India</option>
          <option value="Sundargarh Odisha India">Sundargarh Odisha India</option>
          <option value="Sonepur Odisha India">Sonepur Odisha India</option>
          <option value="Simlipal Odisha India">Simlipal Odisha India</option>
          <option value="Titlagarh Odisha India">Titlagarh Odisha India</option>
          <option value="Talcher Odisha India">Talcher Odisha India</option>
          <option value="Taptapani Odisha India">Taptapani Odisha India</option>
          <option value="dropme">Other Location (If Any)</option>
        </select>
                </div>
              </div>
              <div class="form-group col-md-7">
              <div class="row">
                  <input type="text" name="arrivaldate_taxi" id="arrival" class="form-control datepicker" readonly="readonly" placeholder="Pick-Up Date (If Available) ..." value="" required="">
                  </div>
              </div>
              <div class="form-group col-md-5">
              <div class="row pdlft">
                  <select name="arrivaltime_taxi" class="form-control">
              <option value="00:00 AM" selected="selected">Pick-Up Time</option>
              <option value="00:30 AM">00:30 AM</option>
              <option value="01:00 AM">01:00 AM</option>
              <option value="01:30 AM">01:30 AM</option>
              <option value="02:00 AM">02:00 AM</option>
              <option value="02:30 AM">02:30 AM</option>
              <option value="03:00 AM">03:00 AM</option>
              <option value="03:30 AM">03:30 AM</option>
              <option value="04:00 AM">04:00 AM</option>
              <option value="04:30 AM">04:30 AM</option>
              <option value="05:00 AM">05:00 AM</option>
              <option value="05:30 AM">05:30 AM</option>
              <option value="06:00 AM">06:00 AM</option>
              <option value="06:30 AM">06:30 AM</option>
              <option value="07:00 AM">07:00 AM</option>
              <option value="07:30 AM">07:30 AM</option>
              <option value="08:00 AM">08:00 AM</option>
              <option value="08:30 AM">08:30 AM</option>
              <option value="09:00 AM">09:00 AM</option>
              <option value="09:30 AM">09:30 AM</option>
              <option value="10:00 AM">10:00 AM</option>
              <option value="10:30 AM">10:30 AM</option>
              <option value="11:00 AM">11:00 AM</option>
              <option value="11:30 AM">11:30 AM</option>
              <option value="12:00 PM">12:00 PM</option>
              <option value="12:30 PM">12:30 PM</option>
              <option value="01:00 PM">13:00 PM</option>
              <option value="01:30 PM">13:30 PM</option>
              <option value="02:00 PM">14:00 PM</option>
              <option value="02:30 PM">14:30 PM</option>
              <option value="03:00 PM">15:00 PM</option>
              <option value="03:30 PM">15:30 PM</option>
              <option value="04:00 PM">16:00 PM</option>
              <option value="04:30 PM">16:30 PM</option>
              <option value="05:00 PM">17:00 PM</option>
              <option value="05:30 PM">17:30 PM</option>
              <option value="06:00 PM">18:00 PM</option>
              <option value="06:30 PM">18:30 PM</option>
              <option value="07:00 PM">19:00 PM</option>
              <option value="07:30 PM">19:30 PM</option>
              <option value="08:00 PM">20:00 PM</option>
              <option value="08:30 PM">20:30 PM</option>
              <option value="09:00 PM">21:00 PM</option>
              <option value="09:30 PM">21:30 PM</option>
              <option value="10:00 PM">22:00 PM</option>
              <option value="10:30 PM">22:30 PM</option>
              <option value="11:00 PM">23:00 PM</option>
              <option value="11:30 PM">23:30 PM</option>
              <option value="12:00 AM">00:00 AM</option>
            </select>
                  </div>
              </div>
              <div class="form-group col-md-7">
              <div class="row">
                  <input type="text" name="arrivaldate_taxi" id="arrival" class="form-control datepicker" readonly="readonly" placeholder="Drop Date (If Available) ..." value="" required="">
                  </div>
              </div>
              <div class="form-group col-md-5">
              <div class="row pdlft">
                  <select name="arrivaltime_taxi" class="form-control">
              <option value="00:00 AM" selected="selected">Drop Time</option>
              <option value="00:30 AM">00:30 AM</option>
              <option value="01:00 AM">01:00 AM</option>
              <option value="01:30 AM">01:30 AM</option>
              <option value="02:00 AM">02:00 AM</option>
              <option value="02:30 AM">02:30 AM</option>
              <option value="03:00 AM">03:00 AM</option>
              <option value="03:30 AM">03:30 AM</option>
              <option value="04:00 AM">04:00 AM</option>
              <option value="04:30 AM">04:30 AM</option>
              <option value="05:00 AM">05:00 AM</option>
              <option value="05:30 AM">05:30 AM</option>
              <option value="06:00 AM">06:00 AM</option>
              <option value="06:30 AM">06:30 AM</option>
              <option value="07:00 AM">07:00 AM</option>
              <option value="07:30 AM">07:30 AM</option>
              <option value="08:00 AM">08:00 AM</option>
              <option value="08:30 AM">08:30 AM</option>
              <option value="09:00 AM">09:00 AM</option>
              <option value="09:30 AM">09:30 AM</option>
              <option value="10:00 AM">10:00 AM</option>
              <option value="10:30 AM">10:30 AM</option>
              <option value="11:00 AM">11:00 AM</option>
              <option value="11:30 AM">11:30 AM</option>
              <option value="12:00 PM">12:00 PM</option>
              <option value="12:30 PM">12:30 PM</option>
              <option value="01:00 PM">13:00 PM</option>
              <option value="01:30 PM">13:30 PM</option>
              <option value="02:00 PM">14:00 PM</option>
              <option value="02:30 PM">14:30 PM</option>
              <option value="03:00 PM">15:00 PM</option>
              <option value="03:30 PM">15:30 PM</option>
              <option value="04:00 PM">16:00 PM</option>
              <option value="04:30 PM">16:30 PM</option>
              <option value="05:00 PM">17:00 PM</option>
              <option value="05:30 PM">17:30 PM</option>
              <option value="06:00 PM">18:00 PM</option>
              <option value="06:30 PM">18:30 PM</option>
              <option value="07:00 PM">19:00 PM</option>
              <option value="07:30 PM">19:30 PM</option>
              <option value="08:00 PM">20:00 PM</option>
              <option value="08:30 PM">20:30 PM</option>
              <option value="09:00 PM">21:00 PM</option>
              <option value="09:30 PM">21:30 PM</option>
              <option value="10:00 PM">22:00 PM</option>
              <option value="10:30 PM">22:30 PM</option>
              <option value="11:00 PM">23:00 PM</option>
              <option value="11:30 PM">23:30 PM</option>
              <option value="12:00 AM">00:00 AM</option>
            </select>
                  </div>
              </div>
            <div class="form-group col-md-12">
              <div class="row">
                <select name="transportation_taxi" class="form-control">
          <option selected="selected">Vehicle Type - Cab / Mini Coach / Bus</option>
          <optgroup label="Mini &amp; Sedan Cars">
          <option value="AC Tata Indigo (4+1Driver)">AC Tata Indigo (4+1Driver)</option>
          <option value="AC Swift Dzire (4+1Driver)">AC Swift Dzire (4+1Driver)</option>
          <option value="AC Hyundai Xcent (4+1Driver)">AC Hyundai Xcent (4+1Driver)</option>
          <option value="AC Chevrolet Sail (4+1Driver)">AC Chevrolet Sail (4+1Driver)</option>
          <option value="AC Honda Amaze (4+1Driver)">AC Honda Amaze (4+1Driver)</option>
          <option value="AC Toyota Etios (4+1Driver)">AC Toyota Etios (4+1Driver)</option>
          <option value="AC Honda City (4+1Driver)">AC Honda City (4+1Driver)</option>
          <option value="AC Hyundai Verna (4+1Driver)">AC Hyundai Verna (4+1Driver)</option>
          <option value="AC Maruti Suzuki Ciaz (4+1Driver)">AC Maruti Suzuki Ciaz (4+1Driver)</option>
          <option value="AC Chevrolet Cruze (4+1Driver)">AC Chevrolet Cruze (4+1Driver)</option>
          <option value="AC Corolla Altis (4+1Driver)">AC Corolla Altis (4+1Driver)</option>
          </optgroup>
          <optgroup label="SUVs Cars">
          <option value="AC Chevrolet Enjoy (7+1Driver)">AC Chevrolet Enjoy (7+1Driver)</option>
          <option value="AC Mahindra Scorpio (6+1Driver)">AC Mahindra Scorpio (6+1Driver)</option>
          <option value="AC Chevrolet Tavera (9+1Driver)">AC Chevrolet Tavera (9+1Driver)</option>
          <option value="AC Toyota Innova (7+1Driver)">AC Toyota Innova (7+1Driver)</option>
          <option value="AC Toyota Innova Crysta (7+1Driver)">AC Toyota Innova Crysta (7+1Driver)</option>
                  <option value="AC Toyota Fortuner (7+1Driver)">AC Toyota Fortuner (7+1Driver)</option>
          </optgroup>
          <optgroup label="Premium Cars">
         <option value="Mercedes Benz E250">Mercedes Benz E250</option>
         <option value="Audi A6">Audi A6</option>
         <option value="Jaguar XF">Jaguar XF</option>
        
         <option value="Audi A4">Audi A4</option>
          <option value="Audi Q3">Audi Q3</option>
         <option value="BMW 3 Series">BMW 3 Series</option>
          </optgroup>
          
          <optgroup label="AC Winger">
  <option value="AC 9 Seater Luxury Winger Van">AC 9 Seater Luxury Winger Van</option>
  </optgroup>
  <optgroup label="AC Tempo Traveller">
  <option value="AC 13 Seater Tempo Traveller Cab">AC 13 Seater Tempo Traveller Cab</option>
  <option value="AC 17 Seater Tempo Traveller Cab">AC 17 Seater Tempo Traveller Cab</option>
  <option value="AC 26 Seater Tempo Traveller Cab">AC 26 Seater Tempo Traveller Cab</option>
  </optgroup>
  <optgroup label="AC Luxury SML COACH">
  <option value="AC 13 Seater SML Coach">AC 13 Seater SML Coach</option>
  <option value="AC 18 Seater SML Coach">AC 18 Seater SML Coach</option>
  <option value="AC 28 Seater SML Coach">AC 28 Seater SML Coach</option>
  </optgroup>
  <optgroup label="AC Luxury Coach">
  <option value="AC 44 Seater Luxury Bus">AC 44 Seater Luxury Bus</option>
  <option value="AC 45 Seater Volvo Bus">AC 45 Seater Volvo Bus</option>
  </optgroup>
         
          <optgroup label="AC Luxury Tempo Traveller">
          <option value="AC Luxury Force Traveller (15+1Driver)">AC Luxury Force Traveller (15+1Driver)</option>
          <option value="AC Luxury Force Traveller (18+1Driver)">AC Luxury Force Traveller (18+1Driver)</option>
          </optgroup>
          
        </select>
                </div>
              </div>
              <div class="form-group col-md-12">
                  <div class="row">
              Need Hotel Booking&nbsp;&nbsp;<input type="radio" name="yes" value="yes">&nbsp;Yes&nbsp;&nbsp;<input type="radio" name="yes" value="No">&nbsp;No
              </div></div>
            <textarea class="form-control" placeholder="Taxi Requirements & Preferences ( If Any )..."></textarea><br>
              <h4>Your Contact Information</h4>
              <div class="form-group col-md-12">
              <div class="row">
                  <input type="text" placeholder="Full Name..." class="form-control">
                  </div>
              </div>
               <div class="form-group col-md-12">
              <div class="row">
                  <input type="text" placeholder="Email Id..." class="form-control">
                  </div>
              </div>
              <div class="form-group col-md-12">
              <div class="row">
                  <input type="tel" placeholder="Contact Number..." class="form-control">
                  </div>
              </div>
              <div class="form-group col-md-4">
              <div class="row">
                  <input type="text" class="form-control" value="7654">
                  </div>
              </div>
               <div class="form-group col-md-8">
              <div class="row pdlft">
                  <input type="text" placeholder="Enter Captcha code.." class="form-control">
                  </div>
              </div>
              <button type="submit" class="btn  btn-block custom">Submit</button>
            </form>
        </div>
        
        
    </div>
       
       <?php
include("footer.php");
?>
   
    </body>
    </html>