<?php
session_start();
include ('dbcon.php');
include('header.php');
?>

   <div class="inner-banner">
       <img src="images/about-us-ban.jpg" alt="" class="img-responsive">
       <h3>ABOUT US</h3>
    </div>
<div class="head-bg">
<div class="container">
<ul class="breadcrumb">
  <li><a href="index.html">Home</a></li>
  <li><a>About Us</a></li>
</ul></div>
    </div>
        <div class="clearfix"></div>
        <div class="container">
    <div class="about">
    <div class="col-md-8">
        <div class="row">
            <img src="images/dh.jpg" alt="" class="img-responsive"></div></div>
        <div class="col-md-4">
      <h3 class="get-in-touch">GET IN TOUCH</h3>
          <form class="sidebar-form" name="abt_form" id="abt_form" method="POST">
           <input type="text" placeholder="Full Name" class="form-control" name="abt_fname" id="abt_fname" required="required"><br>
            <input type="text" placeholder="Email Id" class="form-control" name="abt_email" id="abt_email" required="required"><br>
            <input type="tel" placeholder="Contact Number" class="form-control" name="abt_phn" id="abt_phn" required="equired"> <br>
            <textarea class="form-control" placeholder="Message..." name="abt_msg" id="abt_msg"></textarea><br>
            <button type="submit" class="btn  btn-block custom" id="abt_sbmt" name="abt_sbmt">Submit</button>
            </form>
        </div>
        <div class="col-md-12">
            <div class="row">
                <?php
                $code="select * from add_page where status=1 and page_title='about-us'";
                $aresult=mysqli_query($con,$code);
                $rowcnt=mysqli_num_rows($aresult);
                $a=1;
                if($rowcnt>0)
                {
                    while($aquery=mysqli_fetch_assoc($aresult))
                    {
                        ?>
                        <?=$aquery['page_content']; ?>
                        
                        <?php
                    }
                    $a++;
                }
                else{
                    echo 'no result found';
                }
                ?>
        </div>
        
            <h3>Our Team</h3>
            <?php
                $sql="select * from our_team where status=1";
                $result=mysqli_query($con,$sql);
                $rowcount=mysqli_num_rows($result);
                $i=1;
                if($rowcount>0){
                    
                    while($query=mysqli_fetch_array($result))
                    {
            ?>
                    <div class="col-md-3 col-sm-6 col-xs-12 pad0">
                        <div class="about-team">
                            <div class="team">
                                <img src="admin/team-images/<?=$query['image']; ?>" class="img-responsive">
                                </div> 
                                <div class="info">
                                <h4><?=$query['name']; ?></h4>
                                <p><?=$query['designation']; ?></p><p><?=$query['phone']; ?></p><p><?=$query['email']; ?></p>
                            </div> 
                        </div>
                    </div>
            <?php
                }
                 $i++;
            }
            else{
                echo 'Result not found';
            }
            ?>
               
            </div>
        
    </div>
        </div>
        <?php
include("footer.php");
?>

<script>

    $(document).ready(function(){
        
        $("#abt_form").submit(function (e) {
            
    	e.preventDefault();
    	
		$.ajax({
			type: $('#abt_form').attr('method'),
			url: 'about_ajax.php',
			data: $('#abt_form').serialize(),
			success: function (data) {
				alertify.alert(data);
				$("#abt_form")[0].reset();
				//document.getElementById("cform").reset();
			},
			error: function (data) {
				console.log('An error occurred.');
			},
		});
		
        return false;
    });
    
    });
    
</script>
   
    </body>
    </html>