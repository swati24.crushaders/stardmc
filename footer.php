        <div class="footer footer-banner mgntpbt">
            <div class="container">
        <div class="col-md-3 mgntpbt">
            <h4>ADDRESS</h4>
            <p class="loc">Plot No 72, Kharvel Nagar,<br>&nbsp;&nbsp;&nbsp;&nbsp;Master Canteen Road<br>&nbsp;&nbsp;&nbsp;&nbsp;Unit-3, Kharvel Nagar,<br>&nbsp;&nbsp;&nbsp;&nbsp;Bhubaneswar, 751001<br>&nbsp;&nbsp;&nbsp;&nbsp;INDIA</p>
            <p class="call">7735749753/ 52/54/56/59/63</p><p class="call">7032200357/ 7735746608</p>
            </div>
            <div class="col-md-3 footer-link mgntpbt">
                <h4>QUICL LINKS</h4>
                <ul>
                <li><a href="about-us.php">About us</a></li>
                <li><a href="#">FAQs</a></li>
                <li><a href="#">Terms & Condition</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Disclaimer</a></li>
                <li><a href="#">Payment</a></li>
                <li><a href="#">Contact Us</a></li>
                </ul>
                </div>
            <div class="col-md-3 mgntpbt">
                <h4>PAYMENT  OPTIONS</h4>
                <img src="images/payment.png" alt="">
                </div>
            <div class="col-md-3 mgntpbt">
                <h4>SOCIAL LINKS</h4>
                <ul class="social">
                <li><a href="#"><img src="images/fb.png" alt=""></a></li>
                <li><a href="#"><img src="images/twit.png" alt=""></a></li>
                <li><a href="#"><img src="images/utube.png" alt=""></a></li>
                </ul>
                  <p>Subscribe To Our Newsletter And Join 15,000 + Subscribers</p>
                <form>
                    <div class="form-group">
                     <input type="text" placeholder="Your  Mail" class="form-control">
                    </div>
                    <div class="form-group">
                     <button class="btn btn-info">SUBSCRIBE</button>
                    </div>
                </form>
                </div>
        </div>
        
        </div>
        <a class="feedback" href="feedback.php">FEEDBACK</a>
        <button type="button" class="btn enquiry" data-toggle="modal" data-target="#myModal-enquiry">
  ENQUIRY NOW</button>
<div class="modal" id="myModal-enquiry">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Enquiry now</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       <form>
           <div class="form-group">
          <input type="text" placeholder="name" class="form-control">
          </div>
           <div class="form-group">
          <input type="text" placeholder="email" class="form-control">
          </div>
           <div class="form-group">
          <input type="text" placeholder="phone" class="form-control">
          </div>
           <div class="form-group">
          <textarea class="form-control" placeholder="message"></textarea>
          </div>
           <button class="btn submit">submit</button>
          </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<div class="modal" id="abc">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Get Customize Tour Package</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body holiday-customize">
          <form id="regForm" action="/action_page.php">
  <!-- One "tab" for each step in the form: -->
  <div class="tab">
      <h4>Step-1/3</h4>
    <div class="col-md-6">
        <div class="form-group">
        <input type="text" placeholder="Full name..."  class="form-control"  name="fname"></div></div>

    <div class="col-md-6">
        <div class="form-group">
        <input  type="text" placeholder="Email"  class="form-control"  name="email"></div></div>


      <div class="col-md-6">
        <div class="form-group">
        <input type="tel" placeholder="Contact No."  class="form-control"  name="phn"></div></div>


    <div class="col-md-6">
        <div class="form-group">
        <select class="form-control" name="cntry">
            <option>Country</option>
            <option>India</option>
            </select></div></div>

  </div>
  <div class="tab">
      <h4>Step-2/3</h4>
    <div class="col-md-6">
        <div class="form-group">
        <input type="date" placeholder="Arrival Date" name="avdate" class="form-control">
        </div></div>
    <div class="col-md-6">
        <div class="form-group">
        <input type="date" placeholder="Deparature Date" name="dpdate" class="form-control"></div></div>
      <div class="col-md-6">
        <div class="form-group">
        <input type="number" placeholder="No. of Adults"  class="form-control"  name="adlt"></div></div>
      <div class="col-md-6">
        <div class="form-group">
        <input type="number" placeholder="No. of Child"  class="form-control"  name="chld"></div></div>
  </div>
  <div class="tab">
      <h4>Step-3/3</h4>
    <div class="col-md-6">
        <div class="form-group">
        <input type="text"  placeholder="Trip Duration"  class="form-control"  name="trpdu"></div></div>
    <div class="col-md-6">
        <div class="form-group">
        <select name="transport" class="form-control">
            <option>Type of Transport</option>
            <option>Mini Car</option>
            <option>Suv</option>
            </select></div></div>
    <div class="col-md-6">
            Already booked a flight&nbsp;&nbsp;<input type="radio" name="x" value="yes">&nbsp;Yes<input type="radio" name="x" value="No">&nbsp;No
            </div>
      <div class="col-md-6">
    Need Taxi for  your Trip&nbsp;&nbsp;<input type="radio" name="yes" value="yes">&nbsp;Yes<input type="radio" name="yes" value="no">&nbsp;No</div>
      <div class="col-md-12">
      <div class="form-group">
          <textarea class="form-control" name="tour_reqmnt" placeholder="Tour Requirement & References(if any...)"></textarea>
          </div>
      </div>
  </div>
<!--
  <div style="overflow:auto;">
    <div style="float:right;">
-->
      <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
      <button type="button" id="nextBtn" onclick="nextPrev(1)" style="float:right">Next</button>
<!--
    </div>
  </div>
-->
  <!-- Circles which indicates the steps of the form: -->
  <div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
  </div>
</form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
    </body>
    </html>