<?php
require 'class/SMTPMailer.php';
$mail = new SMTPMailer();
//$ip=$_SERVER['REMOTE_ADDR'];

    extract($_POST);
    $mail->addTo('girijanandini.crushaders@gmail.com');
    $mail->addCc($hotel_email);
    $mail->Subject('Hotel Booking Enquiry');
    $mail->Body(
            '<html><body>

            <table border="0" cellspacing="0" cellpadding="0" width="99%" style="width:99.0%;background:rgb(234,234,234)">
            <tbody><tr><td style="padding:0.75pt 0.75pt 0.75pt 0.75pt"><table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;background:white"><tbody>
            
            <tr><td colspan="2" style="background:rgb(234,242,250);padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><strong><span style="font-size:9.0pt;font-family:Arial,sans-serif">Visitor Name</span></strong><span style="font-size:12.0pt"></span></p></td></tr>
            <tr><td width=20 style="width:15.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal">&nbsp;<span style="font-size:12.0pt"></span></p></td><td style="padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><span style="font-size:9.0pt;font-family:Arial,sans-serif">' . $hotel_fname. '</span><span style="font-size:12.0pt"></span></p></td></tr>
            
            <tr><td colspan="2" style="background:rgb(234,242,250);padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><strong><span style="font-size:9.0pt;font-family:Arial,sans-serif">Visitor Email Id</span></strong><span style="font-size:12.0pt"></span></p></td></tr>
            <tr><td width=20 style="width:15.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal">&nbsp;<span style="font-size:12.0pt"></span></p></td><td style="padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><span style="font-size:9.0pt;font-family:Arial,sans-serif">' . $hotel_email. '</span><span style="font-size:12.0pt"></span></p></td></tr>
            
            <tr><td colspan="2" style="background:rgb(234,242,250);padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><strong><span style="font-size:9.0pt;font-family:Arial,sans-serif">Visitor Phone</span></strong><span style="font-size:12.0pt"></span></p></td></tr>
            <tr><td width=20 style="width:15.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal">&nbsp;<span style="font-size:12.0pt"></span></p></td><td style="padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><span style="font-size:9.0pt;font-family:Arial,sans-serif">' . $hotel_phn. '</span><span style="font-size:12.0pt"></span></p></td></tr>
            
            <tr><td colspan="2" style="background:rgb(234,242,250);padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><strong><span style="font-size:9.0pt;font-family:Arial,sans-serif">Choose a location</span></strong><span style="font-size:12.0pt"></span></p></td></tr>
            <tr><td width=20 style="width:15.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal">&nbsp;<span style="font-size:12.0pt"></span></p></td><td style="padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><span style="font-size:9.0pt;font-family:Arial,sans-serif">' . $hotel_loc. '</span><span style="font-size:12.0pt"></span></p></td></tr>
            
            <tr><td colspan="2" style="background:rgb(234,242,250);padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><strong><span style="font-size:9.0pt;font-family:Arial,sans-serif">Checkin</span></strong><span style="font-size:12.0pt"></span></p></td></tr>
            <tr><td width=20 style="width:15.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal">&nbsp;<span style="font-size:12.0pt"></span></p></td><td style="padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><span style="font-size:9.0pt;font-family:Arial,sans-serif">' . $hotel_chkin. '</span><span style="font-size:12.0pt"></span></p></td></tr>
            
            <tr><td colspan="2" style="background:rgb(234,242,250);padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><strong><span style="font-size:9.0pt;font-family:Arial,sans-serif">Checkout</span></strong><span style="font-size:12.0pt"></span></p></td></tr>
            <tr><td width=20 style="width:15.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal">&nbsp;<span style="font-size:12.0pt"></span></p></td><td style="padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><span style="font-size:9.0pt;font-family:Arial,sans-serif">' . $hotel_chkout. '</span><span style="font-size:12.0pt"></span></p></td></tr>
            
            <tr><td colspan="2" style="background:rgb(234,242,250);padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><strong><span style="font-size:9.0pt;font-family:Arial,sans-serif">No. of Adults</span></strong><span style="font-size:12.0pt"></span></p></td></tr>
            <tr><td width=20 style="width:15.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal">&nbsp;<span style="font-size:12.0pt"></span></p></td><td style="padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><span style="font-size:9.0pt;font-family:Arial,sans-serif">' . $hotel_adlt. '</span><span style="font-size:12.0pt"></span></p></td></tr>
            
            <tr><td colspan="2" style="background:rgb(234,242,250);padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><strong><span style="font-size:9.0pt;font-family:Arial,sans-serif">No. of Children</span></strong><span style="font-size:12.0pt"></span></p></td></tr>
            <tr><td width=20 style="width:15.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal">&nbsp;<span style="font-size:12.0pt"></span></p></td><td style="padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><span style="font-size:9.0pt;font-family:Arial,sans-serif">' . $hotel_child. '</span><span style="font-size:12.0pt"></span></p></td></tr>
            
            <tr><td colspan="2" style="background:rgb(234,242,250);padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><strong><span style="font-size:9.0pt;font-family:Arial,sans-serif">Hotel Type</span></strong><span style="font-size:12.0pt"></span></p></td></tr>
            <tr><td width=20 style="width:15.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal">&nbsp;<span style="font-size:12.0pt"></span></p></td><td style="padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><span style="font-size:9.0pt;font-family:Arial,sans-serif">' . $hotel_type. '</span><span style="font-size:12.0pt"></span></p></td></tr>
            
            <tr><td colspan="2" style="background:rgb(234,242,250);padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><strong><span style="font-size:9.0pt;font-family:Arial,sans-serif">No. of Rooms</span></strong><span style="font-size:12.0pt"></span></p></td></tr>
            <tr><td width=20 style="width:15.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal">&nbsp;<span style="font-size:12.0pt"></span></p></td><td style="padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><span style="font-size:9.0pt;font-family:Arial,sans-serif">' . $room. '</span><span style="font-size:12.0pt"></span></p></td></tr>
            
            <tr><td colspan="2" style="background:rgb(234,242,250);padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><strong><span style="font-size:9.0pt;font-family:Arial,sans-serif">Need Taxi for  your Trip</span></strong><span style="font-size:12.0pt"></span></p></td></tr>
            <tr><td width=20 style="width:15.0pt;padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal">&nbsp;<span style="font-size:12.0pt"></span></p></td><td style="padding:3.75pt 3.75pt 3.75pt 3.75pt"><p class="MsoNormal"><span style="font-size:9.0pt;font-family:Arial,sans-serif">' . $yes. '</span><span style="font-size:12.0pt"></span></p></td></tr>
            </tbody></table></td></tr></tbody></table>
            </body></html>'
         );

    if ($mail->Send())
    {
	    echo "Thank You For Tour Package Enquiry. We Will Contact You Soon..";
        // echo "<script>alert('Thank You For Tour Package Enquiry. We Will Contact You Soon..');location.href='http://demo.ctslproject.com/stardmc.com/';</script>";
    }
    else
    {
        echo "There is some errors. Please try after sometimes";
    }

?>