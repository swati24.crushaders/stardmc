$(document).ready(function() {
$("#content-slider").lightSlider({
	loop:true,
	keyPress:true,
	speed:2000,
	auto:false,
	loop:true,

	responsive : [
		{
			breakpoint: 600,
			settings: { 
				item: 1,
				slideMove: 1,
				slideMargin: 6
			}
		},
		{
			breakpoint: 480,
			settings: { 
				item: 1,
				slideMove: 1
			}
		}
	]
});
responsive : [
	{
		breakpoint: 992,
		settings: {
			item: 4,
			slideMove: 1,
			slideMargin: 6
		}
	},
	{    breakpoint: 684,
	 settings: { 
		 item: 3,
		 slideMove: 1,
		 slideMargin: 6
	 }
	},
	{    breakpoint: 500,
	 settings: { 
		 item: 2,
		 slideMove: 1,
		 slideMargin: 6
	 }
	},
	{
		breakpoint: 321,
		settings: {  
			item: 1,
			slideMove: 1
		}
	}
]

});

$(document).ready(function() {
$("#hotel").lightSlider({
	loop:true,
	keyPress:true,
	speed:2000,
	auto:false,
	loop:true,

	responsive : [
		{
			breakpoint: 600,
			settings: { 
				item: 1,
				slideMove: 1,
				slideMargin: 6
			}
		},
		{
			breakpoint: 480,
			settings: { 
				item: 1,
				slideMove: 1
			}
		}
	]
});
responsive : [
	{
		breakpoint: 992,
		settings: {
			item: 4,
			slideMove: 1,
			slideMargin: 6
		}
	},
	{    breakpoint: 684,
	 settings: { 
		 item: 3,
		 slideMove: 1,
		 slideMargin: 6
	 }
	},
	{    breakpoint: 500,
	 settings: { 
		 item: 2,
		 slideMove: 1,
		 slideMargin: 6
	 }
	},
	{
		breakpoint: 321,
		settings: {  
			item: 1,
			slideMove: 1
		}
	}
]


});
 $(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            $(this).toggleClass('open');       
        }
    );
       $(".dropdown").click(function(){
           location.href = "car-rentals.php";
       });  
});

$(document).ready(function(){
    $("#dropdown-place").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            $(this).toggleClass('open');       
        }
    );
      $("#dropdown-place").click(function(){
          location.href = "places.php";
      });  
});

	$(document).ready(function(){
                $(".review-book").click(function() {
       $('.review').show();
       });
        $(".cancel").click(function() {
       $('.review').hide("fast");
       });
                  //  $('.cancel').slideup(2000);
            });	
            
$(function() {
   $("input[name='x']").click(function() {
     if ($("#include").is(":checked")) {
       $(".include-content").show();
         $(".exclude-content").hide();
     } else {
       $(".include-content").hide();
          $(".exclude-content").show();
     }
   });
 });
            
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}


var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
      alert('Submited Successfully');
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}

function readURL(input) {
if (input.files && input.files[0])
{
var reader = new FileReader();
reader.onload = function(e) {
$('#std_photo').attr('src', e.target.result);
}
reader.readAsDataURL(input.files[0]);
}
}
$("#img_upload").change(function() {
readURL(this);
});

