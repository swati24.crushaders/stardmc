<?php
session_start();
include('header.php');
?>
<div class="banner">
    <div class="container">
        <div class="demo">
            <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#TOUR-PACKAGE">TOUR PACKAGE</a></li>
            <li><a data-toggle="tab" href="#TAXI-PACKAGE">TAXI PACKAGE</a></li>
            <li><a data-toggle="tab" href="#Airport">Airport/ Railway transfer</a></li>
            <li><a data-toggle="tab" href="#Hotel-Booking">Hotel Booking</a></li>
            <li><a data-toggle="tab" href="#Wedding-Car"> Wedding Car</a></li>
            </ul>
          <div class="tab-content">
            <div id="TOUR-PACKAGE" class="tab-pane fade in active">
              <div class="col-md-12 white">
               <div class="col-md-7">
                    <div class="row">
                        <h3>GET A  FREE   QUOTATION  FROM OUR TRAVEL EXPERTS</h3>
                        <h4>[For Best Tour Request,Best price Quotation,Best Itinerary Suggestion]</h4>
                        <form name="tour_form" method="POST" id="tour_form" >
                            <!--<form name="tour-form" action="save.php" method="post" data-toggle="validator" novalidate="true">-->
                            <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input placeholder="Arrival Date" class="form-control" name="arivedate" type="text" onfocus="(this.type='date')" value=""  id="arivedate" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Departure Date" name="departdate" type="text" onfocus="(this.type='date')" value=""  id="departdate" required="required">
                                    
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="number" class="form-control" name="adult" placeholder="No of Adults" value="" required="required" id="adult">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="number" class="form-control" name="child" placeholder="No of Chidren" value="" id="child" required="required">
                                  
                                </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="city" placeholder="Your City(Departure City)" id="city" value="" required="required">
                                    
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="vduration" placeholder="Tour Duration" value="" id="vduration" required="required" >
                    
                                </div>
                            </div>
                            </div>
                            <div class="form-group row">
                            <div class="col-md-12">
                               <textarea  class="form-control" name="vreq" id="vreq" placeholder="Tour Requirements & Preferences(if Any..)"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                            <label>YOUR CONTACT INFORMATION</label>
                            </div>
                            <div class="row">
                               <div class="col-md-6">
                                   <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Full name.." name="vname" value="" id="vname" required="required" >
                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Email Address..." name="vemail" id="vemail" value="" required="required" >
                                       
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="tel" class="form-control" placeholder="Contact Number" name="vphn" id="vphn" value="" required="required" >
                                        
                                    </div>
                                </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Address" value="" name="vaddr" id="vaddr" required="required" >
                                   
                                </div>
                            </div> 
                        </div>
                        <button type="submit" class="btn btn-info submit" name="tourbtn" id="submitId">SUBMIT</button>
                    </form>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row">
                <div class="head-image">
                  <img src="images/puri.jpg" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
</div>
            <div id="TAXI-PACKAGE" class="tab-pane fade">
              <div class="col-md-12 white">
               <div class="col-md-7">
                   <div class="row">
                  <h3>GET A  FREE   QUOTATION  FROM OUR TRAVEL EXPERTS</h3>
                  <h4>[For Best Tour Request,Best price Quotation,Best Itinerary Suggestion]</h4>
                    <form name="taxi_form" method="POST" id="taxi_form">
                        <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <select name="taxi_pick" id="taxi_pick" class="form-control" >
                        <option selected="selected">Pick up Location</option>
                        <option value=" Bhubaneswar Airport (BBI)"> Bhubaneswar Airport (BBI)</option>
                        <option value="Bhubaneswar Railway Station ">Bhubaneswar Railway Station </option>
                        <option value="Bhubaneswar Hotels">Bhubaneswar Hotels</option>
                        <option value="Puri Railway Station ">Puri Railway Station </option>
                        <option value="Puri Hotels">Puri Hotels</option>
                        <option value="Puri Area">Puri Area</option>
                        <option value="Khurdha Jn Railway Station">Khurdha Jn Railway Station</option>
                        <option value="Cuttack Railway Station">Cuttack Railway Station</option>
                        <option value="Berhampur Railway Station">Berhampur Railway Station</option>
                        <option value="Angul Odisha India">Angul Odisha India</option>
                        <option value="Bhubaneswar Odisha India">Bhubaneswar Odisha India</option>
                        <option value="Brahmagiri Odisha India">Brahmagiri Odisha India</option>
                        <option value="Bhitarkanika Odisha India">Bhitarkanika Odisha India</option>
                        <option value="Brahmapur Odisha India">Brahmapur Odisha India</option>
                        <optgroup label="----------------------------------------"></optgroup>
                        <option value="Balangir Odisha India">Balangir Odisha India</option>
                        <option value="Balasore Odisha India ">Balasore Odisha India </option>
                        <option value="Barbil Odisha India">Barbil Odisha India</option>
                        <option value="Bargarh Odisha India">Bargarh Odisha India</option>
                        <option value="Baripada Odisha India">Baripada Odisha India</option>
                        <option value="Bhadrak Odisha India">Bhadrak Odisha India</option>
                        <option value="Balugaon Odisha India">Balugaon Odisha India</option>
                        <option value="Bhawanipatna Odisha India">Bhawanipatna Odisha India</option>
                        <option value="Balliguda Odisha India">Balliguda Odisha India</option>
                        <option value="Boudh Odisha India">Boudh Odisha India</option>
                        <option value="Chilika Lake Odisha India">Chilika Lake Odisha India</option>
                        <option value="Cuttack Odisha India">Cuttack Odisha India</option>
                        <option value="Chandipur Odisha India">Chandipur Odisha India</option>
                        <option value="Chhatrapur Odisha India">Chhatrapur Odisha India</option>
                        <option value="Daringbadi Odisha India">Daringbadi Odisha India</option>
                        <option value="Dhabaleswar Odisha India">Dhabaleswar Odisha India</option>
                        <option value="Dhenkanal Odisha India">Dhenkanal Odisha India</option>
                        <option value="Deogarh Odisha India">Deogarh Odisha India</option>
                        <option value="Damanjodi Odisha India">Damanjodi Odisha India</option>
                        <option value="Gopalpur Odisha India">Gopalpur Odisha India</option>
                        <option value="Ganjam Odisha India">Ganjam Odisha India</option>
                        <option value="Jajpur Odisha India">Jajpur Odisha India</option>
                        <option value="Jaleswar Odisha India">Jaleswar Odisha India</option>
                        <option value="Jharsuguda Odisha India">Jharsuguda Odisha India</option>
                        <option value="Jagdalpur Odisha India">Jagdalpur Odisha India</option>
                        <option value="Jeypore Odisha India">Jeypore Odisha India</option>
                        <option value="Jagatsinghpur Odisha India">Jagatsinghpur Odisha India</option>
                        <option value="Khurdha Odisha India">Khurdha Odisha India</option>
                        <option value="Konark Odisha India">Konark Odisha India</option>
                        <option value="Kendrapara Odisha India">Kendrapara Odisha India</option>
                        <option value="Koraput Odisha India">Koraput Odisha India</option>
                        <option value="Keonjhar Odisha India">Keonjhar Odisha India</option>
                        <option value="Mayurbhanj Odisha India">Mayurbhanj Odisha India</option>
                        <option value="Muniguda Odisha India">Muniguda Odisha India</option>
                        <option value="Malkangiri Odisha India">Malkangiri Odisha India</option>
                        <option value="Nayagarh Odisha India">Nayagarh Odisha India</option>
                        <option value="Nowrangpur Odisha India">Nowrangpur Odisha India</option>
                        <option value="Nuapada Odisha India">Nuapada Odisha India</option>
                        <option value="Puri Odisha India">Puri Odisha India</option>
                        <option value="Paradip Odisha India">Paradip Odisha India</option>
                        <option value="Phulbani Odisha India">Phulbani Odisha India</option>
                        <option value="Paralakhemundi Odisha India">Paralakhemundi Odisha India</option>
                        <option value="Rayagada Odisha India">Rayagada Odisha India</option>
                        <option value="Rourkela Odisha India">Rourkela Odisha India</option>
                        <option value="Satapada Odisha India">Satapada Odisha India</option>
                        <option value="SakhiGopal Odisha India">SakhiGopal Odisha India</option>
                        <option value="Sambalpur Odisha India">Sambalpur Odisha India</option>
                        <option value="Sunabeda Odisha India">Sunabeda Odisha India</option>
                        <option value="Sundargarh Odisha India">Sundargarh Odisha India</option>
                        <option value="Sonepur Odisha India">Sonepur Odisha India</option>
                        <option value="Simlipal Odisha India">Simlipal Odisha India</option>
                        <option value="Titlagarh Odisha India">Titlagarh Odisha India</option>
                        <option value="Talcher Odisha India">Talcher Odisha India</option>
                        <option value="Taptapani Odisha India">Taptapani Odisha India</option>
                        <option value="taxifrom">Other Location</option>
                      </select>
                     </div></div>
                            <div class="col-md-6">
                                <div class="form-group">
                            <select name="taxi_drop" id="taxi_drop" class="form-control" >
                        <option selected="selected">Drop Location</option>
                        <option value=" Bhubaneswar Airport (BBI)"> Bhubaneswar Airport (BBI)</option>
                        <option value="Bhubaneswar Railway Station ">Bhubaneswar Railway Station </option>
                        <option value="Bhubaneswar Hotels">Bhubaneswar Hotels</option>
                        <option value="Puri Railway Station ">Puri Railway Station </option>
                        <option value="Puri Hotels">Puri Hotels</option>
                        <option value="Puri Area">Puri Area</option>
                        <option value="Khurdha Jn Railway Station">Khurdha Jn Railway Station</option>
                        <option value="Cuttack Railway Station">Cuttack Railway Station</option>
                        <option value="Berhampur Railway Station">Berhampur Railway Station</option>
                        <option value="Angul Odisha India">Angul Odisha India</option>
                        <option value="Bhubaneswar Odisha India">Bhubaneswar Odisha India</option>
                        <option value="Brahmagiri Odisha India">Brahmagiri Odisha India</option>
                        <option value="Bhitarkanika Odisha India">Bhitarkanika Odisha India</option>
                        <option value="Brahmapur Odisha India">Brahmapur Odisha India</option>
                        <optgroup label="----------------------------------------"></optgroup>
                        <option value="Balangir Odisha India">Balangir Odisha India</option>
                        <option value="Balasore Odisha India ">Balasore Odisha India </option>
                        <option value="Barbil Odisha India">Barbil Odisha India</option>
                        <option value="Bargarh Odisha India">Bargarh Odisha India</option>
                        <option value="Baripada Odisha India">Baripada Odisha India</option>
                        <option value="Bhadrak Odisha India">Bhadrak Odisha India</option>
                        <option value="Balugaon Odisha India">Balugaon Odisha India</option>
                        <option value="Bhawanipatna Odisha India">Bhawanipatna Odisha India</option>
                        <option value="Balliguda Odisha India">Balliguda Odisha India</option>
                        <option value="Boudh Odisha India">Boudh Odisha India</option>
                        <option value="Chilika Lake Odisha India">Chilika Lake Odisha India</option>
                        <option value="Cuttack Odisha India">Cuttack Odisha India</option>
                        <option value="Chandipur Odisha India">Chandipur Odisha India</option>
                        <option value="Chhatrapur Odisha India">Chhatrapur Odisha India</option>
                        <option value="Daringbadi Odisha India">Daringbadi Odisha India</option>
                        <option value="Dhabaleswar Odisha India">Dhabaleswar Odisha India</option>
                        <option value="Dhenkanal Odisha India">Dhenkanal Odisha India</option>
                        <option value="Deogarh Odisha India">Deogarh Odisha India</option>
                        <option value="Damanjodi Odisha India">Damanjodi Odisha India</option>
                        <option value="Gopalpur Odisha India">Gopalpur Odisha India</option>
                        <option value="Ganjam Odisha India">Ganjam Odisha India</option>
                        <option value="Jajpur Odisha India">Jajpur Odisha India</option>
                        <option value="Jaleswar Odisha India">Jaleswar Odisha India</option>
                        <option value="Jharsuguda Odisha India">Jharsuguda Odisha India</option>
                        <option value="Jagdalpur Odisha India">Jagdalpur Odisha India</option>
                        <option value="Jeypore Odisha India">Jeypore Odisha India</option>
                        <option value="Jagatsinghpur Odisha India">Jagatsinghpur Odisha India</option>
                        <option value="Khurdha Odisha India">Khurdha Odisha India</option>
                        <option value="Konark Odisha India">Konark Odisha India</option>
                        <option value="Kendrapara Odisha India">Kendrapara Odisha India</option>
                        <option value="Koraput Odisha India">Koraput Odisha India</option>
                        <option value="Keonjhar Odisha India">Keonjhar Odisha India</option>
                        <option value="Mayurbhanj Odisha India">Mayurbhanj Odisha India</option>
                        <option value="Muniguda Odisha India">Muniguda Odisha India</option>
                        <option value="Malkangiri Odisha India">Malkangiri Odisha India</option>
                        <option value="Nayagarh Odisha India">Nayagarh Odisha India</option>
                        <option value="Nowrangpur Odisha India">Nowrangpur Odisha India</option>
                        <option value="Nuapada Odisha India">Nuapada Odisha India</option>
                        <option value="Puri Odisha India">Puri Odisha India</option>
                        <option value="Paradip Odisha India">Paradip Odisha India</option>
                        <option value="Phulbani Odisha India">Phulbani Odisha India</option>
                        <option value="Paralakhemundi Odisha India">Paralakhemundi Odisha India</option>
                        <option value="Rayagada Odisha India">Rayagada Odisha India</option>
                        <option value="Rourkela Odisha India">Rourkela Odisha India</option>
                        <option value="Satapada Odisha India">Satapada Odisha India</option>
                        <option value="SakhiGopal Odisha India">SakhiGopal Odisha India</option>
                        <option value="Sambalpur Odisha India">Sambalpur Odisha India</option>
                        <option value="Sunabeda Odisha India">Sunabeda Odisha India</option>
                        <option value="Sundargarh Odisha India">Sundargarh Odisha India</option>
                        <option value="Sonepur Odisha India">Sonepur Odisha India</option>
                        <option value="Simlipal Odisha India">Simlipal Odisha India</option>
                        <option value="Titlagarh Odisha India">Titlagarh Odisha India</option>
                        <option value="Talcher Odisha India">Talcher Odisha India</option>
                        <option value="Taptapani Odisha India">Taptapani Odisha India</option>
                        <option value="taxifrom">Other Location</option>
                      </select>
                            </div></div>
                        </div>
                        <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                            <input placeholder="Pickup Date" class="form-control" name="pickup_date" type="text" onfocus="(this.type='date')" value=""  id="pickup_date" required="required" >
                            </div></div>
                        <div class="col-md-3">
                            <div class="form-group">
                            <input type="text" class="form-control" placeholder="Pickup Time" name="pick_time" id="pick_time" required="required">
                           </div></div>
                            <div class="col-md-3">
                                <div class="form-group">
                            <input placeholder="Drop Date" class="form-control" name="drop_date" type="text" onfocus="(this.type='date')" value=""  id="drop_date" required="required" >
                            </div></div>
                            <div class="col-md-3">
                                <div class="form-group">
                            <input type="text" class="form-control" placeholder="Drop Time" name="drop_time" id="drop_time" required="required" >
                            </div></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                           <select name="veh_type" id="veh_type" class="form-control"  >
                               <option selected>Vehicle Type - Cab / Mini Coach / Bus</option>
                                </select>
                           </div></div>
                        <div class="col-md-6">
                            <div class="form-group">
                           <textarea  class="form-control" name="taxi_ref" id="taxi_ref" placeholder="Taxi Requirements & Preferences(if Any..)"></textarea>
                            </div></div>
                        </div>
                        <div class="form-group">
                        <label>YOUR CONTACT INFORMATION</label>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                               <div class="form-group">
                            <input type="text" class="form-control" name="taxi_fname" id="taxi_fname"  value=""  placeholder="Full name.." required="required">
                           </div></div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <input type="text" class="form-control" name="taxi_email" id="taxi_email"  value=""  placeholder="Email Address..." required="required">
                            </div></div>
                        </div>
                        
                        <div class="row">
                           <div class="col-md-6">
                               <div class="form-group">
                            <input type="tel" class="form-control" name="taxi_phn" id="taxi_phn"   placeholder="Contact Number" required="required">
                            </div></div>
                            <div class="col-md-2">
                                <div class="form-group">
                                <input type ="text" placeholder="7518" class="form-control">
                                </div></div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <input type="text" class="form-control" placeholder="Enter Captcha Code Here">
                            </div></div> 
                        </div>
                        
                        <button type="submit" class="btn btn-info submit" name="taxibtn" id="submitId_taxi">SUBMIT</button>
                        
                    </form>
                    
                </div>
             </div>
             
               <div class="col-md-5">
                   <div class="row">
                    <div class="head-image">
                  <img src="images/puri.jpg" class="img-responsive">
                       </div>
                       </div>
                       </div>
                  </div>
            </div>
            
            <div id="Airport" class="tab-pane fade">
              <div class="col-md-12 white">
               <div class="col-md-7">
                   <div class="row">
                  <h3>GET A  FREE   QUOTATION  FROM OUR TRAVEL EXPERTS</h3>
                  <h4>[For Best Tour Request,Best price Quotation,Best Itinerary Suggestion]</h4>
                    <form name="airport_form" method="POST" id="airport_form">
                        <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <select name="airport_pickup" id="airport_pickup" class="form-control">
                        <option selected="selected">Pick up From</option>
                        <option value=" Bhubaneswar Airport (BBI)"> Bhubaneswar Airport (BBI)</option>
                        <option value="Bhubaneswar Railway Station ">Bhubaneswar Railway Station </option>
                        <option value="Bhubaneswar Hotels">Bhubaneswar Hotels</option>
                        <option value="Puri Railway Station ">Puri Railway Station </option>
                        <option value="Puri Hotels">Puri Hotels</option>
                        <option value="Puri Area">Puri Area</option>
                        <option value="Khurdha Jn Railway Station">Khurdha Jn Railway Station</option>
                        <option value="Cuttack Railway Station">Cuttack Railway Station</option>
                        <option value="Berhampur Railway Station">Berhampur Railway Station</option>
                        <option value="Angul Odisha India">Angul Odisha India</option>
                        <option value="Bhubaneswar Odisha India">Bhubaneswar Odisha India</option>
                        <option value="Brahmagiri Odisha India">Brahmagiri Odisha India</option>
                        <option value="Bhitarkanika Odisha India">Bhitarkanika Odisha India</option>
                        <option value="Brahmapur Odisha India">Brahmapur Odisha India</option>
                        <optgroup label="----------------------------------------"></optgroup>
                        <option value="Balangir Odisha India">Balangir Odisha India</option>
                        <option value="Balasore Odisha India ">Balasore Odisha India </option>
                        <option value="Barbil Odisha India">Barbil Odisha India</option>
                        <option value="Bargarh Odisha India">Bargarh Odisha India</option>
                        <option value="Baripada Odisha India">Baripada Odisha India</option>
                        <option value="Bhadrak Odisha India">Bhadrak Odisha India</option>
                        <option value="Balugaon Odisha India">Balugaon Odisha India</option>
                        <option value="Bhawanipatna Odisha India">Bhawanipatna Odisha India</option>
                        <option value="Balliguda Odisha India">Balliguda Odisha India</option>
                        <option value="Boudh Odisha India">Boudh Odisha India</option>
                        <option value="Chilika Lake Odisha India">Chilika Lake Odisha India</option>
                        <option value="Cuttack Odisha India">Cuttack Odisha India</option>
                        <option value="Chandipur Odisha India">Chandipur Odisha India</option>
                        <option value="Chhatrapur Odisha India">Chhatrapur Odisha India</option>
                        <option value="Daringbadi Odisha India">Daringbadi Odisha India</option>
                        <option value="Dhabaleswar Odisha India">Dhabaleswar Odisha India</option>
                        <option value="Dhenkanal Odisha India">Dhenkanal Odisha India</option>
                        <option value="Deogarh Odisha India">Deogarh Odisha India</option>
                        <option value="Damanjodi Odisha India">Damanjodi Odisha India</option>
                        <option value="Gopalpur Odisha India">Gopalpur Odisha India</option>
                        <option value="Ganjam Odisha India">Ganjam Odisha India</option>
                        <option value="Jajpur Odisha India">Jajpur Odisha India</option>
                        <option value="Jaleswar Odisha India">Jaleswar Odisha India</option>
                        <option value="Jharsuguda Odisha India">Jharsuguda Odisha India</option>
                        <option value="Jagdalpur Odisha India">Jagdalpur Odisha India</option>
                        <option value="Jeypore Odisha India">Jeypore Odisha India</option>
                        <option value="Jagatsinghpur Odisha India">Jagatsinghpur Odisha India</option>
                        <option value="Khurdha Odisha India">Khurdha Odisha India</option>
                        <option value="Konark Odisha India">Konark Odisha India</option>
                        <option value="Kendrapara Odisha India">Kendrapara Odisha India</option>
                        <option value="Koraput Odisha India">Koraput Odisha India</option>
                        <option value="Keonjhar Odisha India">Keonjhar Odisha India</option>
                        <option value="Mayurbhanj Odisha India">Mayurbhanj Odisha India</option>
                        <option value="Muniguda Odisha India">Muniguda Odisha India</option>
                        <option value="Malkangiri Odisha India">Malkangiri Odisha India</option>
                        <option value="Nayagarh Odisha India">Nayagarh Odisha India</option>
                        <option value="Nowrangpur Odisha India">Nowrangpur Odisha India</option>
                        <option value="Nuapada Odisha India">Nuapada Odisha India</option>
                        <option value="Puri Odisha India">Puri Odisha India</option>
                        <option value="Paradip Odisha India">Paradip Odisha India</option>
                        <option value="Phulbani Odisha India">Phulbani Odisha India</option>
                        <option value="Paralakhemundi Odisha India">Paralakhemundi Odisha India</option>
                        <option value="Rayagada Odisha India">Rayagada Odisha India</option>
                        <option value="Rourkela Odisha India">Rourkela Odisha India</option>
                        <option value="Satapada Odisha India">Satapada Odisha India</option>
                        <option value="SakhiGopal Odisha India">SakhiGopal Odisha India</option>
                        <option value="Sambalpur Odisha India">Sambalpur Odisha India</option>
                        <option value="Sunabeda Odisha India">Sunabeda Odisha India</option>
                        <option value="Sundargarh Odisha India">Sundargarh Odisha India</option>
                        <option value="Sonepur Odisha India">Sonepur Odisha India</option>
                        <option value="Simlipal Odisha India">Simlipal Odisha India</option>
                        <option value="Titlagarh Odisha India">Titlagarh Odisha India</option>
                        <option value="Talcher Odisha India">Talcher Odisha India</option>
                        <option value="Taptapani Odisha India">Taptapani Odisha India</option>
                        <option value="taxifrom">Other Location</option>
                      </select>
                      </div></div>
                            <div class="col-md-6">
                                <div class="form-group">
                            <select name="airport_drop" id="airport_drop" class="form-control" >
                        <option selected="selected">Drop To</option>
                        <option value=" Bhubaneswar Airport (BBI)"> Bhubaneswar Airport (BBI)</option>
                        <option value="Bhubaneswar Railway Station ">Bhubaneswar Railway Station </option>
                        <option value="Bhubaneswar Hotels">Bhubaneswar Hotels</option>
                        <option value="Puri Railway Station ">Puri Railway Station </option>
                        <option value="Puri Hotels">Puri Hotels</option>
                        <option value="Puri Area">Puri Area</option>
                        <option value="Khurdha Jn Railway Station">Khurdha Jn Railway Station</option>
                        <option value="Cuttack Railway Station">Cuttack Railway Station</option>
                        <option value="Berhampur Railway Station">Berhampur Railway Station</option>
                        <option value="Angul Odisha India">Angul Odisha India</option>
                        <option value="Bhubaneswar Odisha India">Bhubaneswar Odisha India</option>
                        <option value="Brahmagiri Odisha India">Brahmagiri Odisha India</option>
                        <option value="Bhitarkanika Odisha India">Bhitarkanika Odisha India</option>
                        <option value="Brahmapur Odisha India">Brahmapur Odisha India</option>
                        <optgroup label="----------------------------------------"></optgroup>
                        <option value="Balangir Odisha India">Balangir Odisha India</option>
                        <option value="Balasore Odisha India ">Balasore Odisha India </option>
                        <option value="Barbil Odisha India">Barbil Odisha India</option>
                        <option value="Bargarh Odisha India">Bargarh Odisha India</option>
                        <option value="Baripada Odisha India">Baripada Odisha India</option>
                        <option value="Bhadrak Odisha India">Bhadrak Odisha India</option>
                        <option value="Balugaon Odisha India">Balugaon Odisha India</option>
                        <option value="Bhawanipatna Odisha India">Bhawanipatna Odisha India</option>
                        <option value="Balliguda Odisha India">Balliguda Odisha India</option>
                        <option value="Boudh Odisha India">Boudh Odisha India</option>
                        <option value="Chilika Lake Odisha India">Chilika Lake Odisha India</option>
                        <option value="Cuttack Odisha India">Cuttack Odisha India</option>
                        <option value="Chandipur Odisha India">Chandipur Odisha India</option>
                        <option value="Chhatrapur Odisha India">Chhatrapur Odisha India</option>
                        <option value="Daringbadi Odisha India">Daringbadi Odisha India</option>
                        <option value="Dhabaleswar Odisha India">Dhabaleswar Odisha India</option>
                        <option value="Dhenkanal Odisha India">Dhenkanal Odisha India</option>
                        <option value="Deogarh Odisha India">Deogarh Odisha India</option>
                        <option value="Damanjodi Odisha India">Damanjodi Odisha India</option>
                        <option value="Gopalpur Odisha India">Gopalpur Odisha India</option>
                        <option value="Ganjam Odisha India">Ganjam Odisha India</option>
                        <option value="Jajpur Odisha India">Jajpur Odisha India</option>
                        <option value="Jaleswar Odisha India">Jaleswar Odisha India</option>
                        <option value="Jharsuguda Odisha India">Jharsuguda Odisha India</option>
                        <option value="Jagdalpur Odisha India">Jagdalpur Odisha India</option>
                        <option value="Jeypore Odisha India">Jeypore Odisha India</option>
                        <option value="Jagatsinghpur Odisha India">Jagatsinghpur Odisha India</option>
                        <option value="Khurdha Odisha India">Khurdha Odisha India</option>
                        <option value="Konark Odisha India">Konark Odisha India</option>
                        <option value="Kendrapara Odisha India">Kendrapara Odisha India</option>
                        <option value="Koraput Odisha India">Koraput Odisha India</option>
                        <option value="Keonjhar Odisha India">Keonjhar Odisha India</option>
                        <option value="Mayurbhanj Odisha India">Mayurbhanj Odisha India</option>
                        <option value="Muniguda Odisha India">Muniguda Odisha India</option>
                        <option value="Malkangiri Odisha India">Malkangiri Odisha India</option>
                        <option value="Nayagarh Odisha India">Nayagarh Odisha India</option>
                        <option value="Nowrangpur Odisha India">Nowrangpur Odisha India</option>
                        <option value="Nuapada Odisha India">Nuapada Odisha India</option>
                        <option value="Puri Odisha India">Puri Odisha India</option>
                        <option value="Paradip Odisha India">Paradip Odisha India</option>
                        <option value="Phulbani Odisha India">Phulbani Odisha India</option>
                        <option value="Paralakhemundi Odisha India">Paralakhemundi Odisha India</option>
                        <option value="Rayagada Odisha India">Rayagada Odisha India</option>
                        <option value="Rourkela Odisha India">Rourkela Odisha India</option>
                        <option value="Satapada Odisha India">Satapada Odisha India</option>
                        <option value="SakhiGopal Odisha India">SakhiGopal Odisha India</option>
                        <option value="Sambalpur Odisha India">Sambalpur Odisha India</option>
                        <option value="Sunabeda Odisha India">Sunabeda Odisha India</option>
                        <option value="Sundargarh Odisha India">Sundargarh Odisha India</option>
                        <option value="Sonepur Odisha India">Sonepur Odisha India</option>
                        <option value="Simlipal Odisha India">Simlipal Odisha India</option>
                        <option value="Titlagarh Odisha India">Titlagarh Odisha India</option>
                        <option value="Talcher Odisha India">Talcher Odisha India</option>
                        <option value="Taptapani Odisha India">Taptapani Odisha India</option>
                        <option value="taxifrom">Other Location</option>
                      </select>
                            </div></div>
                        </div>
                        <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                            <input type="text" class="form-control" placeholder="Arrival Date" onfocus="(this.type='date')"  name="airport_avdate" value="" id="airport_avdate"  required="required">
                           
                          </div></div>
                        <div class="col-md-3">
                            <div class="form-group">
                            <input type="text" class="form-control" placeholder="Arrival Time" name="airport_avtime" id="airport_avtime" value="" required="required">
                            </div></div>
                            <div class="col-md-3">
                                <div class="form-group">
                            <input type="text" class="form-control" placeholder="Deparature Date" name="airport_dpdate" onfocus="(this.type='date')" value="" id="airport_dpdate" required="required">
                            </div></div>
                            <div class="col-md-3">
                                <div class="form-group">
                            <input type="text" class="form-control" placeholder="Deparature Time" name="airport_dptime" id="airport_dptime" required="required" value="" >
                            </div></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                           <select name="airport_veh_type" id="airport_veh_type" class="form-control">
                               <option selected>Vehicle Type - Cab / Mini Coach / Bus</option>
                              </select>
                           </div></div>
                        <div class="col-md-6">
                            <div class="form-group">
                           <textarea  class="form-control" placeholder="Tour Requirements & Preferences(if Any..)" name="airport_ref" id="airport_ref"></textarea>
                            </div></div>
                        </div>
                        <div class="form-group">
                        <label>YOUR CONTACT INFORMATION</label>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                               <div class="form-group">
                            <input type="text" class="form-control" placeholder="Full name.." name="airport_fname" id="airport_fname" required="required">
                            </div></div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <input type="text" class="form-control" placeholder="Email Address..." name="airport_email" id="airport_email" required="required">
                            </div></div> 
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                               <div class="form-group">
                            <input type="tel" class="form-control" placeholder="Contact Number" name="airport_phn" id="airport_phn"  required="required">
                            </div></div>
                            <div class="col-md-2">
                                <div class="form-group">
                                <input type ="text" placeholder="7518" class="form-control">
                            </div></div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <input type="text" class="form-control" placeholder="Enter Captcha Code Here">
                            </div></div>
                        </div>
                        <button type="submit" class="btn btn-info submit"  name="airportbtn" id="submitId_airport">SUBMIT</button>
                    </form>
                    </div>
                  </div>
               <div class="col-md-5">
                   <img src="images/airport.jpg" class="img-responsive">
                       </div>
                  </div>
            </div>
            <div id="Hotel-Booking" class="tab-pane fade">
              <div class="col-md-12 white">
               <div class="col-md-7">
                   <div class="row">
                  <h3>GET A  FREE   QUOTATION  FROM OUR TRAVEL EXPERTS</h3>
                  <h4>[For Best Tour Request,Best price Quotation,Best Itinerary Suggestion]</h4>
                    <form name="hotel_form" method="POST" id="hotel_form">
                        <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                           <select class="form-control" name="hotel_loc" id="hotel_loc" >
                            <option selected>Enter a Location...</option>
                            <option>Bhubaneswar</option>
                            </select>
                            </div></div>
                            <div class="col-md-3">
                                <div class="form-group">
                            <input type="text" class="form-control" placeholder="Checkin.." name="hotel_chkin" id="hotel_chkin" onfocus="(this.type='date')" required="required">
                            </div></div>
                            <div class="col-md-3">
                                <div class="form-group">
                            <input type="text" class="form-control" placeholder="Checkout.." name="hotel_chkout" id="hotel_chkout" onfocus="(this.type='date')" required="required">
                                </div></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6"></div>
                        <div class="col-md-3">
                            <div class="form-group">
                            <input type="number" class="form-control" placeholder="No. of Adults" name="hotel_adlt" id="hotel_adlt" required="required">
                            </div></div>
                        <div class="col-md-3">
                            <div class="form-group">
                            <input type="number" class="form-control" placeholder="No. of Children" name="Hotel_child" id="hotel_child" required="required">
                              </div></div>
                        </div>
                        <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                           <select class="form-control" name="hotel_type" id="hotel_type" >
                            <option selected>Hotel Type...</option>
                            <option value="Heritage Hotel">Heritage Hotel</option>
                            <option value="5 Star hotel">5 Star Hotel</option>
                            <option value="4 Star Hotel">4 Star Hotel</option>
                            <option value="3 Star Hotel">3 Star Hotel</option>
                            <option value="Budget Hotel">Budget Hotel</option>
                            </select>
                            </div></div>
                            <div class="col-md-6">
                                <div class="form-group">
                            <input type="number" placeholder="No. of Rooms" class="form-control" name="room" id="room" required="required">
                            </div></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                        Need Taxi for  your Trip&nbsp; &nbsp;<input type="radio" name="yes" value="yes" id="yes" >&nbsp;Yes
                            <input type="radio" name="yes" value="no" id="no" required="required">&nbsp;No
                        </div></div></div>
                        <div class="form-group">
                        <label>YOUR CONTACT INFORMATION</label>
                        </div>
                       <div class="row">
                           <div class="col-md-6">
                               <div class="form-group">
                            <input type="text" class="form-control" placeholder="Full name.." name="hotel_fname" id="hotel_fname" required="required">
                            </div></div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <input type="text" class="form-control" placeholder="Email Address..." name="hotel_email" id="hotel_email" required="required">
                            </div></div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                               <div class="form-group">
                            <input type="tel" class="form-control" placeholder="Contact Number" name="hotel_phn" id="hotel_phn" required="required">
                            </div></div>
                            <div class="col-md-2">
                                <div class="form-group">
                                <input type ="text" placeholder="7518" class="form-control">
                            </div></div>
                        <div class="col-md-4">
                            <div class="form-group">
                            <input type="text" class="form-control" placeholder="Enter Captcha Code Here">
                            </div></div>
                        </div>
                        <button type="submit" class="btn btn-info submit" id="submitId_hotel" name="hotelbtn">SUBMIT</button>
                    </form>
                    </div>
                  </div>
               <div class="col-md-5">
                   <div class="row">
                       <div class="head-image">
                   <img src="images/htl.jpg" class="img-responsive"></div>
                       </div>
                   </div>
                  </div>
            </div>
            <div id="Wedding-Car" class="tab-pane fade">
              <div class="col-md-12 white">
               <div class="col-md-7">
                   <div class="row">
                  <h3>GET A  FREE   QUOTATION  FROM OUR TRAVEL EXPERTS</h3>
                  <h4>[For Best Tour Request,Best price Quotation,Best Itinerary Suggestion]</h4>
                    <form name="wed_form" id="wed_form" method="POST" >
                        <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                           <select class="form-control" name="wed_city" id="wed_city" >
                            <option>Select City..</option>
                            </select>
                            </div></div>
                            <div class="col-md-6">
                                <div class="form-group">
                            <select class="form-control" name="wed_pickup" id="wed_pickup">
                                <option>Pickup Location</option>
                            </select>
                            </div></div>
                        </div>
                        <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                            <input type="text" class="form-control" placeholder="Pick Up Date.." name="wed_pickdate" id="wed_pickdate" onfocus="(this.type='date')" required="required">
                            </div></div>
                        <div class="col-md-3">
                            <div class="form-group">
                          <input type="text" name="wed_picktime" class="form-control" id="wed_picktime" required="required"  placeholder="Pickup time">
                            </div></div>
                            <div class="col-md-6">
                                <div class="form-group">
                            <select class="form-control" name="wed_veh" id="wed_veh">
                            <option>Vehicle Type</option>
                            </select>
                            </div></div>
                        </div>
                        <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <input type="text" placeholder="Duration" name="wed_dur" id="wed_dur"  class="form-control" required="required">
                            </div></div>
                            <div class="col-md-6">
                                <div class="form-group">
                            Need Flower Decoration&nbsp; &nbsp;<input type="radio" name="yes" value="yes" id="yes">&nbsp;Yes
                            <input type="radio" name="yes" value="no" id="no">&nbsp;No
                            </div></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                            <div class="from-group">
                        <textarea class="form-control" name="wed_ref" id="wed_ref">Customize Message</textarea>
                        </div></div></div><br>
                        <div class="form-group">
                        <label>YOUR CONTACT INFORMATION</label>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                               <div class="form-group">
                            <input type="text" class="form-control" placeholder="Full name.." name="wed_fname" id="wed_fname" required="required">
                            </div></div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <input type="text" class="form-control" placeholder="Email Address..." name="wed_email" id="wed_email" required="required">
                            </div></div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                               <div class="form-group">
                            <input type="tel" class="form-control" placeholder="Contact Number" name="wed_phn" id="wed_phn" required="required">
                            </div></div>
                            <div class="col-md-2">
                                <input type ="text" placeholder="7518" class="form-control">
                            </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Enter Captcha Code Here">
                            </div> 
                        </div>
                        <button type="submit" class="btn btn-info submit" name="wedbtn" id="submitId_wed">SUBMIT</button>
                    </form>
                    </div>
                  </div>
               <div class="col-md-5">
                   <div class="row">
                       <div class="head-image">
                   <img src="images/wed-car.jpg" class="img-responsive">
                       </div></div></div>
                  </div>
            </div>
            </div>
    <div class="clearfix"></div>
     </div>
     <div class="clearfix"></div>
     </div>
     </div>
        <div class="container">
        <div class="welcome-mg mgntpbt">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <h3>Welcome To STAR DMC! We cover all World...</h3>
            <p>STAR DMC is a professionally managed team established in the year 2009 and during last few year the company has focused entirely on corporate house, multinational companies and foreign clients. the company has organised some of seminars, conference,annual picnics, Adventures tour, Pilgrimage tour, Family & group tour, Educational Tour & Yatra trips. We are one of leading a complete travels solution company in India. We provide an authentic travel experience, revealing the true pulse of India.</p>
              <br>
              <button class="read-more"><a href="#">Read More</a></button>
            </div>
           <div class="col-md-6 col-sm-6 col-xs-12">
           <div class="row">
            <div class="head-image">
          <div id="slide2" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#slide2" data-slide-to="0" class="active"></li>
      <li data-target="#slide2" data-slide-to="1"></li>
      <li data-target="#slide2" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="item active">
        <img src="images/about-slide-1.png" alt="Los Angeles" >
      </div>

      <div class="item">
        <img src="images/about-slide-2.png" alt="Chicago">
      </div>
    
      <div class="item">
        <img src="images/about-slide-3.png" alt="New york">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#slide2" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#slide2" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
     <span class="sr-only">Next</span>
    </a>
  </div>
               </div>
               </div>
            </div>
            <div class="clearfix"></div>
            </div>
      
         
        <div class="tour mgntpbt">
            <h3>FEATURED ODISHA PACKAGES</h3><br>
            <div class="item-slide">
			<div class="item">
				<ul id="content-slider" class="content-slider">
					<li>
                       <div class="package"><img src="images/odisha.png" class="img-responsive">
                        <div class="black">
                        <img src="images/car.png" alt="">
                            <img src="images/accom.png" alt="">
                            <img src="images/food.png" alt="">
                           </div>
                           <div class="text">
                           <h4>Lorem Ipsum is simply </h4>
                           <p>09 NIGHTS / 10 DAYS</p>
                            <a href="detail.php">View More</a>
                           <button class="btn  btn-info book-now" data-toggle="modal" data-target="#abc">Customised & Book Online</button>
                           </div>
                        </div>
                        
					</li>
					<li>
                       <div class="package"><img src="images/odisha.png" class="img-responsive">
                        <div class="black">
                        <img src="images/car.png" alt="">
                            <img src="images/accom.png" alt="">
                            <img src="images/food.png" alt="">
                           </div>
                           <div class="text">
                           <h4>Lorem Ipsum is simply </h4>
                           <p>09 NIGHTS / 10 DAYS</p>
                            <a href="#">View More</a>
                           <button class="custom">Customised & Book Online</button>
                           </div>
                        </div>
                        
					</li>
					<li>
                       <div class="package"><img src="images/odisha.png" class="img-responsive">
                        <div class="black">
                        <img src="images/car.png" alt="">
                            <img src="images/accom.png" alt="">
                            <img src="images/food.png" alt="">
                           </div>
                           <div class="text">
                           <h4>Lorem Ipsum is simply </h4>
                           <p>09 NIGHTS / 10 DAYS</p>
                            <a href="#">View More</a>
                           <button class="custom">Customised & Book Online</button>
                           </div>
                        </div>
                        
					</li>
					<li>
                       <div class="package"><img src="images/odisha.png" class="img-responsive">
                        <div class="black">
                        <img src="images/car.png" alt="">
                            <img src="images/accom.png" alt="">
                            <img src="images/food.png" alt="">
                           </div>
                           <div class="text">
                           <h4>Lorem Ipsum is simply </h4>
                           <p>09 NIGHTS / 10 DAYS</p>
                            <a href="#">View More</a>
                           <button class="custom">Customised & Book Online</button>
                           </div>
                        </div>
                        
					</li>
					<li>
                       <div class="package"><img src="images/odisha.png" class="img-responsive">
                        <div class="black">
                        <img src="images/car.png" alt="">
                            <img src="images/accom.png" alt="">
                            <img src="images/food.png" alt="">
                           </div>
                           <div class="text">
                           <h4>Lorem Ipsum is simply </h4>
                           <p>09 NIGHTS / 10 DAYS</p>
                            <a href="#">View More</a>
                           <button class="custom">Customised & Book Online</button>
                           </div>
                        </div>
                        
					</li>
					<li>
                       <div class="package"><img src="images/odisha.png" class="img-responsive">
                        <div class="black">
                        <img src="images/car.png" alt="">
                            <img src="images/accom.png" alt="">
                            <img src="images/food.png" alt="">
                           </div>
                           <div class="text">
                           <h4>Lorem Ipsum is simply </h4>
                           <p>09 NIGHTS / 10 DAYS</p>
                            <a href="#">View More</a>
                           <button class="custom">Customised & Book Online</button>
                           </div>
                        </div>
                        
					</li>
				</ul>
			</div>
</div>
            
        </div>
        </div>
        <div class="clearfix"></div>
        
        <div class="hotels mgntpbt">
        <div class="container">
               <h3>EXCLUSIVE HOTEL DEALS</h3><br>
            <div class="item-slide">
			<div class="item">
				<ul id="hotel" class="content-slider">
					<li>
                       <div class="package"><img src="images/hotel.png" class="img-responsive">
                        <div class="black">
                        <img src="images/car.png" alt="">
                            <img src="images/accom.png" alt="">
                            <img src="images/food.png" alt="">
                           </div>
                           <div class="text">
                           <h4>Lorem Ipsum is simply </h4>
                           <p>09 NIGHTS / 10 DAYS</p>
                            <a href="hotel-deals.php">View More</a>
                           <button class="custom">Customised & Book Online</button>
                           </div>
                        </div>
                        
					</li>
					<li>
                       <div class="package"><img src="images/hotel.png" class="img-responsive">
                        <div class="black">
                        <img src="images/car.png" alt="">
                            <img src="images/car.png" alt="">
                            <img src="images/car.png" alt="">
                           </div>
                           <div class="text">
                           <h4>Lorem Ipsum is simply </h4>
                           <p>09 NIGHTS / 10 DAYS</p>
                            <a href="hotel-deals.php">View More</a>
                           <button class="custom">Customised & Book Online</button>
                           </div>
                        </div>
                        
					</li>
					<li>
                       <div class="package"><img src="images/hotel.png" class="img-responsive">
                        <div class="black">
                        <img src="images/car.png" alt="">
                            <img src="images/car.png" alt="">
                            <img src="images/car.png" alt="">
                           </div>
                           <div class="text">
                           <h4>Lorem Ipsum is simply </h4>
                           <p>09 NIGHTS / 10 DAYS</p>
                            <a href="hotel-deals.php">View More</a>
                           <button class="custom">Customised & Book Online</button>
                           </div>
                        </div>
                        
					</li>
					<li>
                       <div class="package"><img src="images/hotel.png" class="img-responsive">
                        <div class="black">
                        <img src="images/car.png" alt="">
                            <img src="images/car.png" alt="">
                            <img src="images/car.png" alt="">
                           </div>
                           <div class="text">
                           <h4>Lorem Ipsum is simply </h4>
                           <p>09 NIGHTS / 10 DAYS</p>
                            <a href="hotel-deals.php">View More</a>
                           <button class="custom">Customised & Book Online</button>
                           </div>
                        </div>
                        
					</li>
					<li>
                       <div class="package"><img src="images/hotel.png" class="img-responsive">
                        <div class="black">
                        <img src="images/car.png" alt="">
                            <img src="images/car.png" alt="">
                            <img src="images/car.png" alt="">
                           </div>
                           <div class="text">
                           <h4>Lorem Ipsum is simply </h4>
                           <p>09 NIGHTS / 10 DAYS</p>
                            <a href="hotel-deals.php">View More</a>
                           <button class="custom">Customised & Book Online</button>
                           </div>
                        </div>
                        
					</li>
					<li>
                       <div class="package"><img src="images/hotel.png" class="img-responsive">
                        <div class="black">
                        <img src="images/car.png" alt="">
                            <img src="images/car.png" alt="">
                            <img src="images/car.png" alt="">
                           </div>
                           <div class="text">
                           <h4>Lorem Ipsum is simply </h4>
                           <p>09 NIGHTS / 10 DAYS</p>
                            <a href="hotel-deals.php">View More</a>
                           <button class="custom">Customised & Book Online</button>
                           </div>
                        </div>
                        
					</li>
				</ul>
			</div>
</div>
        <div class="hotel-list">
            <button class="read-more">
            <a href="hotel-main.php">View More</a>
            </button></div> 
            
            </div> 
        
        </div>
        
        <div class="cars mgntpbt">
           <div class="container">
               <h3>FEATURED CARS</h3><br>
               <div class="row">
               <div class="col-md-3">
                <div class="car-image"><img src="images/toyota.jpg" class="img-responsive">
                           <div class="text-car">
                           <h4 style="margin:0;">TOYOTA YARIS</h4>
                           <p>CAPACITY -7G +1D</p>
                          <a href="#">View More</a>
                           <button class="custom">Customised & Book Online</button>
                           </div>
                </div>
                   </div>
                   <div class="col-md-3">
                <div class="car-image"><img src="images/toyota.jpg" class="img-responsive">
                           <div class="text-car">
                           <h4 style="margin:0;">TOYOTA YARIS</h4>
                           <p>CAPACITY -7G +1D</p>
                           <a href="#">View More</a>
                           <button class="custom">Customised & Book Online</button>
                           </div>
                </div>
                   </div>
                    <div class="col-md-3">
                <div class="car-image"><img src="images/toyota.jpg" class="img-responsive">
                           <div class="text-car">
                           <h4 style="margin:0;">TOYOTA YARIS</h4>
                           <p>CAPACITY -7G +1D</p>
                           <a href="#">View More</a>
                           <button class="custom">Customised & Book Online</button>
                           </div>
                </div>
                   </div>
                    <div class="col-md-3">
                <div class="car-image"><img src="images/toyota.jpg" class="img-responsive">
                           <div class="text-car">
                           <h4 style="margin:0;">TOYOTA YARIS</h4>
                           <p>CAPACITY -7G +1D</p>
                           <a href="#">View More</a>
                           <button class="custom">Customised & Book Online</button>
                           </div>
                </div>
                   </div>
                   
                   
               </div>
            </div><br><br>
        </div>
        <div class="quote mgntpbt">
            <div class="container">
                <div class="quote-txt">
       <img src="images/atithi.png" class="athithi" alt=""/><br/>
                    <button class="book">Book  Your Trip</button>
                </div>
            </div>
     </div>
       

        <div class="testimonial mgntpbt">
        <div class="container">
            <h3>CLIENT&#39;S  TESTIMONIAL</h3><br>
            <div class="slideshow-container">
<div class="col-md-12">
<div class="mySlides" style="display:block;">
    <div class="col-md-6">
      <p>&nbsp;&nbsp;&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.&nbsp;&nbsp;&nbsp;</p>
      <h5 class="author">- Lorem ipsum</h5>
    </div>
    <div class="col-md-6">
      <img src="images/a.jpg" class="img-responsive">
    </div>
</div>
<div class="mySlides">
    <div class="col-md-6">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
      <h5 class="author">- Lorem ipsum</h5>
    </div>
    <div class="col-md-6">
      <img src="images/a.jpg" class="img-responsive">
    </div>
</div>
    <div class="mySlides">
    <div class="col-md-6">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
      <h5 class="author">- Lorem ipsum</h5>
    </div>
    <div class="col-md-6">
      <img src="images/a.jpg" class="img-responsive">
    </div>
</div>
    </div>

<a class="prev" onclick="plusSlides(-1)"><</a>
<a class="next" onclick="plusSlides(1)">></a>

</div>
<div class="clearfix"></div>
<div class="dot-container">
  <span class="dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(3)"></span> 
</div>
</div>
</div>
<?php
include("footer.php");
?>
<script>
$( document ).ready(function() {
    $("#tour_form").submit(function (e) {
    	e.preventDefault();
		$.ajax({
			type: $('#tour_form').attr('method'),
			url: 'tour_ajax.php',
			data: $('#tour_form').serialize(),
			success: function (data) {
				alertify.alert(data);
				$("#tour_form")[0].reset();
				//document.getElementById("cform").reset();
			},
			error: function (data) {
				console.log('An error occurred.');
			},
		});
        return false;
    });
    
    $("#taxi_form").submit(function (e) {
    	e.preventDefault();
		$.ajax({
			type: $('#taxi_form').attr('method'),
			url: 'taxi_ajax.php',
			data: $('#taxi_form').serialize(),
			success: function (data) {
				alertify.alert(data);
				$("#taxi_form")[0].reset();
				//document.getElementById("cform").reset();
			},
			error: function (data) {
				console.log('An error occurred.');
			},
		});
        return false;
    });
    $("#airport_form").submit(function (e) {
    	e.preventDefault();
		$.ajax({
			type: $('#airport_form').attr('method'),
			url: 'airport_ajax.php',
			data: $('#airport_form').serialize(),
			success: function (data) {
				alertify.alert(data);
				$("#airport_form")[0].reset();
				//document.getElementById("cform").reset();
			},
			error: function (data) {
				console.log('An error occurred.');
			},
		});
        return false;
    });
    $("#hotel_form").submit(function (e) {
    	e.preventDefault();
		$.ajax({
			type: $('#hotel_form').attr('method'),
			url: 'hotel_ajax.php',
			data: $('#hotel_form').serialize(),
			success: function (data) {
				alertify.alert(data);
				$("#hotel_form")[0].reset();
				//document.getElementById("cform").reset();
			},
			error: function (data) {
				console.log('An error occurred.');
			},
		});
        return false;
    });
    $("#wed_form").submit(function (e) {
    	e.preventDefault();
		$.ajax({
			type: $('#wed_form').attr('method'),
			url: 'wedding_ajax.php',
			data: $('#wed_form').serialize(),
			success: function (data) {
				alertify.alert(data);
				$("#wed_form")[0].reset();
				//document.getElementById("cform").reset();
			},
			error: function (data) {
				console.log('An error occurred.');
			},
		});
        return false;
    });
});
</script>